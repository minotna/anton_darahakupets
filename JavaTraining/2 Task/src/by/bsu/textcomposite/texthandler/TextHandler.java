package by.bsu.textcomposite.texthandler;

import by.bsu.textcomposite.comparator.SizeSentensesComparator;
import by.bsu.textcomposite.textpart.*;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by 21vek.by on 07.06.2015.
 */
public class TextHandler {
    private static final String PARAGRAPH = "\\s{4,}[A-Za-z,.():'\\-\\d\\s\\n]+(\\.|!|\\?)";
    private static final String LISTING = "//:[^\\t]+///:~";
    private static final String PARAGRAPH_OR_LISTING = "((\\s{4,}[A-Za-z,.():'\\-\\d\\s]+(\\.|!|\\?)))|(//:[^\\t]+///:~)";
    private static final String SENTENCE = "([^(\\.|!|\\?)]+)(\\.|!|\\?)";
    private static final String WORD = "[A-Za-z'\\d]+";
    private static final String SYMBOL = "[.,!?:;]";
    private static final String WORD_OR_SYMBOL = "[A-Za-z\\d]+|[.,!?:;]";
    private static final Logger LOG = Logger.getLogger(TextHandler.class.getSimpleName());

    public static String parseFile(String url)  {
        StringBuilder str = new StringBuilder();
        int b;
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(new File(url));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while ((b = fileReader.read()) != -1){
                str.append((char) b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    public static TextComposite textCreator(String str) {
        TextComposite text = new TextComposite();

        String paragraphAndListing = null;
        String paragraph = null;
        String sentence = null;
        String wordAndSymbol = null;

        Pattern patternParagraphAndListing = Pattern.compile(PARAGRAPH_OR_LISTING);
        Pattern patternParagraph = Pattern.compile(PARAGRAPH);
        Pattern patternListing = Pattern.compile(LISTING);
        Pattern patternSentence = Pattern.compile(SENTENCE);
        Pattern patternWordAndSymbol = Pattern.compile(WORD_OR_SYMBOL);
        Pattern patternWord = Pattern.compile(WORD);
        Pattern patternSymbol = Pattern.compile(SYMBOL);

        Matcher matcherListing;
        Matcher matcherParagraph;
        Matcher matcherSentence;
        Matcher matcherWordAndSymbol;
        Matcher matcherWord;
        Matcher matcherSymbol;

        Matcher matcherParagraphAndListing = patternParagraphAndListing.matcher(str);

        while (matcherParagraphAndListing.find()) {
            paragraphAndListing = matcherParagraphAndListing.group();
            matcherParagraph = patternParagraph.matcher(paragraphAndListing);
            matcherListing = patternListing.matcher(paragraphAndListing);
            if (matcherListing.find()) {
                text.addComponent(new Leaf(matcherListing.group(), TypeLeaf.LISTING));
            }else {
                while (matcherParagraph.find()) {
                    paragraph = matcherParagraph.group();
                    matcherSentence = patternSentence.matcher(paragraph);
                    Composite compositeParagraph = new Composite(TypeLeaf.PARAGRAPH);
                    while (matcherSentence.find()) {
                        sentence = matcherSentence.group();
                        matcherWordAndSymbol = patternWordAndSymbol.matcher(sentence);
                        Composite compositeSentence = new Composite(TypeLeaf.SENTENCE);
                        while (matcherWordAndSymbol.find()) {
                            wordAndSymbol = matcherWordAndSymbol.group();
                            matcherWord = patternWord.matcher(wordAndSymbol);
                            matcherSymbol = patternSymbol.matcher(wordAndSymbol);
                            if (matcherWord.find()) {
                                compositeSentence.addComponent(new Leaf(matcherWord.group(),TypeLeaf.WORD));
                            }else if(matcherSymbol.find()){
                                compositeSentence.addComponent(new Leaf(matcherSymbol.group(),TypeLeaf.SYMBOL));
                            }
                        }
                        compositeParagraph.addComponent(compositeSentence);
                    }
                    text.addComponent(compositeParagraph);
                }
            }
        }
        return text;
    }

    public static List findSortedSentenceList(TextComposite textComposite){
        List<Component> list = new ArrayList<Component>();
        textComposite.getComponents().stream().filter(component -> component.getTypeLeaf() == TypeLeaf.PARAGRAPH).forEach(component -> {
            list.addAll(component.getComponents().stream().collect(Collectors.toList()));
        });
        Collections.sort(list, new SizeSentensesComparator());
        return list;
    }

    public static Component findNonrecuringWord(TextComposite text){
        List<Component> wordsFromFirstSentence = new ArrayList<Component>();
        List<Component> otherWords = new ArrayList<Component>();

        wordsFromFirstSentence.addAll(findParagraph(text, 0).getComponents().get(0).getComponents().stream().filter(component -> component.getTypeLeaf() == TypeLeaf.WORD).collect(Collectors.toList()));
        text.getComponents().stream().filter(component -> component.getTypeLeaf() == TypeLeaf.PARAGRAPH).forEach(component -> {
            for (Component component1 : component.getComponents()) {
                component1.getComponents().stream().filter(component2 -> component2.getTypeLeaf() == TypeLeaf.WORD).forEach(component2 -> {
                    otherWords.add(component2);
                    if (otherWords.equals(wordsFromFirstSentence)) {
                        otherWords.clear();
                    }

                });

            }
        });
        wordsFromFirstSentence.removeAll(otherWords);


        LOG.info(wordsFromFirstSentence);
        if(wordsFromFirstSentence.get(0) == null){
            LOG.info("No Such Word!");
        }
        return wordsFromFirstSentence.get(0);
    }


    public static int numberOfWords(Component component){
        int words = 0;
        if(component.getTypeLeaf() != TypeLeaf.SENTENCE){
            LOG.error("Illegal component");
        }
        for (Component component1 : component.getComponents()) {
            if(component1.getTypeLeaf() == TypeLeaf.WORD){
                ++words;
            }
        }
        return words;
    }
    public static Component findParagraph(TextComposite text, int i){
        List<Component> list = text.getComponents().stream().filter(component -> component.getTypeLeaf() == TypeLeaf.PARAGRAPH).collect(Collectors.toList());
        if((i+1) >= list.size()) {
            LOG.error("No such paragraph");
        }
        return list.get(i);
    }

}
