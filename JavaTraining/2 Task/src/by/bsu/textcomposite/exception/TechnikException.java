package by.bsu.textcomposite.exception;

/**
 * Created by 21vek.by on 15.06.2015.
 */
public class TechnikException extends Exception {
    public TechnikException() {
        super();
    }

    public TechnikException(String message) {
        super(message);
    }

    public TechnikException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnikException(Throwable cause) {
        super(cause);
    }

    protected TechnikException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
