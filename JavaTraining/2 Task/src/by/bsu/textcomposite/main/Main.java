package by.bsu.textcomposite.main;

import by.bsu.textcomposite.reporter.Reporter;
import by.bsu.textcomposite.texthandler.TextHandler;
import by.bsu.textcomposite.textpart.TextComposite;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;

/**
 * Created by 21vek.by on 15.06.2015.
 */
public class Main {
    public static final Logger LOG = Logger.getLogger(Main.class);
    static{
        PropertyConfigurator.configure("src/property/log4j.properties");
    }
    public static void main(String[] args){
        TextComposite textComposite = null;
        textComposite = TextHandler.textCreator(TextHandler.parseFile("data/text.txt"));


        System.out.println(textComposite.toString());
        System.out.println(Reporter.generalReport(textComposite));
    }
}
