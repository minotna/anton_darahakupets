package by.bsu.textcomposite.comparator;

import by.bsu.textcomposite.texthandler.TextHandler;
import by.bsu.textcomposite.textpart.Component;
import by.bsu.textcomposite.textpart.Composite;
import org.apache.log4j.Logger;

import java.util.Comparator;

/**
 * Created by 21vek.by on 08.06.2015.
 */
public class SizeSentensesComparator implements Comparator<Component> {
    private static final Logger log = Logger.getLogger(SizeSentensesComparator.class);
    @Override
    public int compare(Component o1, Component o2) {
        return TextHandler.numberOfWords(o1)- TextHandler.numberOfWords(o2);
    }
}
