package by.bsu.textcomposite.textpart;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public class Leaf implements Component{
    private TypeLeaf type;
    private String contents;

    public Leaf(String content, TypeLeaf type) {
        this.type = type;
        this.contents = content;
    }

    @Override
    public boolean addComponent(Component component) {
        throw new UnsupportedOperationException();
    }

    @Override
    public TypeLeaf getTypeLeaf() {
        return type;
    }

    public void setTypeLeaf(TypeLeaf type) {
        this.type = type;
    }

    @Override
    public List<Component> getComponents(){
        throw new UnsupportedOperationException();
    }

    public boolean removeComponent(Component component){
        throw new UnsupportedOperationException();
    }

    public String getContents(){
        return contents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Leaf)) return false;
        if (o == null) return false;

        Leaf leaf = (Leaf) o;

        if (!contents.equals(leaf.getContents())) return false;
        if (type != leaf.getTypeLeaf()) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + type.hashCode();
        result = 31 * result + contents.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return contents;
    }

}
