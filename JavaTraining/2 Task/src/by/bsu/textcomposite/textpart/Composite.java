package by.bsu.textcomposite.textpart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public class Composite implements Component{

    private List<Component> componentList;
    private TypeLeaf typeLeaf;

    public Composite(TypeLeaf typeLeaf) {
        this.typeLeaf = typeLeaf;
        componentList = new ArrayList<Component>();

    }

    public Composite() {
        this.componentList = new ArrayList<Component>();
    }

    @Override
    public List<Component> getComponents() {
        return componentList;
    }

    @Override
    public boolean addComponent(Component component) {
        return componentList.add(component);
    }

    @Override
    public TypeLeaf getTypeLeaf() {
        return typeLeaf;
    }

    @Override
    public void setTypeLeaf(TypeLeaf typeLeaf) {
        this.typeLeaf = typeLeaf;

    }

    @Override
    public boolean removeComponent(Component component) {
       return componentList.remove(component);
    }

    @Override
    public boolean equals(Object obj){
        if (obj == this) return false;
        if (!(obj instanceof Composite)) return false;
        if (obj == null) return false;

        Composite composite = (Composite) obj;

        if(!(componentList.equals(composite.getComponents()))) return false;
        if (typeLeaf != composite.getTypeLeaf()) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result = 1;
        result = 37 * result + componentList.hashCode();
        result = 37 * result + typeLeaf.hashCode();
        return result;
    }


}
