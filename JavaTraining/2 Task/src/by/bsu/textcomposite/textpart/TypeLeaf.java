package by.bsu.textcomposite.textpart;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public enum TypeLeaf {
    TEXT, PARAGRAPH, SENTENCE, WORD, SYMBOL, LISTING
}
