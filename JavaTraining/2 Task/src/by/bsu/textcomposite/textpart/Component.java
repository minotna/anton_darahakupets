package by.bsu.textcomposite.textpart;

import java.util.List;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public interface Component {
    public List<Component> getComponents();
    public boolean addComponent(Component component);
    public TypeLeaf getTypeLeaf();
    public void setTypeLeaf(TypeLeaf typeLeaf);
    public boolean removeComponent(Component component);

}
