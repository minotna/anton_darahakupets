package by.bsu.textcomposite.textpart;

/**
 * Created by 21vek.by on 08.06.2015.
 */
public class TextComposite extends Composite {
    publicHave you heard of static constructor in Java? I guess yes but the fact is that they are not allowed in Java. A constructor can not be static in Java.  Before I explain the reason let�s have a look at the below piece of code: TextComposite() {
        this.setTypeLeaf(TypeLeaf.TEXT);
}
