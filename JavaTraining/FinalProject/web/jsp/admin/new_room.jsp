<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>
<c:import url="../common/menu.jsp"/>
<html>
<head>
  <link rel="stylesheet" href="../../css/style.css">
  <title><title><fmt:message key="message.headtable.newroom" bundle="${rb}"/> </title></title>

</head>
<body>
<c:set var="pattern" value="[0-9]{1,3}"/>
<table cellspacing="0" class="general_table">
  <caption><fmt:message key="message.headtable.newroom" bundle="${rb}"/></caption>
  <form action="controller" method="post">
    <tr>
      <td align="center"><fmt:message key="message.roomnumber" bundle="${rb}"/></td>
      <td align="center"><input type="text" name="room_number" required value="" pattern="${pattern}"></td>
      <td colspan="2" align="center"><button type="submit" name="command" value="add_room"><fmt:message key="message.button.create" bundle="${rb}"/></button></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><fmt:message key="message.headtable.roomlist" bundle="${rb}"/></td>
      <td colspan="2" class="errormessage">${errorMessage}</td>
    </tr>
    <tr class="line_with_id">
      <td align="center"><fmt:message key="message.pricelist.type" bundle="${rb}"/></td>
      <td align="center"><fmt:message key="message.pricelist.rate" bundle="${rb}"/></td>
      <td align="center"><fmt:message key="message.pricelist.maxpersons" bundle="${rb}"/></td>
      <td></td>
    </tr>
    <c:forEach var="type" items="${types}">
      <tr>
        <td>${type.type.type}</td>
        <td>${type.rate}</td>
        <td>${type.maxPersons}</td>
        <td><input type="radio" name="id_room_type" value="${type.id}"></td>
      </tr>
    </c:forEach>
  </form>
</table>
<a href="../../index.jsp">
  <input type="submit" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
