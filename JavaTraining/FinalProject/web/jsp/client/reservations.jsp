<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="../common/menu.jsp"/>
<html>
<head>
    <title><fmt:message key="message.headtable.reservations" bundle="${rb}"/> </title>
</head>
<body>
<table class="general_table">
    <caption>
        <fmt:message key="message.headtable.reservations" bundle="${rb}"/>
    </caption>
    <tr>
        <td align="center"><fmt:message key="message.login" bundle="${rb}"/></td>
        <td align="center"><fmt:message key="message.reservation.datereservation" bundle="${rb}"/></td>
        <td></td>
    </tr>
    <c:forEach var="reservation" items="${reservations}">
        <form action="controller" method="post">
            <input type="hidden" name="id_reservation" value="${reservation.id}"/>
            <tr>
                <td><div>${reservation.detail.login}</div></td>
                <td>
                    <fmt:formatDate value="${reservation.detail.dateReservation}" type="date"/>
                    <fmt:formatDate value="${reservation.detail.dateReservation}" type="time"/>
                </td>
                <td align="center">
                    <input type="hidden" name="command" value="reservation_detail">
                    <input type="submit" value="<fmt:message key="message.button.detail" bundle="${rb}"/>"></td>
            </tr>
        </form>

    </c:forEach>
</table>

<a href="../../index.jsp">
    <input type="button" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
