<%--
  Created by IntelliJ IDEA.
  User: 21vek.by
  Date: 13.07.2015
  Time: 13:50
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="property.pagecontent" var="rb"/>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
</head>
<body>
<fmt:setLocale value= "${locale}" scope="session"/>
<jsp:forward page="/controller?command=auth"/>
</body>
</html>