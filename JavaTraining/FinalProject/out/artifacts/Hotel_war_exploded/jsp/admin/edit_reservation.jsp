<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources.property.pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:import url="../common/menu.jsp"/>
<html>
<head>
  <title><fmt:message key="message.headtable.details" bundle="${rb}"/> </title>
</head>
<body>
<table class="general_table">
  <caption>
    <fmt:message key="message.headtable.details" bundle="${rb}"/>
  </caption>
  <tr>
    <td colspan="2" align="center">${reservation.detail.login}</td>
    <td colspan="2" align="center"><fmt:formatDate value="${reservation.detail.dateReservation}" type="date"/>
      <fmt:formatDate value="${reservation.detail.dateReservation}" type="time"/>
    </td>
  </tr>
  <tr align="center">
     <td><fmt:message key="message.pricelist.maxpersons" bundle="${rb}"/></td>
     <td><fmt:message key="message.pricelist.type" bundle="${rb}"/></td>
     <td><fmt:message key="message.reservation.datearrival" bundle="${rb}"/></td>
     <td><fmt:message key="message.reservation.datedeparture" bundle="${rb}"/></td>
  </tr>
  <tr>
    <td>${reservation.detail.roomType.maxPersons}</td>
    <td>${reservation.detail.roomType.type.type}</td>
    <td><fmt:formatDate value="${reservation.arrivalDate}" type="date"/></td>
    <td><fmt:formatDate value="${reservation.departureDate}" type="date"/></td>
  </tr>
  <form action="controller" method="post">
      <input type="hidden" name="id_reservation" value="${reservation.id}">
    <tr>
      <td colspan="4" align="center">
          <button type="submit" name="command" value="cancel_reservation"><fmt:message key="message.button.cancel"
                                                                                         bundle="${rb}"/> </button>
      </td>
    </tr>
  </form>
</table>
<table class="general_table">
  <caption>
    <fmt:message key="message.headtable.listfreerooms" bundle="${rb}"/>
  </caption>
  <tr>
      <td><fmt:message key="message.roomnumber" bundle="${rb}"/></td>
      <td><fmt:message key="message.pricelist.type" bundle="${rb}"/></td>
      <td><fmt:message key="message.pricelist.maxpersons" bundle="${rb}"/></td>
      <td><fmt:message key="message.pricelist.rate" bundle="${rb}"/></td>
  </tr>
  <c:forEach var="room" items="${rooms}">
   <form>
     <tr>
       <td align="center">
         <input type="hidden" name="id_room" value="${room.id}">
           ${room.id}
       </td>
       <td>${room.type.type.type}</td>
       <td>${room.type.maxPersons}</td>
       <td>${room.type.rate}$</td>
       <td>
         <button type="submit" name="command" value="submit_room"><fmt:message key="message.button.submit" bundle="${rb}"/></button>
       </td>
     </tr>
   </form>
  </c:forEach>

</table>
<table class="minor_table">
  <caption>
    <fmt:message key="message.headtable.searchcreteria" bundle="${rb}"/>
  </caption>
  <tr>
  <td>${reservation.detail.login}</td>
  </tr>
 <form action="controller" method="post">
  <tr>
    <td align="center">
      <fmt:message key="message.reservation.datearrival" bundle="${rb}"/>
    </td>
    </tr>
   <tr>
    <td align="center">
      <input type="date" name="arrival_date" required value="" default=""/>
    </td>
     </tr>
   <tr>
    <td align="center">
      <fmt:message key="message.reservation.datedeparture" bundle="${rb}"/>
    </td>
   </tr>
   <tr>
    <td align="center">
      <input type="date" name="departure_date" required value=""/>
    </td>
  </tr>
   <tr>
     <td align="center"><fmt:message key="message.pricelist.maxpersons" bundle="${rb}"/></td>
   </tr>
   <tr>
     <td align="center">
       <select name="max_persons">
         <option value="1">1</option>
         <option value="2">2</option>
       </select>
     </td>
   </tr>
   <tr>
     <td align="center">
          <button type="submit" name="command" value="find_free_room"><fmt:message key="message.button.select" bundle="${rb}"/> </button></td>
   </tr>
   </form>
</table>
<a href="../../index.jsp">
  <input type="button" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
