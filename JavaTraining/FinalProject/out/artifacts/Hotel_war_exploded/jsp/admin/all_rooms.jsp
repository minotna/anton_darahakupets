<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>

<c:import url="../common/menu.jsp"/>
<html>
<head>
  <link rel="stylesheet" href="../../css/style.css">

</head>
<body>
<table cellspacing="0" class="general_table">
  <caption>
    <fmt:message key="message.headtable.rooms" bundle="${rb}"/>
  </caption>
  <tr>
    <form action="controller" method="post">
      <input type="hidden" name="command" value="add_new_room">
      <td align="center" colspan="5"><input type="submit" value="<fmt:message key="message.button.newroom" bundle="${rb}"/>"/></td>
    </form>
  </tr>
  <tr align="center" class="line_with_id">
    <td><fmt:message key="message.roomnumber" bundle="${rb}"/></td>
    <td><fmt:message key="message.pricelist.type" bundle="${rb}"/></td>
    <td><fmt:message key="message.pricelist.rate" bundle="${rb}"/></td>
    <td><fmt:message key="message.pricelist.maxpersons" bundle="${rb}"/></td>
    <td></td>
  </tr>
  <c:forEach var="room" items="${rooms}">
    <form action="controller" method="post">
      <input type="hidden" name="id_room" value="${room.id}">
      <tr>
        <td>${room.id}</td>
        <td>${room.type.type.type}</td>
        <td>${room.type.rate}</td>
        <td>${room.type.maxPersons}</td>
        <td><button type="submit" name="command" value="delete_room"><fmt:message key="message.button.delete" bundle="${rb}"/></button></td>
      </tr>
    </form>

  </c:forEach>
</table>

<a href="../../index.jsp">
  <input type="submit" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
