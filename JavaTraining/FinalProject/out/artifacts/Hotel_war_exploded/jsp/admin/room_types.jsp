<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>
<c:import url="../common/menu.jsp"/>
<html>
<head>
  <link rel="stylesheet" href="../../css/style.css">
  <title><fmt:message key="message.headtable.roomlist" bundle="${rb}"/> </title>

</head>
<body>
<c:set var="pattern_1" value="[1-9]{3}"/>
<c:set var="pattern_2" value="[1-9]{10}"/>

<table cellspacing="0" class="general_table">
  <caption><fmt:message key="message.headtable.roomlist" bundle="${rb}"/></caption>
  <tr align="center">
    <td><fmt:message key="message.pricelist.type" bundle="${rb}"/></td>
    <td><fmt:message key="message.pricelist.rate" bundle="${rb}"/></td>
    <td><fmt:message key="message.pricelist.maxpersons" bundle="${rb}"/></td>
    <td></td>
  </tr>
  <form action="controller" method="post">
    <tr>
      <td align="center">
        <select name="accommodation_type">
          <c:forEach var="accommodationType" items="${accommodationTypes}">
            <option value="${accommodationType.id}">${accommodationType.type}</option>
          </c:forEach>
        </select>
      </td>
      <td size="200" align="center"><input type="text" name="rate" required value="" pattern="${pattern_2}"></td>
      <td size="200" align="center"><input type="text" name="max_persons" required value="" pattern="${pattern_1}"></td>
      <td><button type="submit" name="command" value="add_room_type"><fmt:message key="message.button.add" bundle="${rb}"/></button></td>
    </tr>
  </form>
  <c:forEach var="type" items="${types}">
    <form action="controller" method="post">
      <input type="hidden" name="id_type" value="${type.id}">
      <tr>
        <td align="center">${type.type.type}</td>
        <td align="center">${type.rate}</td>
        <td align="center">${type.maxPersons}</td>
        <td><button type="submit" name="command" value="delete_room_type"><fmt:message key="message.button.delete" bundle="${rb}"/></button></td></td>
      </tr>
    </form>
  </c:forEach>
</table>
<a href="../../index.jsp">
  <input type="submit" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
