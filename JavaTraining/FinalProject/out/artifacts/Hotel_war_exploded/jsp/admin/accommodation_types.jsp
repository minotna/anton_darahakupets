<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>
<c:import url="../common/menu.jsp"/>
<html>
<head>
  <link rel="stylesheet" href="../../css/style.css">

</head>
<body>
<c:set var="pattern" value="[0-9a-zA-Z\s]{1,25}"/>
<div class="info_message">
  ${errorTypeDelete}
</div>
<table cellspacing="0" class="general_table">
  <caption><fmt:message key="message.headtable.roomlist" bundle="${rb}"/></caption>
  <tr align="center">
    <td><fmt:message key="message.pricelist.type" bundle="${rb}"/></td>
    <td class="errormessage">${errorMessage}</td>
  </tr>
  <form action="controller" method="post">
    <tr>
      <td align="center"><input type="text" name="accommodation_type" required value="" pattern="${pattern}"></td>
      <td><button type="submit" name="command" value="add_accommodation_type"><fmt:message key="message.button.add" bundle="${rb}"/></button></td>
    </tr>
  </form>
  <c:forEach var="type" items="${types}">
    <form action="controller" method="post">
      <input type="hidden" name="id_type" value="${type.id}">
      <tr>
        <td align="center">${type.type}</td>
        <td><button type="submit" name="command" value="delete_accommodation_type"><fmt:message key="message.button.delete" bundle="${rb}"/></button></td></td>
      </tr>
    </form>
  </c:forEach>
</table>
<a href="../../index.jsp">
  <input type="submit" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
