<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>
<html>
<head>
  <link rel="stylesheet" href="../../css/style.css">
  <title><fmt:message key="message.headtable.newuser" bundle="${rb}"/></title>
</head>
<body>
<c:set var="pattern" value="[\S ]{4,10}"/>
<form action="controller" method="post">
  <input type="hidden" name="command" value="user_registration">
  <table class="general_table">
    <caption>
      <fmt:message key="message.headtable.registration" bundle="${rb}"/>
    </caption>
    <tr>
      <td>
        <fmt:message key="message.login" bundle="${rb}"/>
      </td>
      <td><input type="text" name="login" required value="" pattern="${pattern}"/><h6>*<fmt:message
              key="message.format.info" bundle="${rb}"/></h6></td>
    </tr>
    <tr>
      <td>
        <fmt:message key="message.password" bundle="${rb}"/>
      </td>
      <td><input type="password" name="password" required value="" pattern="${pattern}"/>
        <h6>*<fmt:message key="message.format.info" bundle="${rb}"/></h6>
      </td>
    </tr>
    <tr>
      <td>
        <fmt:message key="message.confirmpassword" bundle="${rb}"/>
      </td>
      <td><input type="password" name="confirm_password" required value="" pattern="${pattern}"/>
        <h6>*<fmt:message key="message.format.info" bundle="${rb}"/></h6>
      </td>
    </tr>
    <tr>
      <td><input type="submit" value="<fmt:message key="message.button.registration" bundle="${rb}"/>"/></td>
      <td>${errorMessage}</td>
    </tr>
  </table>
</form>
<a href="../../index.jsp">
  <input type="button" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>

</body>
</html>
