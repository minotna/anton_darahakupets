<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>

<c:import url="../common/menu.jsp"/>
<html>
<head>
  <title><fmt:message key="message.headtable.menu" bundle="${rb}"/>A</title>
  <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<table class="general_table">
  <caption>
    <fmt:message key="message.headtable.menu" bundle="${rb}"/>
  </caption>
  <tr>
    <td><fmt:message key="message.welcome" bundle="${rb}"/>, ${user}</td>
  </tr>
  <tr>
    <form action="controller" method="post">
      <input type="hidden" name="command" value="create_reservation">
      <td align="center"><input type="submit" value="<fmt:message key="message.menu.reservation" bundle="${rb}"/>"/></td>
    </form>
  </tr>
  <tr>
    <form action="controller" method="post">
      <input type="hidden" name="command" value="view_reservations">
      <td align="center"><input type="submit" value="<fmt:message key="message.menu.reservations" bundle="${rb}"/>"/></td>
    </form>
  </tr>

</table>
<div class="info_message">
  ${infoMessage}
</div>
</body>
</html>
