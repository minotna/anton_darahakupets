<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="../common/menu.jsp"/>
<html>
<head>
  <title><fmt:message key="message.reservationdetail" bundle="${rb}"/> </title>
</head>
<body>
<table class="general_table">
  <caption>
    <fmt:message key="message.headtable.newreservation" bundle="${rb}"/>
  </caption>
  <tr>
    <td colspan="2" align="center">${reservation.detail.login}</td>
    <td colspan="2" align="center"><fmt:formatDate value="${reservation.detail.dateReservation}" type="date"/>
      <fmt:formatDate value="${reservation.detail.dateReservation}" type="time"/>
    </td>
  </tr>
  <tr>
    <td>${reservation.detail.roomType.maxPersons}</td>
    <td>${reservation.detail.roomType.type.type}</td>
    <td><fmt:formatDate value="${reservation.arrivalDate}" type="date"/></td>
    <td><fmt:formatDate value="${reservation.departureDate}" type="date"/></td>
  </tr>
  <c:if test="${idStatus == 4}" var="status_4" scope="page">
    <tr>
      <td colspan="4" class="info_message" align="center"><fmt:message  key="message.info.reservationcanceledbyadmin" bundle="${rb}"/></td>
    </tr>
  </c:if>
  <c:if test="${idStatus == 5}" var="status_5" scope="page">
    <tr>
      <td colspan="4" class="info_message" align="center"><fmt:message key="message.info.reservationcanceledbyclient" bundle="${rb}"/></td>
    </tr>
  </c:if>
  <c:if test="${idStatus == 1}" var="status" scope="page">
   <form action="controller" method="post">
     <input type="hidden" name="id_reservation" value="${reservation.id}">
    <tr>
      <td colspan="4" align="center">
        <button type="submit" name="command" value="cancel_reservation"><fmt:message key="message.button.cancel"
                                                                                  bundle="${rb}"/> </button>
      </td>
    </tr>
    </form>
  </c:if>
</table>
<c:if test="${idStatus == 2}" var="status_3" scope="page">
 <table class="general_table">
   <form action="controller" method="post">
     <input type="hidden" name="id_reservation" value="${reservation.id}">
     <tr>
       <td>${room.id}</td>
       <td>${room.type.type.type}</td>
       <td>${room.type.maxPersons}</td>
       <td>${room.type.rate}</td>
       <td>
         <button type="submit" name="command" value="confirm_room"><fmt:message key="message.button.confirm"
                                                                                     bundle="${rb}"/> </button>
       </td>
       <td>
         <button type="submit" name="command" value="cancel_reservation"><fmt:message key="message.button.cancel"
                                                                                     bundle="${rb}"/> </button>
       </td>
     </tr>
   </form>

 </table>

</c:if>
<c:if test="${idStatus == 3}" var="status_4" scope="page">
 <table class="general_table">
   <tr>
     <td colspan="5" align="center"><fmt:message key="message.reservation.yoursettling" bundle="${rb}"/></td>
   </tr>
   <tr>
     <td><fmt:message key="message.pricelist.type" bundle="${rb}"/></td>
     <td><fmt:message key="message.pricelist.maxpersons" bundle="${rb}"/></td>
     <td><fmt:message key="message.pricelist.rate" bundle="${rb}"/></td>
     <td><fmt:message key="message.reservation.datearrival" bundle="${rb}"/></td>
     <td><fmt:message key="message.reservation.datedeparture" bundle="${rb}"/></td>
   </tr>
   <tr>
     <td>${room.type.type.type}</td>
     <td>${room.type.maxPersons}</td>
     <td>${room.type.rate}</td>
     <td><fmt:formatDate value="${reservation.arrivalDate}" type="date"/></td>
     <td><fmt:formatDate value="${reservation.departureDate}" type="date"/></td>
   </tr>
 </table>
</c:if>

<a href="../../index.jsp">
  <input type="button" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
