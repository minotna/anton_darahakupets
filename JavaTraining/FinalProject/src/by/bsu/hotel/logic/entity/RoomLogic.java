package by.bsu.hotel.logic.entity;

import by.bsu.hotel.dao.exception.ConnectionException;
import by.bsu.hotel.dao.exception.DAOException;
import by.bsu.hotel.dao.reservation.ReservationDAO;
import by.bsu.hotel.dao.room.RoomDAO;
import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.reservation.ReservationStatus;
import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.entity.room.RoomType;
import by.bsu.hotel.logic.exception.LogicException;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by 21vek.by on 13.08.2015.
 */
public class RoomLogic {

    /**
     * @param id Id of room type
     * @return RoomType with id
     * @throws LogicException if some Connection or DAO problems
     */
    public static RoomType findRoomTypeWithId(int id) throws LogicException {
        RoomType type = null;
        List<RoomType> types = RoomLogic.takeAllRoomTypes();
        Iterator<RoomType> iterator = types.iterator();
        do {
            if (id == iterator.next().getId()) {
                type = iterator.next();
            }
        } while (id != iterator.next().getId());
        return type;
    }

    /**
     * @return List of all AccommodationTypes
     * @throws LogicException if some Connection or DAO problems
     */
    public static List<AccommodationType> takeAllAccommodationTypes() throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeAllAccommodationsTypes();
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     * @param type AccommodationType that will be written
     * @throws LogicException if some Connection or DAO problems
     */
    public static void writeAccommodationType(AccommodationType type) throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            if(!type.hasId()) {
                roomDAO.wrtieAccommodationType(type);
                int i = roomDAO.takeLastAccomodationTypeId();
                type.setId(i);
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     * @param idRoom Id of room
     * @param idRoomType Id of room type of room
     * @return true if Room written
     * @throws LogicException if some Connection or DAO problems
     */
    public static boolean writeRoom(int idRoom, int idRoomType) throws LogicException {
        boolean check = true;
        List<Room> rooms = takeAllRooms();
        for (Room room: rooms) {
            if (room.getId() == idRoom) {
                return !check;
            }
        }
        try (RoomDAO roomDAO = new RoomDAO()) {
            roomDAO.writeRoom(idRoom, idRoomType);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return check;
    }

    /**
     * @param rate rate of room type
     * @param maxPersons max persons of room type
     * @param idAccomodationType id of AccommodationType
     * @throws LogicException if some Connection or DAO problems
     */
    public static void writeRoomType(double rate, int maxPersons, int idAccomodationType) throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            roomDAO.writeRoomType(rate, maxPersons, idAccomodationType);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     * @return List of  all Rooms
     * @throws LogicException if some Connection or DAO problems
     */
    public static List<Room> takeAllRooms() throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeAllRooms();
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     * @return List of all RoomTypes
     * @throws LogicException if some Connection or DAO problems
     */
    public static List<RoomType> takeAllRoomTypes() throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeAllRoomTypesRoom();
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     * @param id id of AccommodationType
     * @return true if AccommodationType will be deleted
     * @throws LogicException if some Connection or DAO problems
     */
    public static boolean deleteAccommodationTypeWithId(int id) throws LogicException {
        boolean check = true;
        List<RoomType> roomTypes = RoomLogic.takeAllRoomTypes();
        for (RoomType roomType: roomTypes) {
            if (id==roomType.getType().getId()){
                return !check;
            }
        }
        try (RoomDAO roomDAO = new RoomDAO()) {
            roomDAO.deleteAccommodationTypeWithId(id);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return check;
    }

    /**
     * @param id of room
     * @return true if Room will be deleted
     * @throws LogicException if some Connection or DAO problems
     */
    public static boolean deleteRoomWithId(int id) throws LogicException {
        boolean check = true;
        List<Reservation> reservationList1 = ReservationLogic.takeReservationsWithStatus(ReservationStatus.RESERVED);
        List<Reservation> reservationList2 = ReservationLogic.takeReservationsWithStatus(ReservationStatus.PROCESSING);
        reservationList1.addAll(reservationList2);

        for (Reservation reservation: reservationList1) {
            if (takeRoomWithIdReservation(reservation.getId()).getId() == id) {
                return !check;
            }
        }
        try (RoomDAO roomDAO = new RoomDAO()) {
            roomDAO.deleteRoomWithId(id);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }

        return check;

    }

    /**
     *
     * @param id of room type
     * @return true if RoomType will be deleted
     * @throws LogicException if some Connection or DAO problems
     */
    public static boolean deleteRoomTypeWithId(int id) throws LogicException {
        boolean check = true;
        List<Room> rooms = RoomLogic.takeAllRooms();
        for (Room room: rooms) {
            if (id == room.getType().getId()){
                return !check;
            }
        }
        try (RoomDAO roomDAO = new RoomDAO()) {
            roomDAO.deleteRoomTypeWithId(id);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return check;
    }

    /**
     * @param id id of room type
     * @return RoomType with id
     * @throws LogicException if some Connection or DAO problems
     */
    public static  RoomType takeRoomTypeWithId(int id) throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeRoomTypeWithId(id);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     * @param idAccommodationType id of accommodation type
     * @return AccommodationType with id
     * @throws LogicException if some Connection or DAO problems
     */
    public static  AccommodationType findAccommodationTypeWithId(int idAccommodationType) throws LogicException {
        AccommodationType accommodationType = null;
        try (RoomDAO roomDAO = new RoomDAO()) {
            for(AccommodationType type: roomDAO.takeAllAccommodationsTypes()) {
                if(type.getId()==idAccommodationType) {
                    accommodationType = type;
                }
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return accommodationType;
    }

    /**
     * @param arrivalDate arrival date of client
     * @param departureDate departure date of client
     * @param maxPersons max persons of room
     * @return List of free Rooms with criteria
     * @throws LogicException if some Connection or DAO problems
     */
    public static List<Room>  findFreeRoom(Date arrivalDate, Date departureDate, int maxPersons) throws LogicException {
        List<Room> freeRooms = null;
        List<Reservation> reservations = null;
        ArrayList<Integer> numbers = new ArrayList<>();
        int idReservation = 0;

        for (Room room: findRoomsWithMaxPersons(maxPersons)) {
            numbers.add(room.getId());
        }

        try (RoomDAO roomDAO = new RoomDAO()) {
            freeRooms = findRoomsWithMaxPersons(maxPersons);
            reservations = ReservationLogic.takeReservationsWithStatus(ReservationStatus.RESERVED);
            for (Integer integer: numbers) {
                boolean checkRoom = true;
                for (Reservation reservation: reservations) {
                    idReservation = reservation.getId();
                    if ((checkRoom)&&(integer.equals(roomDAO.takeRoomWithIdReservation(idReservation).getId()))) {
                        Date dateArrival = reservation.getArrivalDate();
                        Date dateDeparture = reservation.getDepartureDate();
                        boolean a = (arrivalDate.compareTo(dateDeparture)>0);
                        boolean b = (departureDate.compareTo(dateArrival)<0);
                        if(a||b) {

                        } else {
                            Room room  = RoomLogic.findRoomWithId(integer);
                            int  i = freeRooms.indexOf(room);
                            freeRooms.remove(i);
                            checkRoom = false;
                        }
                    }
                }
            }

        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return freeRooms;
    }

    /**
     * @param id id of room
     * @return Room with id
     * @throws LogicException if some Connection or DAO problems
     */
    public static Room findRoomWithId(int id) throws LogicException {
        Room room = null;
            List<Room> rooms = takeAllRooms();
            for (Room room2: rooms) {
                if (room2.getId()==id) {
                    room = room2;
                }
            }
        return room;
    }

    /**
     * @param maxPersons max persons of room
     * @return List of Rooms with max persons
     * @throws LogicException if some Connection or DAO problems
     */
    public static List<Room> findRoomsWithMaxPersons(int maxPersons) throws LogicException {
        List<Room> rooms = new ArrayList<>();

        for (Room room: takeAllRooms()) {
            if(room.getType().getMaxPersons()==maxPersons) {
                rooms.add(room);
            }
        }
        return rooms;
    }

    /**
     * @param idReservation id of reservation
     * @return Room with reservation id
     * @throws LogicException if some Connection or DAO problems
     */
    public static Room takeRoomWithIdReservation(int idReservation) throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeRoomWithIdReservation(idReservation);
        } catch (DAOException e) {
            throw new LogicException("Connection problem", e);
        } catch (ConnectionException e) {
            throw new LogicException("DAO problem", e);
        }
    }
}
