package by.bsu.hotel.logic.entity;

import by.bsu.hotel.dao.exception.ConnectionException;
import by.bsu.hotel.dao.exception.DAOException;
import by.bsu.hotel.dao.reservation.ReservationDAO;
import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.reservation.ReservationDetail;
import by.bsu.hotel.entity.reservation.ReservationStatus;
import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.entity.room.RoomStatus;
import by.bsu.hotel.entity.user.UserRole;
import by.bsu.hotel.logic.exception.LogicException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 21vek.by on 19.08.2015.
 */
public class ReservationLogic {

    /**
     * @param idReservationDetail Id ReservationDetail
     * @param reservation Reservation to be written
     * @param status of Reservation
     * @throws LogicException if some Connection or DAO problems
     */
    public static void writeReservation(int idReservationDetail, Reservation reservation, ReservationStatus status) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            if(!reservation.hasId()) {
                reservationDAO.writeReservation(idReservationDetail,reservation,status);
                int i = reservationDAO.takeLastReservationDetailsId();
                reservation.setId(i);
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }

    }

    /**
     * @param reservationDetail that will be written
     * @param login Login of Reservation
     * @param idRoomType Id RoomType
     * @throws LogicException if some Connection or DAO problems
     */
    public static void writeReservationDetail(ReservationDetail reservationDetail, String login, int idRoomType) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            if(!reservationDetail.hasId()) {
                reservationDAO.writeReservationDetail(reservationDetail, login, idRoomType);
                int i = reservationDAO.takeLastReservationDetailsId();
                reservationDetail.setId(i);
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     * @param status of Reservation
     * @return List of Reservations with Status
     * @throws LogicException if some Connection or DAO problems
     */
    public static List<Reservation> takeReservationsWithStatus(ReservationStatus status) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()){
            return reservationDAO.takeReservationsWithStatus(status);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     *
     * @param id id of reservation
     * @return Reservation with id and with NEW status
     * @throws LogicException if some Connection or DAO problems
     */
    public static Reservation takeNewReservationWithId(int id) throws LogicException {
        Reservation reservation = null;
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            List<Reservation> reservations = reservationDAO.takeReservationsWithStatus(ReservationStatus.NEW);
            for(Reservation res: reservations) {
                if(res.getId()==id) {
                    reservation = res;
                }
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return reservation;
    }

    /**
     * Change status of reservation
     * @param idReservation id of reservation
     * @param idRoom id of room that will be submit
     * @throws LogicException if some Connection or DAO problems
     */
    public static void submitRoom(int idReservation,int idRoom) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            reservationDAO.submitRoom(idReservation, idRoom);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }

    }

    /**
     * @param login Login of reservations
     * @return List of Reservations with login
     * @throws LogicException if some Connection or DAO problems
     */
    public static List<Reservation> takeReservationsWithLogin(String login) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            return reservationDAO.takeReservationsWithLogin(login);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }

    }

    /**
     * @param id id of reservation
     * @param login login of reservation
     * @return Reservation client's reservation with id
     * @throws LogicException if some Connection or DAO problems
     */
    public static Reservation takeUserReservationWithId(int id, String login) throws LogicException {
        Reservation reservation = null;
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            List<Reservation> reservations = reservationDAO.takeReservationsWithLogin(login);
            for(Reservation res: reservations) {
                if(res.getId()==id) {
                    reservation = res;
                }
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return reservation;
    }

    /**
     *
     * @param idReservation id of reservation
     * @return int Id of reservation status
     * @throws LogicException if some Connection or DAO problems
     */
    public static int takeIdStatus(int idReservation) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            return reservationDAO.takeIdStatusReservation(idReservation);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    /**
     * Change status of reservation
     * @param role of user who canceled reservation
     * @param id id of reservation
     * @throws LogicException if some Connection or DAO problems
     */
    public static void cancelReservation(UserRole role, int id) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            switch (role) {
                case CLIENT:
                    reservationDAO.clientCancelReservation(id);
                    break;
                case ADMINISTRATOR:
                    reservationDAO.adminCancelReservation(id);
                    break;
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }
}
