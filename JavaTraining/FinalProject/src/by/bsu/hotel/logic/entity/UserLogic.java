package by.bsu.hotel.logic.entity;

import by.bsu.hotel.dao.exception.ConnectionException;
import by.bsu.hotel.dao.exception.DAOException;
import by.bsu.hotel.dao.user.UserDAO;
import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.user.User;
import by.bsu.hotel.entity.user.UserRole;
import by.bsu.hotel.logic.exception.LogicException;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;

/**
 * Created by 21vek.by on 11.08.2015.
 */
public class UserLogic {

    /**
     * @param login User Login
     * @return User with login
     * @throws LogicException if some Connection or DAO problems
     */
    public static User takeUserByLogin(String login) throws LogicException {
        User user = null;
        try (UserDAO userDAO = new UserDAO()){
            user = userDAO.findUserByLogin(login);

        } catch (ConnectionException e){
            throw new LogicException("Connection problem " + e);
        } catch ( DAOException e) {
            throw new LogicException("DAO problem" + e);
        }

        return user;
    }

    /**
     * @param login  User login
     * @param password User password
     * @param role User role
     * @return true if new User created
     * @throws LogicException if some Connection or DAO problems
     */
    public static boolean createNewUser(String login, String password, int role) throws LogicException {
        boolean flag = false;
        try (UserDAO userDAO = new UserDAO()) {
            if (userDAO.findUserByLogin(login) == null) {
                userDAO.createNewUser(login, DigestUtils.md5Hex(password), role);
                flag= true;
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem " + e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem " + e);

        }
        return flag;
    }

    /**
     * @param login User Login
     * @return true if User deleted
     * @throws LogicException if some Connection or DAO problems
     */
    public static boolean deleteUserWithLogin(String login) throws LogicException {
        User user = takeUserByLogin(login);
        boolean flag = false;

        List<Reservation> reservations = ReservationLogic.takeReservationsWithLogin(login);
        if (!reservations.isEmpty()) {
            return flag;
        }

        try (UserDAO userDAO = new UserDAO()) {
            userDAO.deleteUserWithLogin(login);
            flag = true;
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem ", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem ", e);
        }
        return flag;
    }

    /**
     * @return List of Administrators and Clients
     * @throws LogicException if some Connection or DAO problems
     */
    public static List<User> takeAllUsers() throws LogicException {
        try(UserDAO userDAO = new UserDAO()){
            return userDAO.takeClients();
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem ", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem ", e);
        }

    }
}
