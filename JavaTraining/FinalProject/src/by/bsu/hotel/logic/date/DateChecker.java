package by.bsu.hotel.logic.date;

import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by 21vek.by on 25.08.2015.
 */
public class DateChecker {
    private final static Logger LOG = Logger.getLogger(DateChecker.class);

    public static boolean checkDate(Date arrivalDate, Date departureDate ) {
        boolean checkDate = false;
        Date today = new Date();
        int a = arrivalDate.compareTo(departureDate);
        int b = today.compareTo(arrivalDate);
        if ((a < 0) && (b < 0)) {
            checkDate = true;
        }
        return checkDate;
    }
}
