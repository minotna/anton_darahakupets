package by.bsu.hotel.logic.login;

import by.bsu.hotel.entity.user.User;
import by.bsu.hotel.logic.entity.UserLogic;
import by.bsu.hotel.logic.exception.LogicException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 21vek.by on 11.08.2015.
 */
public class LoginChecker {
    private static final Logger LOG = Logger.getLogger(LoginChecker.class);
    private static final String LOGIN_AND_PASS_REGEX = "\\S{4,10}";

    public static boolean checkLogin(String enterLogin, String enterPass) throws LogicException {
        User user = UserLogic.takeUserByLogin(enterLogin);
        if (user != null) {
            return DigestUtils.md5Hex(enterPass).equals(user.getPassword());
        }else{
            return false;
        }
    }


    public static boolean checkFormat(String str){
        Pattern format = Pattern.compile(LOGIN_AND_PASS_REGEX);
        Matcher matcher = format.matcher(str);
        return matcher.matches();
    }
}
