package by.bsu.hotel.controller;

import by.bsu.hotel.connectionpool.exception.ConnectionPoolException;
import by.bsu.hotel.connectionpool.pool.ConnectionPool;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.action.ActionFactory;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class Controller extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(Controller.class);
    @Override
    public void init() throws ServletException {
        PropertyConfigurator.configure("/WEB-INF/log4j.properties");
        ConnectionPool.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public void destroy() {
        try {
            ConnectionPool.getInstance().closeConnections();
        } catch (ConnectionPoolException e) {
            LOG.error(e);
        }
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ActionCommand command = ActionFactory.defineCommand(request);
        String page = command.execute(request);
        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            HttpSession session = request.getSession();
            page = ConfigurationManager.getProperty("path.page.index");
            String message = MessageManager.getProperty("message.nullpage", session);
            request.getSession().setAttribute("nullPage", message);
            response.sendRedirect(request.getContextPath() + page);
        }

    }
}
