package by.bsu.hotel.entity.room;


import java.util.Objects;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class Room{
    private int id;
    private RoomType type;


    public Room(){

    }

    public Room(int id, RoomType type) {
        this.id = id;
        this.type = type;

    }

    public int getId() {
        return id;
    }

    public RoomType getType() {
        return type;
    }



    public void setId(int id) {
        this.id = id;
    }



    public void setType(RoomType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;

        Room room = (Room) o;

        if (getId() != room.getId()) return false;
        return !(getType() != null ? !getType().equals(room.getType()) : room.getType() != null);

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", type=" + type +
                '}';
    }
}
