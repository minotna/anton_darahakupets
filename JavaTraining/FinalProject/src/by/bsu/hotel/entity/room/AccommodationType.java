package by.bsu.hotel.entity.room;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class  AccommodationType {
    private int id;
    private String type;

    public AccommodationType() {

    }

    public AccommodationType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean hasId(){
        return !(id==0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccommodationType)) return false;

        AccommodationType that = (AccommodationType) o;

        if (getId() != that.getId()) return false;
        return getType().equals(that.getType());

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getType().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AccommodationType{" +
                "id=" + id +
                ", type='" + type  +
                '}';
    }
}
