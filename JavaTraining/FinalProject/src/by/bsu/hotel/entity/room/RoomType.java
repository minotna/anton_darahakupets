package by.bsu.hotel.entity.room;


/**
 * Created by 21vek.by on 05.08.2015.
 */
public class RoomType {
    private int id;
    private AccommodationType type;
    private double rate;
    private int maxPersons;

    public RoomType() {

    }

    public RoomType(int id, AccommodationType type, double rate, int maxPersons) {
        this.id = id;
        this.type = type;
        this.rate = rate;
        this.maxPersons = maxPersons;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AccommodationType getType() {
        return type;
    }

    public void setType(AccommodationType type) {
        this.type = type;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getMaxPersons() {
        return maxPersons;
    }

    public void setMaxPersons(int maxPersons) {
        this.maxPersons = maxPersons;
    }

    public boolean hasId(){
        return !(id==0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomType)) return false;

        RoomType roomType = (RoomType) o;

        if (getId() != roomType.getId()) return false;
        if (Double.compare(roomType.getRate(), getRate()) != 0) return false;
        if (getMaxPersons() != roomType.getMaxPersons()) return false;

        return true;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getId();
        result = 31 * result + getType().hashCode();
        temp = Double.doubleToLongBits(getRate());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + getMaxPersons();
        return result;
    }

    @Override
    public String toString() {
        return "RoomType{" +
                "id=" + id +
                ", type=" + type +
                ", rate=" + rate +
                ", maxPersons=" + maxPersons +
                '}';
    }
}
