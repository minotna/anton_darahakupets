package by.bsu.hotel.entity.account;

import java.util.GregorianCalendar;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class Account{
    private int id;
    private GregorianCalendar dateAccount;
    private AccountDetail detail;

    public Account() {
        this.dateAccount = new GregorianCalendar();

    }

    public Account(int id, AccountDetail detail) {
        this.id = id;
        this.dateAccount = new GregorianCalendar();
        this.detail = detail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GregorianCalendar getDateAccount() {
        return dateAccount;
    }

    public void setDateAccount(GregorianCalendar dateAccount) {
        this.dateAccount = dateAccount;
    }

    public AccountDetail getDetail() {
        return detail;
    }

    public void setDetail(AccountDetail detail) {
        this.detail = detail;
    }
}
