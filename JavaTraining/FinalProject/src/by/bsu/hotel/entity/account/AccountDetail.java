package by.bsu.hotel.entity.account;



/**
 * Created by 21vek.by on 05.08.2015.
 */
public class AccountDetail{
    private int id;
    private double totalSum;

    public AccountDetail() {

    }

    public AccountDetail(int id, double totalSum) {
        this.id = id;
        this.totalSum = totalSum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountDetail)) return false;

        AccountDetail that = (AccountDetail) o;

        if (getId() != that.getId()) return false;
        return Double.compare(that.getTotalSum(), getTotalSum()) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getId();
        temp = Double.doubleToLongBits(getTotalSum());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "AccountDetail{" +
                "id=" + id +
                ", totalSum=" + totalSum +
                '}';
    }
}
