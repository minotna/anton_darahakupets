package by.bsu.hotel.entity.user;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public enum UserRole {
    GUEST, CLIENT, ADMINISTRATOR
}
