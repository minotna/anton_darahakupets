package by.bsu.hotel.entity.reservation;

import by.bsu.hotel.entity.room.RoomType;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class ReservationDetail {
    private int id;
    private String login;
    private Date dateReservation;
    private RoomType roomType;

    public ReservationDetail() {
        dateReservation = new Date();
    }

    public ReservationDetail(int id, String login, RoomType type) {
        this.id = id;
        this.login = login;
        this.roomType = type;
        this.dateReservation = new Date();
    }

    public int getId() {
        return id;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType type) {
        this.roomType = type;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(Date dateReservation) {
        this.dateReservation = dateReservation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReservationDetail)) return false;

        ReservationDetail that = (ReservationDetail) o;

        if (getId() != that.getId()) return false;
        if (!getLogin().equals(that.getLogin())) return false;
        if (!getDateReservation().equals(that.getDateReservation())) return false;
        return getRoomType().equals(that.getRoomType());

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getLogin().hashCode();
        result = 31 * result + getDateReservation().hashCode();
        result = 31 * result + getRoomType().hashCode();
        return result;
    }



    @Override
    public String toString() {
        return "ReservationDetail{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", dateReservation=" + dateReservation +
                ", roomType=" + roomType +
                '}';
    }

    public boolean hasId(){
        return !(id==0);
    }
}
