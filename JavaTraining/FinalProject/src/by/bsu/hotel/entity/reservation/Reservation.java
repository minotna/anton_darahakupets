package by.bsu.hotel.entity.reservation;

import by.bsu.hotel.entity.room.Room;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * Created by 21vek.by on 05.08.2015.
 */
public class Reservation{
    private int id;
    private ReservationDetail detail;
    private Date arrivalDate;
    private Date departureDate;

    public Reservation() {

    }

    public Reservation(int id, ReservationDetail detail, Room room, Date arrivalDate, Date departureDate) {
        this.id = id;
        this.detail = detail;
        this.arrivalDate = arrivalDate;
        this.departureDate = departureDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ReservationDetail getDetail() {
        return detail;
    }

    public void setDetails(ReservationDetail details) {
        this.detail = details;
    }


    public Date getArrivalDate() {
        return this.arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getDepartureDate() {
        return this.departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reservation)) return false;

        Reservation that = (Reservation) o;

        if (getId() != that.getId()) return false;
        if (!getDetail().equals(that.getDetail())) return false;
        if (!getArrivalDate().equals(that.getArrivalDate())) return false;

        return true;

    }

    public boolean hasId(){
        return !(id==0);
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getDetail().hashCode();
        result = 31 * result + getArrivalDate().hashCode();
        result = 31 * result + getDepartureDate().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", detail=" + detail +
                ", arrivalDate=" + arrivalDate +
                ", departureDate=" + departureDate +
                '}';
    }
}
