package by.bsu.hotel.connectionpool.pool;

import by.bsu.hotel.connectionpool.connectionfactory.ConnectionFactory;
import by.bsu.hotel.connectionpool.exception.ConnectionPoolException;
import by.bsu.hotel.connectionpool.implconnection.ImplConnection;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class ConnectionPool {
    private final static Logger LOG = Logger.getLogger(ConnectionPool.class);
    private static final String DB_PROPERTIES = "resources/property/database";
    private ArrayBlockingQueue<ImplConnection> connectionQueue;

    private  ConnectionPool() throws ConnectionPoolException {
        ResourceBundle resource = ResourceBundle.getBundle(DB_PROPERTIES);
        String poolsize = resource.getString("db.poolsize");
        int pool_size = Integer.parseInt(poolsize);
        connectionQueue = new ArrayBlockingQueue<>(pool_size);
            for (int i = 0; i < pool_size; i++) {
                ImplConnection connection = ConnectionFactory.getInstance();
                connectionQueue.offer(connection);
            }
        }



    /**
     * close all ImplConnection in ConnectionPool
     * @throws ConnectionPoolException
     */
    public void closeConnections() throws ConnectionPoolException {
        for (ImplConnection connection : connectionQueue) {
            try {
                connection.close();

            } catch (SQLException e) {
                throw new ConnectionPoolException();
            }
        }
        LOG.info("Connection pool is closed");
    }


private static class ConnectionPoolHolder {
    private final static ConnectionPool INSTANCE;

    static {
        try {
            INSTANCE = new ConnectionPool();
        } catch (ConnectionPoolException e) {
            LOG.error(e + "Connection pool is not created");
            throw new ExceptionInInitializerError(e + "Connection pool is not created");
        }
    }
}

    public static ConnectionPool getInstance() {
        return ConnectionPoolHolder.INSTANCE;
    }

    /**
     * @return ImplConnection
     * @throws ConnectionPoolException
     * @see ImplConnection
     */
    public ImplConnection takeConnection() throws ConnectionPoolException {
        ImplConnection connection = null;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(e);
        }
        return connection;
    }


    /**
     * return ImplConnection to Connection Pool
     * @param proxyConnection is ImplConnection
     * @return true if ImplConnection is offer to ConnectionPool
     */
    public boolean offerConnection(ImplConnection proxyConnection) {
        return connectionQueue.offer(proxyConnection);
    }
}
