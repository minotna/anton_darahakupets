package by.bsu.hotel.connectionpool.connectionfactory;

import by.bsu.hotel.connectionpool.exception.ConnectionPoolException;
import by.bsu.hotel.connectionpool.implconnection.ImplConnection;
import com.mysql.jdbc.Driver;
import org.apache.log4j.Logger;


import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class ConnectionFactory {
    static {
        try {
            DriverManager.registerDriver(new Driver());
        } catch (SQLException e) {
            throw new ExceptionInInitializerError("Database driver is not registered");
        }
    }
    private static final Logger LOG = Logger.getLogger(ConnectionFactory.class);
    private static final String DB_PROPERTIES = "resources/property/database";

    /**
     * @return ProxyConnection
     * @throws ConnectionPoolException
     * @see ImplConnection
     */
    public static ImplConnection getInstance() throws ConnectionPoolException {
        Properties properties = new Properties();
        ResourceBundle resource = ResourceBundle.getBundle(DB_PROPERTIES);
        String url = resource.getString("db.url");
        String user = resource.getString("db.user");
        String pass = resource.getString("db.password");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url,user,pass);
        } catch (SQLException e) {
            throw new ConnectionPoolException(e);
        }
        return new ImplConnection(connection);
    }

}
