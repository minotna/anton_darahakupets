package by.bsu.hotel.dao.abstractdao;

/**
 * Created by 21vek.by on 11.08.2015.
 */

import by.bsu.hotel.connectionpool.exception.ConnectionPoolException;
import by.bsu.hotel.connectionpool.implconnection.ImplConnection;
import by.bsu.hotel.connectionpool.pool.ConnectionPool;
import by.bsu.hotel.dao.exception.ConnectionException;

/**
 *   Abstract class (AutoCloseable)
 *   Each class in DAO Layer extends it
 *   @see ImplConnection
 */
public abstract class AbstractDAO implements AutoCloseable{
    protected ImplConnection connection;

    public AbstractDAO() throws ConnectionException {
        try {
            connection = ConnectionPool.getInstance().takeConnection();
        } catch (ConnectionPoolException e) {
            throw new ConnectionException(e);
        }
    }

    /**
     * return ImplConnection to ConnectionPool
     */
    public void close(){
        ConnectionPool.getInstance().offerConnection(this.connection);
    }

}
