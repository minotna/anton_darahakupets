package by.bsu.hotel.dao.room;

import by.bsu.hotel.dao.abstractdao.AbstractDAO;
import by.bsu.hotel.dao.exception.ConnectionException;
import by.bsu.hotel.dao.exception.DAOException;
import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.entity.room.RoomStatus;
import by.bsu.hotel.entity.room.RoomType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 21vek.by on 13.08.2015.
 */
public class RoomDAO extends AbstractDAO {
    private final static String TAKE_ALL_ROOMS = "SELECT rooms.idRoom, roomtypes.idRoomType, accommodationtypes.idAccommodationType, accommodationtypes.type, roomtypes.maxPersons, roomtypes.roomRate \n" +
                                            "FROM rooms \n" +
                                            "JOIN roomtypes ON roomtypes.idRoomType = rooms.roomtypes_idRoomType \n" +
                                            "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType ORDER BY rooms.idRoom";
    private static final String TAKE_ALL_ROOM_TYPES = "SELECT roomtypes.idRoomType, roomtypes.roomRate, roomtypes.maxPersons, accommodationtypes.idAccommodationType, accommodationtypes.type \n" +
                                             "FROM roomtypes \n" +
                                             "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType ORDER BY roomtypes.idRoomType";
    private static final String FIND_ROOMTYPE_WITH_ID = "SELECT roomtypes.idRoomType,accommodationtypes.idAccommodationType, accommodationtypes.type, roomtypes.maxPersons, roomtypes.roomRate \n" +
                                                        "FROM roomtypes \n" +
                                                         "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType \n" +
                                                        "WHERE roomtypes.idRoomType=? ";
    private static final String TAKE_ROOM_WITH_IDRESERVATION = "SELECT rooms.idRoom, roomtypes.idRoomType, roomtypes.maxPersons, roomtypes.roomRate, accommodationtypes.idAccommodationType, accommodationtypes.type\n" +
            "FROM rooms\n" +
            "JOIN reservations ON rooms.idRoom = reservations.rooms_idRoom\n" +
            "JOIN roomtypes ON roomtypes.idRoomType = rooms.roomtypes_idRoomType\n" +
            "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType\n" +
            "WHERE reservations.idReservation=? " ;
    private static final String CREATE_NEW_ACCOMMODATION_TYPE = "INSERT INTO accommodationtypes(type) VALUES(?)";
    private static final String CREATE_NEW_ROOM_TYPE = "INSERT INTO roomtypes(roomRate, maxPersons, accommodationtypes_idAccommodationType) VALUES (?,?,?)";
    private static final String CREATE_NEW_ROOM =  "INSERT INTO rooms(idRoom, roomtypes_idRoomType) VALUES (?,?)";
    private static final String FIND_LAST_ROOM_TYPE_ID = "SELECT LAST_INSERT_ID() FROM roomtypes";
    private static final String FIND_LAST_ACCOMODATION_TYPE_ID = "SELECT LAST_INSERT_ID() FROM accommodationtypes";
    private static final String TAKE_ALL_ACCOMMODATIONS_TYPES = "SELECT accommodationtypes.idAccommodationType, accommodationtypes.type\n" +
            "FROM accommodationtypes\n" +
            "ORDER BY accommodationtypes.idAccommodationType ";
    private static final String DELETE_ACCOMMODATION_TYPE = "DELETE FROM accommodationtypes WHERE accommodationtypes.idAccommodationType=? ";
    private static final String DELETE_ROOM_TYPE = "DELETE FROM roomtypes WHERE roomtypes.idRoomType=?";
    private static final String DELETE_ROOM = "DELETE FROM rooms WHERE rooms.idRoom=?";


    public RoomDAO() throws ConnectionException {
    }

    /**
     * Delete room from database
     * @param id of Room
     * @throws DAOException when SQLException in DAOLayer
     */
    public void deleteRoomWithId(int id) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_ROOM)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * Delete room type from database
     * @param id of RoomType
     * @throws DAOException when SQLException in DAOLayer
     */
    public void deleteRoomTypeWithId(int id) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_ROOM_TYPE)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * Delete accommodation type from database
     * @param id of AccommodationType
     * @throws DAOException when SQLException in DAOLayer
     */
    public void deleteAccommodationTypeWithId(int id) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_ACCOMMODATION_TYPE)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * @param type Accommodation type that will write to database
     * DAOException when SQLException in DAOLayer
     */
    public void wrtieAccommodationType(AccommodationType type) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_NEW_ACCOMMODATION_TYPE, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, type.getType());
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * @return int id last in database table accommodationtypes
     * @throws DAOException when SQLException in DAOLayer
     */
    public int takeLastAccomodationTypeId() throws DAOException {
        int id = 0;
        try (PreparedStatement statement = connection.prepareStatement(FIND_LAST_ACCOMODATION_TYPE_ID)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return id;
    }

    /**
     * @return int id last in database table roomtypes
     * @throws DAOException
     */
    public int takeLastRoomTypeId() throws DAOException {
        int id = 0;
        try (PreparedStatement statement = connection.prepareStatement(FIND_LAST_ROOM_TYPE_ID)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return id;
    }


    /**
     * @return List of all AccommodationTypes
     * @throws DAOException when SQLException in DAOLayer
     */
    public List<AccommodationType> takeAllAccommodationsTypes() throws DAOException {
        List<AccommodationType> types = new ArrayList<>();
        AccommodationType type = null;
        try (PreparedStatement statement = connection.prepareStatement(TAKE_ALL_ACCOMMODATIONS_TYPES)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                type = new AccommodationType();
                type.setId(resultSet.getInt(1));
                type.setType(resultSet.getString(2));
                types.add(type);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return types;
    }

    /**
     * @param rate of room type
     * @param maxPersons of room type
     * @param idAccomodationType id AccommodationType of room type is foreign key in database
     * @throws DAOException when SQLException in DAOLayer
     */
    public void writeRoomType(double rate, int maxPersons, int idAccomodationType) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_NEW_ROOM_TYPE)) {
            statement.setDouble(1, rate);
            statement.setInt(2, maxPersons);
            statement.setInt(3, idAccomodationType);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * @param idRoom id of room that will be write in database
     * @param idRoomType id RoomType of room is foreign key in database
     * @throws DAOException when SQLException in DAOLayer
     */
    public void writeRoom(int idRoom, int idRoomType) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_NEW_ROOM)) {
            statement.setInt(1, idRoom);
            statement.setInt(2, idRoomType);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * @return List of all Rooms in database
     * @throws DAOException when SQLException in DAOLayer
     */
    public List<Room> takeAllRooms() throws DAOException {
        List<Room> rooms = new ArrayList<>();
        Room room = null;
        RoomType roomType = null;
        AccommodationType  accommodationType= null;
        try (PreparedStatement statement = connection.prepareStatement(TAKE_ALL_ROOMS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                room = new Room();
                room.setId(resultSet.getInt(1));
                roomType = new RoomType();
                roomType.setId(resultSet.getInt(2));
                accommodationType = new AccommodationType();
                accommodationType.setId(resultSet.getInt(3));
                accommodationType.setType(resultSet.getString(4));
                roomType.setType(accommodationType);
                roomType.setMaxPersons(resultSet.getInt(5));
                roomType.setRate(resultSet.getDouble(6));
                room.setType(roomType);
                rooms.add(room);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return rooms;

    }

    /**
     * Return all room types
     * @return List of all RoomTypes
     * @throws DAOException
     */

    public List<RoomType> takeAllRoomTypesRoom() throws DAOException {
        List<RoomType> types = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(TAKE_ALL_ROOM_TYPES)) {
            ResultSet resultSet = statement.executeQuery();
            RoomType roomType = null;
            while (resultSet.next()) {
                roomType = new RoomType();
                AccommodationType accommodationType = new AccommodationType();
                accommodationType.setId(resultSet.getInt(4));
                accommodationType.setType(resultSet.getString(5));
                roomType.setType(accommodationType);
                roomType.setId(resultSet.getInt(1));
                roomType.setRate(resultSet.getDouble(2));
                roomType.setMaxPersons(resultSet.getInt(3));
                types.add(roomType);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return types;
    }

    /**
     * Read room type in database
     * @param id of room type that should be read
     * @return RoomType from database by id
     * @throws DAOException
     */
    public RoomType takeRoomTypeWithId(int id) throws DAOException {
        RoomType roomType = new RoomType();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ROOMTYPE_WITH_ID)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                AccommodationType accommodationType = new AccommodationType();
                accommodationType.setId(resultSet.getInt(2));
                accommodationType.setType(resultSet.getString(3));
                roomType.setType(accommodationType);
                roomType.setId(resultSet.getInt(1));
                roomType.setRate(resultSet.getDouble(5));
                roomType.setMaxPersons(resultSet.getInt(4));
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return roomType;
    }

    /**
     * Return Room of reservation by its id
     * @param idReservation reservation id
     * @return Room
     * @throws DAOException
     */
    public Room takeRoomWithIdReservation(int idReservation) throws DAOException {
        Room room = new Room();
        RoomType  roomType = new RoomType();
        AccommodationType  accommodationType = new AccommodationType();
        try (PreparedStatement statement = connection.prepareStatement(TAKE_ROOM_WITH_IDRESERVATION)) {
            statement.setInt(1, idReservation);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                room.setId(resultSet.getInt(1));
                roomType.setId(resultSet.getInt(2));
                roomType.setMaxPersons(resultSet.getInt(3));
                roomType.setRate(resultSet.getDouble(4));
                accommodationType.setId(resultSet.getInt(5));
                accommodationType.setType(resultSet.getString(6));
                roomType.setType(accommodationType);
                room.setType(roomType);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }

        return room;
    }



}
