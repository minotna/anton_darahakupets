package by.bsu.hotel.dao.reservation;

import by.bsu.hotel.dao.abstractdao.AbstractDAO;
import by.bsu.hotel.dao.exception.ConnectionException;
import by.bsu.hotel.dao.exception.DAOException;
import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.reservation.ReservationDetail;
import by.bsu.hotel.entity.reservation.ReservationStatus;
import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.entity.room.RoomStatus;
import by.bsu.hotel.entity.room.RoomType;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by 21vek.by on 16.08.2015.
 */
public class ReservationDAO extends AbstractDAO {
    private final static Logger LOG = Logger.getLogger(ReservationDAO.class);

    private final static String CREATE_NEW_RESERVATION = "INSERT INTO reservations(reservationdetails_idReservationDetail, arrivalDate, departureDate, reservationstatuses_idReservationStatus ) VALUES (?,?,?,?)";
    private final static String CREATE_NEW_RESERVATIONDETAIL = "INSERT INTO reservationdetails(users_login, dateReservation, roomtypes_idRoomType) VALUES (?,?,?)";
    private static final String FIND_LAST_ID = "SELECT LAST_INSERT_ID() FROM reservationdetails";
    private static final String FIND_RESERVATIONS_WITH_STATUS = "SELECT reservations.idReservation, reservationdetails.idReservationDetail,reservationdetails.dateReservation, reservationdetails.users_login, roomtypes.idRoomType, accommodationtypes.idAccommodationType,accommodationtypes.type, roomtypes.maxPersons,roomtypes.roomRate,  reservations.arrivalDate, reservations.departureDate\n" +
            "FROM reservations\n" +
            "JOIN reservationdetails ON reservationdetails.idReservationDetail = reservations.reservationdetails_idReservationDetail\n" +
            "JOIN reservationstatuses ON reservationstatuses.idReservationStatus = reservations.reservationstatuses_idReservationStatus\n" +
            "JOIN roomtypes ON roomtypes.idRoomType = reservationdetails.roomtypes_idRoomType\n" +
            "  JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType\n" +
            "WHERE reservations.reservationstatuses_idReservationStatus=? ORDER BY reservationdetails.dateReservation " ;
    private static final String FIND_RESERVATION_WITH_ID =  " SELECT reservations.idReservation, reservationdetails.idReservationDetail, reservationdetails.dateReservation,\n" +
            "  roomtypes.idRoomType, accommodationtypes.idAccommodationType, accommodationtypes.type, roomtypes.maxPersons, roomtypes.roomRate, reservationdetails.users_login, reservations.arrivalDate, reservations.departureDate\n" +
            "FROM reservations\n" +
            "JOIN reservationdetails ON reservationdetails.idReservationDetail = reservations.reservationdetails_idReservationDetail\n" +
            "JOIN roomtypes ON roomtypes.idRoomType = reservationdetails.roomtypes_idRoomType\n" +
            "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType\n" +
            "WHERE reservations.idReservation=?";
    private static final String SUBMIT_ROOM = "UPDATE reservations SET reservations.reservationstatuses_idReservationStatus=2, reservations.rooms_idRoom=? WHERE reservations.idReservation=? ";
    private static final String FIND_RESERVATIONS_WITH_LOGIN = "SELECT reservations.idReservation, reservationdetails.idReservationDetail,reservationdetails.dateReservation, reservationdetails.users_login, roomtypes.idRoomType, accommodationtypes.idAccommodationType,accommodationtypes.type, roomtypes.maxPersons,roomtypes.roomRate,  reservations.arrivalDate, reservations.departureDate\n" +
            "FROM reservations\n" +
            "  JOIN reservationdetails ON reservationdetails.idReservationDetail = reservations.reservationdetails_idReservationDetail\n" +
            "  JOIN roomtypes ON roomtypes.idRoomType = reservationdetails.roomtypes_idRoomType\n" +
            "  JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType\n" +
            "WHERE reservationdetails.users_login=? ORDER BY reservationdetails.dateReservation";
    private static final String TAKE_ID_STATUS_RESERVATION = "SELECT reservations.reservationstatuses_idReservationStatus\n" +
            "FROM reservations\n" +
            "WHERE reservations.idReservation=?" ;
    private static final String ADMIN_CANCEL_RESERVATION = "UPDATE reservations SET reservations.reservationstatuses_idReservationStatus=4  WHERE reservations.idReservation=?";
    private static final String CLIENT_CANCEL_RESERVATION = "UPDATE reservations SET reservations.reservationstatuses_idReservationStatus=5 WHERE reservations.idReservation=?";


    public ReservationDAO() throws ConnectionException {
    }

    /**
     * Return all reservations with login
     * @param login login
     * @return List of Reservations
     * @throws DAOException when SQLException in DAOLayer
     */
    public List<Reservation> takeReservationsWithLogin(String login) throws DAOException {
        List<Reservation> reservations = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_RESERVATIONS_WITH_LOGIN)) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            Reservation reservation = null;
            ReservationDetail detail = null;
            RoomType roomType = null;
            AccommodationType accommodationType = null;
            SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            while (resultSet.next()) {
                reservation = new Reservation();
                detail = new ReservationDetail();
                roomType = new RoomType();
                accommodationType = new AccommodationType();
                reservation.setId(resultSet.getInt(1));
                detail.setId(resultSet.getInt(2));

                String strDate = resultSet.getString(3);
                Date date = smf.parse(strDate);
                detail.setDateReservation(date);
                detail.setLogin(resultSet.getString(4));


                roomType.setId(resultSet.getInt(5));
                accommodationType.setId(resultSet.getInt(6));
                accommodationType.setType(resultSet.getString(7));
                roomType.setType(accommodationType);

                roomType.setMaxPersons(resultSet.getInt(8));
                roomType.setRate(resultSet.getDouble(9));
                detail.setRoomType(roomType);

                strDate = resultSet.getString(10);
                date = sm.parse(strDate);
                reservation.setArrivalDate(date);

                strDate = resultSet.getString(11);
                date = sm.parse(strDate);
                reservation.setDepartureDate(date);
                reservation.setDetails(detail);

                reservations.add(reservation);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        } catch (ParseException e) {
            LOG.error(e);
        }
        return reservations;
    }

    /**
     * Change status of reservation and write room
     * @param idReservation reservation id
     * @param idRoom room id
     * @throws DAOException when SQLException in DAOLayer
     */
    public void submitRoom(int idReservation, int idRoom) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SUBMIT_ROOM)) {
            statement.setInt(1, idRoom);
            statement.setInt(2, idReservation);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * Change status of Reservation
     * @param idReservation reservation
     * @throws DAOException when SQLException in DAOLayer
     */
    public void adminCancelReservation(int idReservation) throws DAOException {
        execute(idReservation, ADMIN_CANCEL_RESERVATION);
    }

    /**
     * Change status of Reservation
     * @param idReservation reservation
     * @throws DAOException when SQLException in DAOLayer
     */
    public void clientCancelReservation(int idReservation) throws DAOException {
        execute(idReservation, CLIENT_CANCEL_RESERVATION);
    }

    /**
     * @param id id reservation that will be changed
     * @param state statement will execute
     * @throws DAOException when SQLException in DAOLayer
     */
    public void execute(int id, String state) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(state)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * @param idReservationDetail foreign key in database
     * @param reservation reservation that should be written
     * @param status status of reservation
     * @throws DAOException when SQLException in DAOLayer
     */
    public void writeReservation(int idReservationDetail, Reservation reservation, ReservationStatus status) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_NEW_RESERVATION, Statement.RETURN_GENERATED_KEYS)) {
            SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
            statement.setInt(1, idReservationDetail);
            statement.setString(2, sm.format(reservation.getArrivalDate()));
            statement.setString(3, sm.format(reservation.getDepartureDate()));
            statement.setInt(4, status.ordinal() + 1);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * @param reservationDetail will write to database
     * @param login with login
     * @throws DAOException when SQLException in DAOLayer
     */
    public void writeReservationDetail(ReservationDetail reservationDetail, String login, int idRoomType) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_NEW_RESERVATIONDETAIL, Statement.RETURN_GENERATED_KEYS)) {
            SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            statement.setString(2, sm.format(reservationDetail.getDateReservation()));
            statement.setString(1, login);
            statement.setInt(3, idRoomType);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
    }

    /**
     * @return int id last in database table reservationdetails
     * @throws DAOException when SQLException in DAOLayer
     */
    public int takeLastReservationDetailsId() throws DAOException {
        int id = 0;
        try (PreparedStatement statement = connection.prepareStatement(FIND_LAST_ID)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return id;
    }

    /**
     * Return all reservations with status
     * @param status reservation status
     * @return List of reservations
     * @throws DAOException when SQLException in DAOLayer
     */
    public List<Reservation> takeReservationsWithStatus(ReservationStatus status) throws DAOException {
        List<Reservation> reservations = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_RESERVATIONS_WITH_STATUS)) {
            statement.setInt(1, status.ordinal() + 1);
            ResultSet resultSet = statement.executeQuery();
            Reservation reservation = null;
            ReservationDetail detail = null;
            RoomType roomType = null;
            AccommodationType accommodationType = null;
            SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            while (resultSet.next()) {
                reservation = new Reservation();
                detail = new ReservationDetail();
                roomType = new RoomType();
                accommodationType = new AccommodationType();
                reservation.setId(resultSet.getInt(1));
                detail.setId(resultSet.getInt(2));

                String strDate = resultSet.getString(3);
                Date date = smf.parse(strDate);
                detail.setDateReservation(date);
                detail.setLogin(resultSet.getString(4));


                roomType.setId(resultSet.getInt(5));
                accommodationType.setId(resultSet.getInt(6));
                accommodationType.setType(resultSet.getString(7));
                roomType.setType(accommodationType);

                roomType.setMaxPersons(resultSet.getInt(8));
                roomType.setRate(resultSet.getDouble(9));
                detail.setRoomType(roomType);

                strDate = resultSet.getString(10);
                date = sm.parse(strDate);
                reservation.setArrivalDate(date);

               strDate = resultSet.getString(11);
                date = sm.parse(strDate);
                reservation.setDepartureDate(date);
                reservation.setDetails(detail);

                reservations.add(reservation);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem" + e);
        } catch (ParseException e) {
            LOG.error(e);
        }
        return reservations;
    }

    /**
     * Return reservation with status ReservationStatus.NEW by its id
     * @param id reservation id
     * @return Reservation
     * @throws DAOException when SQLException in DAOLayer
     */
    public Reservation takeNewReservationWithId(int id) throws DAOException {
        Reservation reservation = new Reservation();
        try (PreparedStatement statement = connection.prepareStatement(FIND_RESERVATION_WITH_ID)){
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            ReservationDetail detail = new ReservationDetail();
            RoomType roomType = new RoomType();
            AccommodationType accommodationType = new AccommodationType();

            SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            while (resultSet.next()) {
                reservation.setId(resultSet.getInt(1));
                detail.setId(resultSet.getInt(2));

                String strDate = resultSet.getString(3);
                Date date = smf.parse(strDate);
                detail.setDateReservation(date);

                roomType.setId(resultSet.getInt(4));

                accommodationType.setId(resultSet.getInt(5));
                accommodationType.setType(resultSet.getString(6));

                roomType.setType(accommodationType);
                roomType.setMaxPersons(resultSet.getInt(7));
                roomType.setRate(resultSet.getDouble(8));

                detail.setRoomType(roomType);

                detail.setLogin(resultSet.getString(9));

                strDate = resultSet.getString(10);
                date = sm.parse(strDate);
                reservation.setArrivalDate(date);

                strDate = resultSet.getString(11);
                date = sm.parse(strDate);
                reservation.setDepartureDate(date);
            }

        } catch (SQLException e) {
            throw new DAOException("SQL problem" + e);
        } catch (ParseException e) {
            LOG.error(e);
        }
        return reservation;
    }

    /**
     * Return id status of reservation
     * @param idReservation reservation id
     * @return int
     * @throws DAOException when SQLException in DAOLayer
     */
    public int takeIdStatusReservation(int idReservation) throws DAOException {
        int idStatus = 0;
        try (PreparedStatement statement = connection.prepareStatement(TAKE_ID_STATUS_RESERVATION)) {
            statement.setInt(1, idReservation);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                idStatus = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            throw new DAOException("SQL problem" + e);
        }
        return idStatus;
    }
}

