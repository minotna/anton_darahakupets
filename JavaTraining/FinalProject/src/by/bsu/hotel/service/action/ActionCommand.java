package by.bsu.hotel.service.action;


import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public interface ActionCommand {

    String execute(HttpServletRequest request);

    default String exeptionHelp(HttpServletRequest request, Exception e, Logger logger) {
        logger.error(e);
        request.setAttribute("errorMessage", e);
        return ConfigurationManager.getProperty("path.page.error");
    }
}
