package by.bsu.hotel.service.action;


import by.bsu.hotel.service.command.common.EmptyCommand;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class ActionFactory {
    private static final Logger LOG = Logger.getLogger(ActionFactory.class);


    public static ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = null;
        String action = request.getParameter("command");
        HttpSession session = request.getSession();
        if(action == null || action.isEmpty()) {
            return new EmptyCommand();
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            LOG.error(e);
            String message = MessageManager.getProperty("message.wrongaction", session);
            request.setAttribute("wrongAction", action + message);
        }
        return current;
    }
}
