package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 02.09.2015.
 */
public class AddAccommodationTypeCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(AddAccommodationTypeCommand.class);
    /**
     * Admin command: add accommodation type
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        String type = request.getParameter("accommodation_type");
        AccommodationType accommodationType = new AccommodationType();
        accommodationType.setType(type);
        List<AccommodationType> types = null;
        try {
            RoomLogic.writeAccommodationType(accommodationType);
            types = RoomLogic.takeAllAccommodationTypes();
            session.setAttribute("types", types);
            page = ConfigurationManager.getProperty("path.page.accommodation_types");
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }

        return page;
    }
}
