package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 21vek.by on 12.08.2015.
 */
public class UserCreateCommand implements ActionCommand{
    /**
     * Admin command: return page to create new user
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        return ConfigurationManager.getProperty("path.page.new_user");
    }
}
