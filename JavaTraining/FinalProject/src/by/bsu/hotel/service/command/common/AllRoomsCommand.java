package by.bsu.hotel.service.command.common;

import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by 21vek.by on 13.08.2015.
 */
public class AllRoomsCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(AllRoomsCommand.class);

    /**
     * Used to get page with all rooms from database
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Room> rooms = null;
        try {
            rooms = RoomLogic.takeAllRooms();
            page = ConfigurationManager.getProperty("path.page.all_rooms");
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        request.getSession().setAttribute("rooms", rooms);

        return page;
    }
}
