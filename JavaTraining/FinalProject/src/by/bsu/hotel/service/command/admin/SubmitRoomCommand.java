package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.logic.entity.ReservationLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by 21vek.by on 29.08.2015.
 */
public class SubmitRoomCommand implements ActionCommand {
    private final static Logger LOG = Logger.getLogger(SubmitRoomCommand.class);

    /**
     * Admin command: change status of reservation and write room
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Reservation reservation = (Reservation)session.getAttribute("reservation");

        String id_room = request.getParameter("id_room");
        int idRoom = Integer.parseInt(id_room);
        try {
            ReservationLogic.submitRoom(reservation.getId(),idRoom);
            page = ConfigurationManager.getProperty("path.page.admin_menu");
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }

        return page;

    }
}
