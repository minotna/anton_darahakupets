package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 02.09.2015.
 */
public class DeleteAccommodationTypeCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(DeleteAccommodationTypeCommand.class);
    /**
     * Admin command: delete accommodation type
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<AccommodationType> types = null;
        HttpSession session = request.getSession();
        String id = (String) request.getParameter("id_type");
        int idType = Integer.parseInt(id);
        try {
            if (RoomLogic.deleteAccommodationTypeWithId(idType)) {
                types = RoomLogic.takeAllAccommodationTypes();
                session.setAttribute("types",types);
                page = ConfigurationManager.getProperty("path.page.accommodation_types");
            } else {

                String message = MessageManager.getProperty("message.error.deletetype", session);
                request.setAttribute("errorMessage", message);
                page = ConfigurationManager.getProperty("path.page.accommodation_types");
                types = RoomLogic.takeAllAccommodationTypes();
                session.setAttribute("types",types);
            }

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        return page;
    }
}
