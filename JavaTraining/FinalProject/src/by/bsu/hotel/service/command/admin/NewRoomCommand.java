package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.RoomType;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 06.09.2015.
 */
public class NewRoomCommand implements ActionCommand {
    private final static Logger LOG = Logger.getLogger(NewRoomCommand.class);

    /**
     * Admin command: return page to create a new room
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        List<RoomType> types = null;
        try {
            types  = RoomLogic.takeAllRoomTypes();
            session.setAttribute("types", types);
            page = ConfigurationManager.getProperty("path.page.new_room");
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        return page;
    }
}
