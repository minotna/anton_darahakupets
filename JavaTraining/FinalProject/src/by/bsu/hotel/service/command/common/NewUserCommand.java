package by.bsu.hotel.service.command.common;

import by.bsu.hotel.logic.entity.UserLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.logic.login.LoginChecker;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by 21vek.by on 11.08.2015.
 */
public class NewUserCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(NewUserCommand.class);
    private HttpServletRequest request;

    /**
     * Admin command: create user
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        this.request = request;
        HttpSession session = request.getSession();
        String page = null;
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirm_password");
        int role = Integer.parseInt(request.getParameter("user_role"));

        try {
            if(password.equals(confirmPassword)) {
                if (LoginChecker.checkFormat(login) && LoginChecker.checkFormat(password)) {
                    if (UserLogic.createNewUser(login, password, role)) {
                        String message = MessageManager.getProperty("message.accountcreate", session);
                        request.setAttribute("infoMessage", message);
                        page = ConfigurationManager.getProperty("path.page.index");
                    } else {
                        request.setAttribute("errorMessage", "Account is not created, id or login exist ");
                        page = ConfigurationManager.getProperty("path.page.registration");

                    }
                } else {
                    String message = MessageManager.getProperty("message.loginformaterror", session);
                    request.setAttribute("errorMessage", message);
                    page = ConfigurationManager.getProperty("path.page.registration");

                }
            }else{
                String message = MessageManager.getProperty("message.confirmpassworderror", session);
                request.setAttribute("errorMessage", message);
                page = ConfigurationManager.getProperty("path.page.registration");
            }

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        return page;
    }
}
