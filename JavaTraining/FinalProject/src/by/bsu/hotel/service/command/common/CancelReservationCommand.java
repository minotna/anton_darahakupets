package by.bsu.hotel.service.command.common;

import by.bsu.hotel.dao.reservation.ReservationDAO;
import by.bsu.hotel.entity.user.UserRole;
import by.bsu.hotel.logic.entity.ReservationLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by 21vek.by on 31.08.2015.
 */
public class CancelReservationCommand implements ActionCommand {
    private final static Logger LOG = Logger.getLogger(CancelReservationCommand.class);

    /**
     * Waiter command: Cancel Reservation
     * @see by.bsu.hotel.entity.reservation.Reservation
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request)  {
        String page = null;
        HttpSession session = request.getSession();
        UserRole role = (UserRole) session.getAttribute("userRole");
        String idReservation = (String) request.getParameter("id_reservation");
        int id = Integer.parseInt(idReservation);
        try {
            ReservationLogic.cancelReservation(role, id);
            String message = MessageManager.getProperty("message.info.reservationcanceled", session);
            request.setAttribute("infoMessage", message);
            switch (role) {
                case CLIENT:
                    page = ConfigurationManager.getProperty("path.page.client_menu");
                    break;
                case ADMINISTRATOR:
                    page = ConfigurationManager.getProperty("path.page.admin_menu");
                    break;
            }

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }

        return page;
    }
}
