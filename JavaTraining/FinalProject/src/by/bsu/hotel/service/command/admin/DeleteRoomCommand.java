package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 05.09.2015.
 */
public class DeleteRoomCommand implements ActionCommand {
    private final static Logger LOG = Logger.getLogger(DeleteRoomCommand.class);
    /**
     * Admin command: delete room
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Room> rooms = null;
        HttpSession session = request.getSession();
        String str = request.getParameter("id_room");
        int id = Integer.parseInt(str);
        try {
            if (RoomLogic.deleteRoomWithId(id)) {
                rooms = RoomLogic.takeAllRooms();
                request.setAttribute("rooms", rooms);
                page = ConfigurationManager.getProperty("path.page.all_rooms");

            } else {
                rooms = RoomLogic.takeAllRooms();
                String message = MessageManager.getProperty("message.error.deleteroom",session);
                request.setAttribute("errorMessage", message);
                session.setAttribute("rooms", rooms);
                page = ConfigurationManager.getProperty("path.page.all_rooms");
            }
            return page;
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        return page;
    }
}
