package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.entity.room.RoomType;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 02.09.2015.
 */
public class AddRoomTypeCommand implements ActionCommand {
    public static final Logger LOG = Logger.getLogger(AddRoomTypeCommand.class);

    /**
     * Admin command: add room type
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<RoomType> types = null;
        HttpSession session = request.getSession();
        String str = request.getParameter("max_persons");
        int maxPersons = Integer.parseInt(str);
        str = request.getParameter("rate");
        double rate = Double.parseDouble(str);
        str  = request.getParameter("accommodation_type");
        int idType = Integer.parseInt(str);

        try {
            RoomLogic.writeRoomType(rate,maxPersons, idType);
            types = RoomLogic.takeAllRoomTypes();
            session.setAttribute("types", types);
            page = ConfigurationManager.getProperty("path.page.room_types");

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }

        return page;
    }
}
