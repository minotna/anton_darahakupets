package by.bsu.hotel.service.command.common;

import by.bsu.hotel.entity.user.UserRole;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class AuthorizationCommand implements ActionCommand {
    private static final String DEFAULT_LOCALE = "ru_RU";

    /**
     * Authorization Command
     * Get role from Session and return page of main menu for this role
     * @see UserRole
     * @see HttpSession
     *
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        UserRole role = (UserRole) session.getAttribute("userRole");
        String page = null;
        if(session.getAttribute("userRole") == null){
            role = UserRole.GUEST;
            session.setAttribute("userRole", role);
        }
        if(session.getAttribute("locale") == null){
            session.setAttribute("locale", DEFAULT_LOCALE);
        }
        switch (role){
            case GUEST:
                page = ConfigurationManager.getProperty("path.page.login");
                break;
            case CLIENT:
                page = ConfigurationManager.getProperty("path.page.client_menu");
                break;

            case ADMINISTRATOR:
                page = ConfigurationManager.getProperty("path.page.admin_menu");
                break;

        }
        return page;
    }

}
