package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.logic.entity.ReservationLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by 21vek.by on 26.08.2015.
 */
public class EditReservationCommand implements ActionCommand {
    private final static Logger LOG =  Logger.getLogger(EditReservationCommand.class);
    /**
     * Admin command: page to this reservation edit
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        String idReservation = request.getParameter("id_reservation");
        int id = Integer.parseInt(idReservation);

        try {
            Reservation reservation = ReservationLogic.takeNewReservationWithId(id);
            page = ConfigurationManager.getProperty("path.page.edit_reservation");
            session.setAttribute("reservation",reservation);
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }

        return page;
    }
}
