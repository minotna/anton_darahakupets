package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.logic.date.DateChecker;
import by.bsu.hotel.logic.entity.ReservationLogic;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by 21vek.by on 27.08.2015.
 */
public class FindFreeRoomCommand implements ActionCommand{
    private final static Logger LOG = Logger.getLogger(FindFreeRoomCommand.class);

    /**
     * Admin command: find free room with search criteria
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        List<Room> rooms = null;
        String arrival = request.getParameter("arrival_date");
        String departure = request.getParameter("departure_date");
        String persons = request.getParameter("max_persons");
        SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
        int maxPersons = Integer.parseInt(persons);
        Date arrivalDate = null;
        Date departureDate = null;
        try {
            arrivalDate = sm.parse(arrival);
            departureDate = sm.parse(departure);

        } catch (ParseException e) {
            page = exeptionHelp(request, e, LOG);
        }
        try {
            if(DateChecker.checkDate(arrivalDate, departureDate)) {
                rooms = RoomLogic.findFreeRoom(arrivalDate, departureDate, maxPersons);
                String message = MessageManager.getProperty("message.reservationcreate", session);
                request.setAttribute("infoMessage", message);
                request.setAttribute("rooms", rooms);
                page = ConfigurationManager.getProperty("path.page.edit_reservation");
            } else {

                String message = MessageManager.getProperty("message.dateerror", session);
                request.setAttribute("errorDatePassMessage",message);
                page = ConfigurationManager.getProperty("path.page.edit_reservation");
            }

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        return page;
    }
}
