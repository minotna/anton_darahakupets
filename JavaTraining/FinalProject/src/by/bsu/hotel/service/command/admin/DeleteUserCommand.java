package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.logic.entity.UserLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by 21vek.by on 12.08.2015.
 */
public class DeleteUserCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(DeleteUserCommand.class);

    /**
     * Admin command: delete user
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page = null;
        try {
            if(UserLogic.deleteUserWithLogin(request.getParameter("del_user_login"))){
                String message = MessageManager.getProperty("message.info.delete_user", session);
                session.setAttribute("infoMessage", message);
            }else{
                String message = MessageManager.getProperty("message.delete_user_error", session);
                session.setAttribute("infoMessage", message);
            }
            page = ConfigurationManager.getProperty("path.page.index");
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        return page;
    }
}
