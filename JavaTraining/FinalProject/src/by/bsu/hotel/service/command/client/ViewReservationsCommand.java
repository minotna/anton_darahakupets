package by.bsu.hotel.service.command.client;

import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.logic.entity.ReservationLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 26.08.2015.
 */
public class ViewReservationsCommand implements ActionCommand {
    private final static Logger LOG = Logger.getLogger(ViewReservationsCommand.class);

    /**
     * Client command: return page to view all reservations
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        String login = (String) session.getAttribute("user");
        List<Reservation> reservations = null;

        try {
            reservations = ReservationLogic.takeReservationsWithLogin(login);
            request.setAttribute("reservations", reservations);
            page = ConfigurationManager.getProperty("path.page.reservations");

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }


        return page;
    }
}
