package by.bsu.hotel.service.command.client;

import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.logic.entity.ReservationLogic;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by 21vek.by on 30.08.2015.
 */
public class ReservationDetailCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(ReservationDetailCommand.class);

    /**
     * Client command: return page to detail of reservation
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        Reservation reservation = null;
        Room room = null;
        HttpSession session = request.getSession();
        String idReservation = (String)request.getParameter("id_reservation");
        String login = (String)session.getAttribute("user");
        int id = Integer.parseInt(idReservation);
        int idStatus = 0;
        try {

            reservation = ReservationLogic.takeUserReservationWithId(id, login);
            idStatus = ReservationLogic.takeIdStatus(id);
            room = RoomLogic.takeRoomWithIdReservation(id);
            request.setAttribute("idStatus", idStatus);
            request.setAttribute("reservation", reservation);
            request.setAttribute("room", room);

            page = ConfigurationManager.getProperty("path.page.reservation_detail");

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        return page;
    }
}
