package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by 21vek.by on 02.09.2015.
 */
public class ViewAccomodationTypesCommand  implements ActionCommand{
    private final static Logger LOG = Logger.getLogger(ViewAccomodationTypesCommand.class);
    /**
     * Admin command: return page to view accommodation types
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<AccommodationType> types= null;
        try {
            types =  RoomLogic.takeAllAccommodationTypes();
            request.setAttribute("types", types);
            page = ConfigurationManager.getProperty("path.page.accommodation_types");
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }

        return page;
    }
}
