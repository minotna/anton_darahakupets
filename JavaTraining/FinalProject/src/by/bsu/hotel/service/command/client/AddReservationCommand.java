package by.bsu.hotel.service.command.client;

import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.reservation.ReservationDetail;
import by.bsu.hotel.entity.reservation.ReservationStatus;
import by.bsu.hotel.logic.date.DateChecker;
import by.bsu.hotel.logic.entity.ReservationLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 21vek.by on 17.08.2015.
 */
public class AddReservationCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(AddReservationCommand.class);

    /**
     * Client command: create new reservation
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request){
        String page = null;
        HttpSession session = request.getSession();
        String typeRoomId = request.getParameter("id_room_type");

        if (typeRoomId == null) {
            String message = MessageManager.getProperty("message.error.wrongroomtype", session);
            request.setAttribute("errorMessage", message);
            page = ConfigurationManager.getProperty("path.page.reservation");
            return page;
        }

        ReservationDetail reservationDetail = new ReservationDetail();
        Reservation reservation = new Reservation();
        SimpleDateFormat sm  = new SimpleDateFormat("yyyy-MM-dd");
        int id = Integer.parseInt(typeRoomId);
        String login = (String) session.getAttribute("user");
        String dateArrival = request.getParameter("arrivaldate");
        String dateDeparture = request.getParameter("departuredate");

        ReservationStatus status = ReservationStatus.NEW;

        Date arrivalDate = null;
        Date departureDate = null;
        try {
            arrivalDate = sm.parse(dateArrival);
            departureDate = sm.parse(dateDeparture);
            reservation.setArrivalDate(arrivalDate);
            reservation.setDepartureDate(departureDate);

        } catch (ParseException e) {
            page = exeptionHelp(request, e, LOG);
        }

        reservation.setDetails(reservationDetail);
        try {
            if(DateChecker.checkDate(arrivalDate,departureDate)) {
                ReservationLogic.writeReservationDetail(reservationDetail, login, id);
                ReservationLogic.writeReservation(reservationDetail.getId(), reservation, status);
                String message = MessageManager.getProperty("message.reservationcreate", session);
                request.setAttribute("infoMessage", message);
                page = ConfigurationManager.getProperty("path.page.client_menu");


            } else {

                String message = MessageManager.getProperty("message.dateerror", session);
                request.setAttribute("errorMessage", message);
                page = ConfigurationManager.getProperty("path.page.reservation");
            }

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }

        return page;
    }
}
