package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.entity.room.RoomType;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 03.09.2015.
 */
public class ViewRoomTypesCommand  implements ActionCommand{
    private static final Logger LOG = Logger.getLogger(ViewRoomTypesCommand.class);

    /**
     * Admin command: return page to view a room types
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<RoomType> types= null;
        List<AccommodationType> accommodationTypes = null;
        HttpSession session = request.getSession();
        try {
            accommodationTypes = RoomLogic.takeAllAccommodationTypes();
            types = RoomLogic.takeAllRoomTypes();
            session.setAttribute("types", types);
            session.setAttribute("accommodationTypes", accommodationTypes);
            page = ConfigurationManager.getProperty("path.page.room_types");
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        return page;
    }
}
