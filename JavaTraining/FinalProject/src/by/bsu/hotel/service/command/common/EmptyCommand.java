package by.bsu.hotel.service.command.common;

import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class EmptyCommand implements ActionCommand {
    /**
     * If ActionFactory cant define command return default page
     * @see ActionCommand
     * @see by.bsu.hotel.service.action.ActionFactory
     * @param request HttpServletRequest
     * @return page address to generate for user
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.index");
        return page;
    }


}
