package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.reservation.ReservationStatus;
import by.bsu.hotel.entity.room.RoomStatus;
import by.bsu.hotel.logic.entity.ReservationLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by 21vek.by on 21.08.2015.
 */
public class ViewNewReservationsCommand implements ActionCommand {
    private final static Logger LOG = Logger.getLogger(ViewNewReservationsCommand.class);

    /**
     * Admin command: return page to view a new reservation
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Reservation> reservations = null;
        try {
            reservations = ReservationLogic.takeReservationsWithStatus(ReservationStatus.NEW);
            page = ConfigurationManager.getProperty("path.page.new_reservations");

        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }
        request.getSession().setAttribute("reservations", reservations);
        return page;
    }
}
