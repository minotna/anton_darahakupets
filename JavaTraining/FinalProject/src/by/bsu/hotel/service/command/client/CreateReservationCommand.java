package by.bsu.hotel.service.command.client;

import by.bsu.hotel.entity.room.RoomType;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 17.08.2015.
 */
public class CreateReservationCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(CreateReservationCommand.class);

    /**
     * Client command: return page to create a client's reservation
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        List<RoomType> types = null;

        try {
            types = RoomLogic.takeAllRoomTypes();
            session.setAttribute("roomtypes", types);
            page = ConfigurationManager.getProperty("path.page.reservation");
        } catch (LogicException e) {
            page = exeptionHelp(request,e,LOG);
        }
        request.setAttribute("roomtypes", types);
        return page;
    }
}
