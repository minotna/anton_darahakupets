package by.bsu.hotel.service.command.admin;

import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.entity.room.RoomType;
import by.bsu.hotel.logic.entity.RoomLogic;
import by.bsu.hotel.logic.exception.LogicException;
import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;
import by.bsu.hotel.service.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 21vek.by on 02.09.2015.
 */
public class AddRoomCommand implements ActionCommand {
    private final static Logger LOG = Logger.getLogger(AddRoomCommand.class);
    /**
     * Admin command: add room
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        RoomType type = null;
        HttpSession session = request.getSession();
        String str = request.getParameter("id_room_type");

        if (str == null) {
            String message = MessageManager.getProperty("message.error.wrongroomtype", session);
            request.setAttribute("errorMessage", message);
            page = ConfigurationManager.getProperty("path.page.new_room");
            return page;
        }

        int idRoomType = Integer.parseInt(str);
        str = request.getParameter("room_number");
        int idRoom = Integer.parseInt(str);



        try {
            if(RoomLogic.writeRoom(idRoom, idRoomType)) {
                page = ConfigurationManager.getProperty("path.page.all_rooms");
                request.setAttribute("rooms", RoomLogic.takeAllRooms());
            } else {
                String message = MessageManager.getProperty("message.error.wrongroomnumber", session);
                request.setAttribute("errorMessage", message);
                page = ConfigurationManager.getProperty("path.page.new_room");

            }
        } catch (LogicException e) {
            page = exeptionHelp(request, e, LOG);
        }


        return page;
    }
}
