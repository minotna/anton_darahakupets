package by.bsu.hotel.service.manager;

import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class MessageManager {

    private MessageManager() { }

    public static String getProperty(String key, Locale locale) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.property.messages", locale);
        return resourceBundle.getString(key);
    }


    public static String getProperty(String key, HttpSession session) {
        String sessionLocale = session.getAttribute("locale").toString();
        Locale locale = new Locale(sessionLocale);
        return getProperty(key, locale);
    }

}
