package by.bsu.bank.cashbox;

import by.bsu.bank.bank.Bank;
import by.bsu.bank.client.Client;
import org.apache.log4j.Logger;

/**
 * Created by 21vek.by on 05.07.2015.
 */
public class Cashbox {
    private static final Logger LOG = Logger.getLogger(Cashbox.class);
    private String name;
    private Bank bank;

    public Cashbox(String name, Bank bank) {
        this.bank = bank;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void cashOut(Client client, double sum){
        double sum1 = bank.getAccount(client).getBalance();
        double cash = client.getCash();
        double totalCash = bank.getTotalCash();


        sum1 = sum1 - sum;
        bank.getAccount(client).setBalance(sum1);
        totalCash = totalCash - sum;
        bank.setTotalCash(totalCash);
        cash = cash + sum;
        client.setCash(cash);


    }

    public void cashIn(Client client, double sum) {
        double sum1 = bank.getAccount(client).getBalance();
        double cash = client.getCash();
        double totalCashInBank = bank.getTotalCash();

        cash = cash - sum;
        client.setCash(cash);
        sum1 = sum1 + sum;
        bank.getAccount(client).setBalance(sum1);
        totalCashInBank = totalCashInBank + sum;
        bank.setTotalCash(totalCashInBank);

    }
}

