package by.bsu.bank.bank;

import by.bsu.bank.cashbox.Cashbox;
import by.bsu.bank.client.Account;
import by.bsu.bank.client.Client;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created by 21vek.by on 06.07.2015.
 */
public class Bank extends Thread {
    private final static Logger LOG = Logger.getLogger(Bank.class);
    private final int BANK_SIZE = 3;
    private double totalCashInBank;
    private Storehouse storehouse;
    private double sizeStorehouse= 100_000;
    private Queue<Client> clients = new LinkedList<>();
    private Queue<Cashbox> freeCashBoxes = new LinkedList<>();
    private final Semaphore semaphore = new Semaphore(BANK_SIZE, true);
    private static Bank bank;
    private ArrayBlockingQueue<Account> accounts = new ArrayBlockingQueue<Account>(20);
    private Lock lock = new ReentrantLock();


    private Bank() throws InterruptedException {
        this.storehouse = new Storehouse(sizeStorehouse);
        Thread.sleep(1_000);
    }

    public Account getAccount(Client client){
        Account account1 = null;

        for(Account account: accounts){
            if(client.getIdAccount() == account.getId()) {
                account1 = account;
            }
        }
        return account1;
    }

    public double getTotalCash() {
        return totalCashInBank;

    }

    public void setTotalCash(double totalCash) {
        lock.lock();
        this.totalCashInBank = totalCash;
        lock.unlock();
    }

    public Storehouse getStorehouse() {
        return storehouse;
    }


    public void setStorehouse(Storehouse storehouse) {
        this.storehouse = storehouse;
    }

    @Override
    public void run() {
        System.out.println("Bank is working");
        try {

            Thread.sleep(5_000);
            totalCash();
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        while (true) {
            if (!clients.isEmpty()) {
                try {
                    Client client = clients.poll();
                    semaphore.acquire();
                    Cashbox cashBox = freeCashBoxes.poll();
                    client.setCashbox(cashBox);
                    Thread.sleep(new Random().nextInt(5_000));
                } catch (InterruptedException e) {
                    LOG.error(e);
                }

            }else{
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    LOG.error(e);
                }
            }

        }
    }


    public static Bank getInstance() throws InterruptedException {
        if (bank == null) {
            bank = new Bank();
        }
        return bank;
    }

    public void addCashBoxes(Queue<Cashbox> cashBoxes) {
        bank.freeCashBoxes.addAll(cashBoxes);
    }

    public void addAccount(Account account){
        accounts.add(account);
    }


    public boolean addClient(Client client){
        return clients.add(client);
    }

    private double totalCash(){
        double totalCash = 0;
        for(Account account: accounts){
            totalCash =  totalCash + account.getBalance();
            this.setTotalCash(totalCash);
        }
        return getTotalCash();

    }

    public void checkTotalCash(Client client){
        lock.lock();
        double totalCash = getTotalCash();
        double balanceStore = storehouse.getBalance();
        double minSum = storehouse.getMIN_SUM();
        double maxSum = storehouse.getMAX_SUM();

        if(totalCash < minSum) {
            balanceStore = balanceStore - (minSum - totalCash);
            totalCash = minSum;
            bank.setTotalCash(totalCash);
            bank.storehouse.setBalance(balanceStore);
        }

        if(totalCash > maxSum) {
            balanceStore = balanceStore + (totalCash - maxSum);
            totalCash = maxSum;
            bank.setTotalCash(totalCash);
            bank.storehouse.setBalance(balanceStore);
        }

        lock.unlock();
    }




    public boolean returnOperator(Cashbox cashBox) {
        System.out.println("CashBox " + cashBox.getName() + " is released");
        LOG.info("CashBox " + cashBox.getName() + " is released");
        bank.semaphore.release();
        return bank.freeCashBoxes.add(cashBox);

    }

}
