package by.bsu.bank.bank;

/**
 * Created by 21vek.by on 07.07.2015.
 */
public class Storehouse {

    private double balance;
    private final double MAX_SUM = 20_000.0;
    private final double MIN_SUM = 5_000.0;

    public Storehouse(double balance){
        this.balance = balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance()
    {
        return balance;
    }

    public double getMAX_SUM() {
        return MAX_SUM;
    }

    public double getMIN_SUM() {
        return MIN_SUM;
    }
}
