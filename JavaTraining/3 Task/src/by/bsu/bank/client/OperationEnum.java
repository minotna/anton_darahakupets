package by.bsu.bank.client;

/**
 * Created by 21vek.by on 10.07.2015.
 */
public enum OperationEnum  {
    CASH_IN, CASH_OUT
}
