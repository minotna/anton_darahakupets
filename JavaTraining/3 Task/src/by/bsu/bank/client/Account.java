package by.bsu.bank.client;

/**
 * Created by 21vek.by on 07.07.2015.
 */
public class Account {
    private int id;
    private double balance;

    public Account(int id, double balance){
        this.id = id;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
