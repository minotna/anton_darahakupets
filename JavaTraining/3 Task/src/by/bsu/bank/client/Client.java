package by.bsu.bank.client;


import by.bsu.bank.bank.Bank;
import by.bsu.bank.cashbox.Cashbox;
import org.apache.log4j.Logger;



/**
 * Created by 21vek.by on 05.07.2015.
 */

public class Client extends Thread {
    private final static Logger LOG = Logger.getLogger(Client.class);
    private String clientName;
    private Bank bank;
    private int idAccount;
    private Cashbox cashBox;
    private boolean waiting;
    private double cash;
    private OperationEnum operation;
    private double sum;

    public Client(Bank bank, OperationEnum operation, double sum, Account account, double cash) {
        this.cash = cash;
        this.sum = sum;
        this.operation = operation;
        this.bank = bank;
        this.idAccount = account.getId();
    }



    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public Cashbox getCashbox() {
        return cashBox;
    }

    public void setCashbox(Cashbox cashBox) {
        this.cashBox = cashBox;
    }

    public boolean isWaiting() {
        return waiting;
    }
    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName){
        this.clientName = clientName;
    }

    private void addClientToEque(){
        waiting = true;
        bank.addClient(this);
        System.out.println(clientName + " is added to eque...");
        LOG.info(clientName+ " is added to eque...");
    }

    @Override
    public void run() {
        addClientToEque();
        while (waiting && cashBox == null){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                LOG.error(e);
            }
        }

        System.out.println("Client " + clientName + " has started  serve by " + cashBox.getName() + " cashBox...");
        LOG.info("Client " + clientName + " has started serve by " + cashBox.getName() + " cashBox...");

        try {
            Thread.sleep(5_000);
            bank.checkTotalCash(this);
        } catch (InterruptedException e) {
            LOG.error(e);
        }
        LOG.info("BEFORE SERVE CLIENT " + getClientName() + " ,totalCashInBank = " + bank.getTotalCash() + "storeHouseBalance = " + bank.getStorehouse().getBalance() + ";");
        switch (this.operation){
            case CASH_IN: cashBox.cashIn(this,sum);
                break;
            case CASH_OUT: cashBox.cashOut(this, sum);
                break;
        }
        LOG.info("AFTER SERVE CLIENT " + getClientName() + " ,totalCashInBank = " + bank.getTotalCash() + "storeHouseBalance = " + bank.getStorehouse().getBalance() + ";");

        System.out.println("Client " + clientName + " has  served by cashBox " + cashBox.getName()  + "!");
        LOG.info("Client " + clientName + " has served by cashBox " + cashBox.getName() + "!");
        System.out.println("Client " + clientName + " go away!");
        LOG.info("Client " + clientName + " go away!");
        bank.returnOperator(cashBox);

    }
}
