package by.bsu.bank.main;


import by.bsu.bank.bank.Bank;
import by.bsu.bank.cashbox.Cashbox;
import by.bsu.bank.client.Account;
import by.bsu.bank.client.Client;
import by.bsu.bank.client.OperationEnum;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by 21vek.by on 05.07.2015.
 */
public class Main {
    static{
        PropertyConfigurator.configure("src//property//log4j.properties");
    }
    public static final Logger LOG = Logger.getLogger(Main.class);
    public static void main(String[] args) {
        Bank bank = null;
        try {
            bank = Bank.getInstance();
        } catch (InterruptedException e) {
            LOG.error(e);
        }


        LinkedList<Cashbox> cashBoxes = new LinkedList<>();

        cashBoxes.add(new Cashbox("1",bank));
        cashBoxes.add(new Cashbox("2",bank));
        cashBoxes.add(new Cashbox("3",bank));

        bank.addCashBoxes(cashBoxes);

        LinkedList<Account> accounts = new LinkedList<>();
        LinkedList<Client> clients = new LinkedList<>();
        Account account;
        Client client;
        double cash = 0.0;
        double sum = 0.0;

        bank.start();

        cash = 200;
        sum = 200;
        account = new Account(1,1000);
        client = new Client(bank, OperationEnum.CASH_IN, sum, account, cash);
        client.setClientName("1");
        bank.addAccount(account);
        client.start();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        cash = 1000;
        sum = 900;
        account = new Account(2,1000);
        client = new Client(bank, OperationEnum.CASH_OUT, sum, account, cash);
        client.setClientName("2");
        bank.addAccount(account);
        client.start();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        cash = 2000;
        sum = 1000;
        account = new Account(3,750);
        client = new Client(bank, OperationEnum.CASH_IN, sum, account, cash);
        client.setClientName("3");
        bank.addAccount(account);
        client.start();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        cash = 250;
        sum = 200;
        account = new Account(4,800);
        client = new Client(bank, OperationEnum.CASH_IN, sum, account, cash);
        client.setClientName("4");
        bank.addAccount(account);
        client.start();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        cash = 400;
        sum = 300;
        account = new Account(5,570);
        client = new Client(bank, OperationEnum.CASH_OUT, sum, account, cash);
        client.setClientName("5");
        bank.addAccount(account);
        client.start();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        cash = 1000;
        sum = 1000;
        account = new Account(6,500);
        client = new Client(bank, OperationEnum.CASH_OUT, sum, account, cash);
        client.setClientName("6");
        bank.addAccount(account);
        client.start();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        cash = 2500;
        sum = 2500;
        account = new Account(7,600);
        client = new Client(bank, OperationEnum.CASH_IN, sum, account, cash);
        client.setClientName("7");
        bank.addAccount(account);
        client.start();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }
}
