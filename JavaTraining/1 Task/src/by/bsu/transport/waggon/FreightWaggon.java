package by.bsu.transport.waggon;

import by.bsu.transport.exception.LogicException;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public class FreightWaggon extends AbstractWaggon{
    private double capacity;
    private String typeCargo;

    public FreightWaggon(){
    }

    public void setCapacity(double capacity) throws LogicException {
        if (capacity > 0) {
            this.capacity = capacity;
        } else throw new LogicException();
    }

    public double getCapacity() {
        return capacity;
    }

    public String getTypeCargo() {
        return typeCargo;
    }

    public void setTypeCargo(String typeCargo) {
        this.typeCargo = typeCargo;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj.getClass()!=getClass()) return false;
        if (!super.equals(obj)) return false;

        FreightWaggon freightWaggon = (FreightWaggon) obj;

        if (Double.compare(freightWaggon.capacity,capacity)!=0) return false;
        if (freightWaggon.typeCargo!=typeCargo) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result = super.hashCode();
        int prime = 31;
        result = prime * result + (int) capacity;
        result = prime * result + typeCargo.hashCode();
        return result;
    }

    @Override
    public String toString(){
        return super.toString() +
                ", capacity=" + capacity +
                ",typeCargo=" + typeCargo +
                " ";
    }
}
