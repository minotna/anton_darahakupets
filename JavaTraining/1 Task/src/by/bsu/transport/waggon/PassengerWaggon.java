package by.bsu.transport.waggon;

import by.bsu.transport.exception.LogicException;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public class PassengerWaggon extends AbstractWaggon {
    private PassengerWaggonEnum typePassengerWaggon;
    private int amountPassengers;
    private int amountLuggages;

    public PassengerWaggon(){
    }

    public PassengerWaggonEnum getTypePassengerWaggon(){
        return typePassengerWaggon;
    }

    public void setTypePassengerWaggon(PassengerWaggonEnum typePassengerWaggon){
        this.typePassengerWaggon = typePassengerWaggon;
    }

    public int getAmountLuggages() {
        return amountLuggages;
    }

    public int getAmountPassengers()  {
       return amountPassengers;
    }

    public void setAmountLuggages(int amountLuggages) throws LogicException {
        if (amountLuggages >= 0) {
            this.amountLuggages = amountLuggages;
        }else throw new LogicException();
    }

    public void setAmountPassengers(int amountPassengers) throws LogicException {
        if ((amountPassengers <= typePassengerWaggon.getNumberSeats()) && (amountPassengers >= 0)) {
            this.amountPassengers = amountPassengers;
        }else throw new LogicException();
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj.getClass()!=getClass()) return false;
        if (!super.equals(obj)) return false;

        PassengerWaggon passengerWaggon = (PassengerWaggon) obj;

        if (passengerWaggon.typePassengerWaggon!=typePassengerWaggon) return false;
        if (passengerWaggon.amountLuggages != amountLuggages) return false;
        if (passengerWaggon.amountPassengers!=amountPassengers) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result = super.hashCode();
        int prime = 31;
        result = prime * result + amountLuggages;
        result = prime * result + amountPassengers;
        result = prime * result + typePassengerWaggon.hashCode();
        return result;
    }

    @Override
    public String toString(){
        return super.toString() +
                ", typePassengerWaggon=" + typePassengerWaggon +
                ", amountLuggages=" + amountLuggages +
                ", amountPassengers=" + amountPassengers +
                " ";
    }
}
