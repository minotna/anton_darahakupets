package by.bsu.transport.waggon;

import by.bsu.transport.exception.LogicException;

/**
 * Created by 21vek.by on 29.05.2015.
 */
public abstract class AbstractWaggon {
    private int id;
    private double weight;

    public AbstractWaggon(){
    }

    public double getWeight() {
        return weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) throws LogicException {
        if (id > 0) {
            this.id = id;
        }else throw new LogicException();
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (!(obj instanceof AbstractWaggon)) return false;

        AbstractWaggon waggon = (AbstractWaggon) obj;

        if (waggon.getId()!=id) return false;
        if (Double.compare(waggon.getWeight(),weight)!=0) return false;

        return true;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result  = 1;
        result = prime * result + id;
        result = prime * result + (int) weight;
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +
                ": id=" + id +
                ", weight=" + weight  + " ";
    }
}
