package by.bsu.transport.waggon;

/**
 * Created by 21vek.by on 27.05.2015.
 */
public enum PassengerWaggonEnum {
    SLEEPER(18), COMPARTMENT(36), RESERVED(54), SEDENTARY(82);

    private final int numberSeats;

    PassengerWaggonEnum(int numberSeats) {
        this.numberSeats = numberSeats;
    }

    public int getNumberSeats() {
        return numberSeats;
    }

}
