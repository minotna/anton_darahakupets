package by.bsu.transport.reporter;

import by.bsu.transport.train.PassengerTrain;
import by.bsu.transport.train.PassengerTrainHandler;
import by.bsu.transport.waggon.PassengerWaggon;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public class Reporter {

    public static StringBuilder trainReport(PassengerTrain train){
        StringBuilder str = new StringBuilder("Passenger Train" + " ");
        str.append("has a" + " ");
        str.append(train.getLocomotive().getTypeLocomotive().name()+ " ");
        str.append("locomotive" + " ");
        str.append("with maxSpeed = ");
        str.append(train.getLocomotive().getMaxSpeed()+ " ");
        str.append("and" + " ");
        str.append(train.getLocomotive().getEngine().name().toLowerCase() + " ");
        str.append("engine." + " ");
        str.append("This train has" + " ");
        str.append(train.getWaggonList().size()+ " ");
        str.append("waggons." + " " + System.lineSeparator());



        for(PassengerWaggon waggon: train.getWaggonList()){
           str.append(waggon.toString() + "\n");
        }
        str.append("Total amount passengers = " + PassengerTrainHandler.findTotalAmountPassengers(train) + ".");
        str.append(" Total amount luggages = " + PassengerTrainHandler.findTotalAmountLuggage(train) + ".");

        return str;


    }

}
