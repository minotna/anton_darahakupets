package by.bsu.transport.comparator;

import by.bsu.transport.waggon.PassengerWaggon;
import java.util.Comparator;
/*
/**
 * Created by 21vek.by on 01.06.2015.
 */
public class LevelComfortComparator implements Comparator<PassengerWaggon> {
    @Override
    public int compare(PassengerWaggon o1, PassengerWaggon o2) {
        return Integer.compare(o1.getTypePassengerWaggon().ordinal(),o2.getTypePassengerWaggon().ordinal());
    }
}
