package by.bsu.transport.locomotive;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public enum EngineEnum {
    DIESEL, ELECTRIC, STEAM
}
