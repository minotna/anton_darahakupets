package by.bsu.transport.locomotive;

import by.bsu.transport.exception.LogicException;
import java.util.logging.Logger;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public class Locomotive {
    private int id;
    private double maxSpeed;
    private LocomotiveEnum typeLocomotive;
    private EngineEnum engine;

    private final static Logger LOG = Logger.getLogger(Locomotive.class.getSimpleName());

    public Locomotive(int id, double maxSpeed, LocomotiveEnum typeLocomotive, EngineEnum engine) {
        this.id = id;
        this.maxSpeed = maxSpeed;
        this.typeLocomotive = typeLocomotive;
        this.engine = engine;
    }

    public Locomotive(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) throws LogicException {
        if (id > 0){
            this.id = id;
        }else throw new LogicException();
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) throws LogicException {
        if ( maxSpeed > 0) {
            this.maxSpeed = maxSpeed;
        }else throw new LogicException();
    }

    public LocomotiveEnum getTypeLocomotive() {
        return typeLocomotive;
    }

    public void setTypeLocomotive(LocomotiveEnum typeLocomotive) {
        this.typeLocomotive = typeLocomotive;
    }

    public EngineEnum getEngine() {
        return engine;
    }

    public void setEngine(EngineEnum engine) {
        this.engine = engine;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null) return false;
        if (this == obj) return true;
        if (obj.getClass()!=getClass()) return false;

        Locomotive locomotive = (Locomotive) obj;

        if (Integer.compare(locomotive.id,id)!=0) return false;
        if (Double.compare(locomotive.maxSpeed,maxSpeed)!=0) return false;
        if (locomotive.engine!=engine) return false;
        if (locomotive.typeLocomotive!=typeLocomotive) return false;

        return true;
    }

    public int hashCode(){
        int result = 1;
        int prime = 37;
        result = prime * result + id;
        result = (int) (prime * result + maxSpeed);
        result = prime * result + typeLocomotive.hashCode();
        result = prime * result + engine.hashCode();
        return result;
    }

    public String toString(){
        return this.getClass().getSimpleName() +
                ": id=" + id +
                ", maxSpeed=" + maxSpeed +
                ", typeLocomotive=" + typeLocomotive +
                ", engine=" + engine +
                 System.lineSeparator();
    }
}
