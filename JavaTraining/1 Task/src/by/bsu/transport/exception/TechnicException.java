package by.bsu.transport.exception;

/**
 * Created by 21vek.by on 26.05.2015.
 */
public class TechnicException extends Exception {
    public TechnicException(){
        super();
    }

    public TechnicException(String message){
        super(message);
    }

    public TechnicException(String message, Throwable cause){
        super(message, cause);
    }

    public TechnicException(Throwable cause){
        super(cause);
    }
}
