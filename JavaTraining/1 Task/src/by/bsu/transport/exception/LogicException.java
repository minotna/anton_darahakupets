package by.bsu.transport.exception;

/**
 * Created by 21vek.by on 26.05.2015.
 */
public class LogicException extends Exception {
    public LogicException(){
        super();
    }

    public LogicException(String message){
        super(message);
    }

    public LogicException(String message, Throwable cause){
        super(message, cause);
    }

    public LogicException(Throwable cause){
        super(cause);
    }
}
