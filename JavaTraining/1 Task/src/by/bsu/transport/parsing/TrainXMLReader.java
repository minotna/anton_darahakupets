package by.bsu.transport.parsing;


import by.bsu.transport.exception.LogicException;
import by.bsu.transport.exception.TechnicException;
import by.bsu.transport.locomotive.EngineEnum;
import by.bsu.transport.locomotive.Locomotive;
import by.bsu.transport.locomotive.LocomotiveEnum;
import by.bsu.transport.waggon.PassengerWaggon;
import by.bsu.transport.waggon.PassengerWaggonEnum;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by 21vek.by on 08.06.2015.
 */
public class TrainXMLReader extends org.xml.sax.helpers.DefaultHandler {


    private List<Locomotive> locomotives;
    private List<PassengerWaggon> waggons;
    private PassengerWaggon waggon = null;
    private Locomotive locomotive = null;
    private String currentText;

    private final static Logger LOGGER = Logger.getLogger(TrainXMLReader.class.getSimpleName());

    public TrainXMLReader(){
        locomotives = new ArrayList<Locomotive>();
        waggons = new ArrayList<PassengerWaggon>();
    }

    public List<Locomotive> getLocomotives(){
        return locomotives;
    }

    public List<PassengerWaggon> getWaggons(){
        return waggons;
    }

    public Locomotive getLocomotive(int i) throws TechnicException {
        try {
            return locomotives.get(i);
        } catch (Exception e){
            throw new TechnicException("Wrong index of Locomotive!");
        }
    }

    public PassengerWaggon getWaggon(int i) throws TechnicException {
        try {
            return waggons.get(i);
        } catch (Exception e){
            throw new TechnicException("Wrong index of Waggon!");
        }
    }

    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        currentText = new String(localName);

        if("locomotive".equals(localName)){

            locomotive = new Locomotive();

            try {
                locomotive.setTypeLocomotive(LocomotiveEnum.valueOf(atts.getValue(0).toUpperCase()));
                locomotive.setId(Integer.parseInt(atts.getValue(1).substring(1)));
            } catch (LogicException e) {
                LOGGER.error("id < 0");
            }
        }

        if("passenger-waggon".equals(qName)) {
            waggon = new PassengerWaggon();
            try {
                waggon.setId(Integer.parseInt(atts.getValue(0).substring(1)));
            } catch (LogicException e) {
                LOGGER.error("id < 0");
            }
        }
    }

    public void endElement(String uri, String localName, String qName){
        currentText = null;

        if("locomotive".equals(localName)){
            locomotives.add(locomotive);
        }

        if("passenger-waggon".equals(localName)){
            waggons.add(waggon);
        }
    }

    public void characters(char[] ch, int start, int len){

        String str = new String(ch,start,len).trim();


        if("max-speed".equals(currentText)){
            try {
                locomotive.setMaxSpeed(Integer.parseInt(str));
            } catch (LogicException e) {
                e.printStackTrace();
            }
        }

        if(("RESERVED".equals(str.toUpperCase()))
                || ("SEDENTARY".equals(str.toUpperCase()))
                || ("SLEEPER".equals(str.toUpperCase()))
                || ("COMPARTMENT".equals(str.toUpperCase()))) {
            waggon.setTypePassengerWaggon(PassengerWaggonEnum.valueOf(str.toUpperCase()));
        }

        if(("DIESEL".equals(str.toUpperCase()))
                || "ELECTRIC".equals(str.toUpperCase())
                || "STEAM".equals(str.toUpperCase())){
            locomotive.setEngine(EngineEnum.valueOf(str.toUpperCase()));
        }

        if("weigth".equals(currentText)){
            waggon.setWeight(Double.parseDouble(str));
        }

        if("amount-passengers".equals(currentText)){
            try {
                waggon.setAmountPassengers(Integer.parseInt(str));
            } catch (LogicException e) {
                e.printStackTrace();
            }
        }

        if("amount-luggages".equals(currentText)){
            try {
                waggon.setAmountLuggages(Integer.parseInt(str));
            } catch (LogicException e) {
                e.printStackTrace();
            }
        }
    }
}
