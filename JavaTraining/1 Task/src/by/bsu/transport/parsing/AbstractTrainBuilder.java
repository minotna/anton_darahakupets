package by.bsu.transport.parsing;

import by.bsu.transport.train.PassengerTrain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 21vek.by on 14.06.2015.
 */
public abstract class AbstractTrainBuilder {
    protected List<PassengerTrain> trains;

    public AbstractTrainBuilder(){
        trains  = new ArrayList<PassengerTrain>();
    }

    public AbstractTrainBuilder(List<PassengerTrain> trains) {
        this.trains = trains;
    }

    public List<PassengerTrain> getTrains() {
        return trains;
    }

    public void setTrains(List<PassengerTrain> trains) {
        this.trains = trains;
    }

    abstract public void buildListTrain(String fileName);

}
