package by.bsu.transport.parsing;

import by.bsu.transport.exception.LogicException;
import by.bsu.transport.locomotive.EngineEnum;
import by.bsu.transport.locomotive.Locomotive;
import by.bsu.transport.locomotive.LocomotiveEnum;
import by.bsu.transport.train.PassengerTrain;
import by.bsu.transport.waggon.PassengerWaggon;
import by.bsu.transport.waggon.PassengerWaggonEnum;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 21vek.by on 14.06.2015.
 */
public class TrainDOMBuilder extends AbstractTrainBuilder {
    private DocumentBuilder documentBuilder;
    private static final Logger LOG = Logger.getLogger(TrainDOMBuilder.class.getSimpleName());

    public TrainDOMBuilder() {
        this.trains = new ArrayList<PassengerTrain>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.error("Ошибка конфигурации парсера:" + e);
        }
    }

    @Override
    public void buildListTrain(String fileName) {
        List<PassengerTrain> trains = new ArrayList<PassengerTrain>();
        List<Locomotive> locomotives = buildListLocomotives(fileName);
        List<PassengerWaggon> waggons = buildListWaggons(fileName);

        for (int i = 2; i >= 0; i--) {
            PassengerTrain train = new PassengerTrain();
            Locomotive locomotive = locomotives.get(i);
            train.setLocomotive(locomotive);
            locomotives.remove(i);
            for (int j = 4; j >= 0; j-- ) {
                PassengerWaggon waggon = waggons.get(j);
                train.addWaggon(waggon);
                waggons.remove(j);
            }
            trains.add(train);
        }
        this.trains = trains;
    }

    private List<Locomotive> buildListLocomotives(String fileName) {
        Document doc = null;
        List<Locomotive> locomotiveList = new ArrayList<Locomotive>();
        try {
            doc = documentBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList locomotiveNodeList = root.getElementsByTagName("locomotive");
            for (int i = 0; i < locomotiveNodeList.getLength(); i++) {
                Element locomotiveElement = (Element) locomotiveNodeList.item(i);
                Locomotive locomotive = buildLocomotive(locomotiveElement);
                locomotiveList.add(locomotive);
            }
        } catch (SAXException  | IOException e ) {
            LOG.error(e);
        }
        return locomotiveList;
    }

    private List<PassengerWaggon> buildListWaggons(String fileName) {
        Document doc = null;
        List<PassengerWaggon> waggonList = new ArrayList<PassengerWaggon>();
        try {
            doc = documentBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList waggonNodeList = root.getElementsByTagName("passenger-waggon");
            for(int i = 0; i < waggonNodeList.getLength(); i++) {
                Element waggonElement = (Element)waggonNodeList.item(i);
                PassengerWaggon waggon = buildPassengerWaggon(waggonElement);
                waggonList.add(waggon);
            }
        } catch (SAXException | IOException e) {
            LOG.error(e);
        }
        return waggonList;
    }

    private Locomotive buildLocomotive(Element locomotiveElement) {
        Locomotive locomotive = new Locomotive();
        try {
            Integer id = Integer.parseInt(locomotiveElement.getAttribute("id").substring(1));
            Double maxSpeed = Double.parseDouble(getElementTextContent(locomotiveElement, "max-speed"));
            EngineEnum engine = EngineEnum.valueOf(getElementTextContent(locomotiveElement, "engine").toUpperCase());
            LocomotiveEnum locType = LocomotiveEnum.valueOf(locomotiveElement.getAttribute("type-locomotive").toUpperCase());
            locomotive.setId(id);
            locomotive.setMaxSpeed(maxSpeed);
            locomotive.setEngine(engine);
            locomotive.setTypeLocomotive(locType);
        } catch (LogicException e) {
            LOG.error(e);
        }
        return locomotive;
    }

    private PassengerWaggon buildPassengerWaggon(Element waggonElement) {
        PassengerWaggon waggon = new PassengerWaggon();
        try {
            Integer id = Integer.parseInt(waggonElement.getAttribute("id").substring(1));
            Double weigth = Double.parseDouble(getElementTextContent(waggonElement, "weigth"));
            Integer amountPassengers = Integer.parseInt(getElementTextContent(waggonElement, "amount-passengers"));
            Integer amountLuggages = Integer.parseInt(getElementTextContent(waggonElement, "amount-luggages"));
            PassengerWaggonEnum typeWaggon = PassengerWaggonEnum.valueOf(getElementTextContent(waggonElement,"type-waggon").toUpperCase());
            waggon.setId(id);
            waggon.setTypePassengerWaggon(typeWaggon);
            waggon.setAmountLuggages(amountLuggages);
            waggon.setAmountPassengers(amountPassengers);

            waggon.setWeight(weigth);
        } catch (LogicException e) {
            LOG.error(e);
        }
        return waggon;

    }

    private  String getElementTextContent(Element element, String elementName){
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        String text = node.getTextContent();
        return text;
    }







    
}
