package by.bsu.transport.parsing;

import by.bsu.transport.exception.TechnicException;
import by.bsu.transport.locomotive.Locomotive;
import by.bsu.transport.train.PassengerTrain;
import by.bsu.transport.waggon.PassengerWaggon;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by 21vek.by on 14.06.2015.
 */
public class TrainSAXBuilder extends AbstractTrainBuilder {
    private final static Logger LOG = Logger.getLogger(String.valueOf(TrainSAXBuilder.class));
    private TrainXMLReader handler;
    private XMLReader reader;


    public TrainSAXBuilder(){
        handler = new TrainXMLReader();

        try {
            reader = XMLReaderFactory.createXMLReader("com.sun.org.apache.xerces.internal.parsers.SAXParser");
            reader.setContentHandler(handler);
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void buildListTrain(String filename) {
        try {
            reader.parse(filename);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        ArrayList<PassengerTrain> trainList = new ArrayList<PassengerTrain>();


        for (int i = 2; i >= 0; i--){
            PassengerTrain train = new PassengerTrain();
            Locomotive locomotive = new Locomotive();
            try {
                locomotive = handler.getLocomotive(i);
                train.setLocomotive(locomotive);
            } catch (TechnicException e) {
                e.printStackTrace();
            }

            for (int j = 4; j >= 0 ; j--){
                PassengerWaggon waggon = new PassengerWaggon();
                try {
                    waggon = handler.getWaggon(j);
                    train.addWaggon(waggon);
                } catch (TechnicException e) {
                    e.printStackTrace();
                }

            }
            trainList.add(train);
        }
        trains = trainList;
    }


    }

