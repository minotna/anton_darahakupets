package by.bsu.transport.parsing;

import org.apache.log4j.Logger;

/**
 * Created by 21vek.by on 15.06.2015.
 */
public class TrainBuilderFactory {
    private final static Logger LOG = Logger.getLogger(TrainBuilderFactory.class);
    private enum TypeParser {
        SAX, DOM
    }

    public AbstractTrainBuilder createTrainBuilder(String typeParser) {
        TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
        switch (type){
            case DOM:
                return new TrainDOMBuilder();
            case SAX:
                return new TrainSAXBuilder();
            default:
                LOG.error("No such enum");
                return  null;
        }
    }
}
