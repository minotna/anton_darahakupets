package by.bsu.transport.train;

import by.bsu.transport.exception.LogicException;
import by.bsu.transport.waggon.PassengerWaggon;
import by.bsu.transport.waggon.PassengerWaggonEnum;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public class PassengerTrainHandler {
    public static int findTotalAmountPassengers(PassengerTrain passengerTrain){
        int result = 0;
        for (PassengerWaggon waggon: passengerTrain.getWaggonList()){
             result += waggon.getAmountPassengers();
        }
        return result;
    }

    public static int findTotalAmountLuggage(PassengerTrain passengerTrain){
        int result = 0;
        for (PassengerWaggon waggon: passengerTrain.getWaggonList()){
             result += waggon.getAmountLuggages();
        }
        return result;
    }

    public static List<PassengerWaggon> findWaggonsByAmountPassengers(PassengerTrain passengerTrain,
                                                                      int startIndex,
                                                                      int endIndex,
                                                                      PassengerWaggonEnum typePassengerWaggon) throws LogicException {
        if ((startIndex > endIndex) && ((startIndex > typePassengerWaggon.getNumberSeats()) && (endIndex > typePassengerWaggon.getNumberSeats()))){
            int y = endIndex;
            endIndex = startIndex;
            startIndex = y;
        }

        List<PassengerWaggon> resultList = new LinkedList<PassengerWaggon>();
        for (PassengerWaggon waggon: passengerTrain.getWaggonList()){
             if ((waggon.getAmountPassengers() > startIndex )&& (waggon.getAmountPassengers() < endIndex) && (waggon.getTypePassengerWaggon() == typePassengerWaggon)){
                 resultList.add(waggon);
             }
        }
        return resultList;



    }
}
