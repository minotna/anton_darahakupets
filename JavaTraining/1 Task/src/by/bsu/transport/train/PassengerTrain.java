package by.bsu.transport.train;

import by.bsu.transport.exception.TechnicException;
import by.bsu.transport.locomotive.Locomotive;
import by.bsu.transport.waggon.PassengerWaggon;

import java.util.LinkedList;
import java.util.logging.Logger;

/**
 * Created by 21vek.by on 01.06.2015.
 */
public class PassengerTrain {
    private Locomotive locomotive;
    private LinkedList<PassengerWaggon> waggonList;

    private final static Logger LOG = Logger.getLogger(PassengerTrain.class.getSimpleName());

    public PassengerTrain(Locomotive locomotive) {
        this.locomotive = locomotive;
        waggonList = new LinkedList<PassengerWaggon>();
    }

    public PassengerTrain(){
        waggonList = new LinkedList<PassengerWaggon>();

    }


    public void setLocomotive(Locomotive locomotive) {
        this.locomotive = locomotive;
    }

    public Locomotive getLocomotive() {
        return locomotive;
    }



    public LinkedList<PassengerWaggon> getWaggonList() {
        return waggonList;
    }

    public void setWaggonList(LinkedList<PassengerWaggon> waggonList){
        this.waggonList = waggonList;
    }

    public void addWaggon(PassengerWaggon waggon){
        getWaggonList().addLast(waggon);
    }

    public void removeWaggon(PassengerTrain waggon){
        getWaggonList().removeLast();
    }

    public  PassengerWaggon getWaggon() throws TechnicException {
        try {
            return getWaggonList().getLast();
        } catch (Exception e){
            throw new TechnicException(" ");

        }
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj.getClass()!=getClass()) return false;

        PassengerTrain passengerTrain = (PassengerTrain) obj;

        if (!waggonList.equals(passengerTrain.waggonList)) return false;
        if (!locomotive.equals(passengerTrain.locomotive)) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result = 1;
        int prime = 31;
        result = prime * result + locomotive.hashCode();
        result = prime * result + waggonList.hashCode();
        return result;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName() +
                "{locomotive=" + locomotive.toString() +
                ", waggonList" + waggonList +
                "}";
    }





}
