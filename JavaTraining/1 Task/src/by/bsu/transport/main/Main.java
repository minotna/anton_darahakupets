package by.bsu.transport.main;


import by.bsu.transport.parsing.AbstractTrainBuilder;
import by.bsu.transport.parsing.TrainBuilderFactory;
import by.bsu.transport.reporter.Reporter;
import by.bsu.transport.train.PassengerTrain;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

/**
 * Created by 21vek.by on 26.05.2015.
 */
public class Main {

    static {
        PropertyConfigurator.configure("src/property/log4j.properties");
    }

    private final static Logger LOG = Logger.getLogger("Main");
    public static void main(String[] args) {

        TrainBuilderFactory factory = new TrainBuilderFactory();
        AbstractTrainBuilder builder = factory.createTrainBuilder("dom");
        builder.buildListTrain("data/transport.xml");
        for (PassengerTrain train: builder.getTrains()) {
            System.out.println(Reporter.trainReport(train));
        }









    }
}

