package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.utils.CommentValidator;

public class AddCommentCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(AddCommentCommand.class);
	
	private final static String ERROR_COMMENT_TEXT = "invalidText";
	
	private final static String PARAM_COMMENT_TEXT = "commentText";
	private final static String PARAM_NEWS_ID = "newsId";
	
	private final static boolean PARAM_STATUS_ERROR_MESSAGE = true; 
	
	@Autowired
	@Qualifier("commentService")
	private CommentService commentService;
	
	@Autowired
	@Qualifier("ViewNewsCommand")
	private ActionCommand viewNewsCommand;
	
	@Override
	public String execute(HttpServletRequest request) {
		String pageCurrent = null;
		String str = request.getParameter(PARAM_COMMENT_TEXT);
		String id = request.getParameter(PARAM_NEWS_ID);
		
		Comment comment = new Comment();
		comment.setNewsId(Long.parseLong(id));
		comment.setCommentText(str);
		
		if (CommentValidator.validate(comment)) {
			try {
				commentService.create(comment);
			} catch (ServiceException e) {
				return exceptionHelp(request, e, LOG);
			}
		} else {
			request.setAttribute(ERROR_COMMENT_TEXT, PARAM_STATUS_ERROR_MESSAGE);
		}
		
		pageCurrent = viewNewsCommand.execute(request);
		
		return pageCurrent;
	}
}
