package com.epam.newsmanagement.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.ConfigurationManager;

public class NextNewsCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(NextNewsCommand.class);
	
	private final static String PATH_PAGE_SUCCES = "path.page.news";
	
	private final static String ERROR_MESSAGE = "errorMessage";
	
	private final static String PARAM_NEWS_VO = "newsVO";
	private final static String PARAM_NEWS_ID = "newsId";
	private final static String PARAM_SEARCH_CRITERIA = "searchCriteria";
	private final static boolean PARAM_STATUS_ERROR_MESSAGE = true;
	
	@Autowired
	@Qualifier("authorService")
	private AuthorService authorService;
	
	@Autowired
	@Qualifier("tagService")
	private TagService tagService;
	
	@Autowired
	@Qualifier("newsService")
	private NewsService newsService;
	
	@Override
	public String execute(HttpServletRequest request) {
		Long newsId = null;
		NewsVO expectedNews = null;
		List<NewsVO> newsVO = null;
		HttpSession session = request.getSession();
		String pageCurrent = ConfigurationManager.getProperty(PATH_PAGE_SUCCES);
		SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(PARAM_SEARCH_CRITERIA);
		String obj = request.getParameter(PARAM_NEWS_ID);

		try {
			newsId = Long.parseLong(obj);
		} catch(NumberFormatException e) {
			return exceptionHelp(request, e, LOG);
		}

		try {
			
			int index = newsService.findIndex(searchCriteria, newsId);

			int i = index + 1;
			newsVO = newsService.getListNewsVO(searchCriteria, i , i);
			if (newsVO.isEmpty()) {
				request.setAttribute(ERROR_MESSAGE, PARAM_STATUS_ERROR_MESSAGE);
				request.setAttribute(PARAM_NEWS_VO, newsService.getNewsVO(newsId));
				return pageCurrent;
			} else {
				expectedNews = newsService.getListNewsVO(searchCriteria, i , i).get(0);
				request.setAttribute(PARAM_NEWS_VO, expectedNews);
				return pageCurrent;
			}
		} catch (ServiceException e) {
			pageCurrent = exceptionHelp(request, e, LOG);
		}
		return pageCurrent;
	}
}
