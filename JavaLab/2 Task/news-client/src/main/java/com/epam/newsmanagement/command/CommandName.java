package com.epam.newsmanagement.command;

public enum CommandName {
	SHOW_LIST_NEWS, CHANGE_LANGUAGE, VIEW_NEWS, ADD_COMMENT, FILTER, RESET_FILTER, NEXT_NEWS, PREVIOUS_NEWS;
}
