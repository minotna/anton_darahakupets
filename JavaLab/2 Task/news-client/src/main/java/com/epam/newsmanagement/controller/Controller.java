package com.epam.newsmanagement.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.command.CommandFactory;
import com.epam.newsmanagement.utils.ConfigurationManager;

@SuppressWarnings("serial")
@WebServlet("/controller")
public class Controller extends HttpServlet {
	
	private CommandFactory client;
	
	private final static String NAME_BEAN_COMMAND_FACTORY = "CommandFactory";
	
	private final static String PATH_ERROR_PAGE = "path.page.error";
	
	private final static String PARAM_REQUEST_MESSAGE = "message";
	private final static String PARAM_REQUEST_MESSAGE_TEXT = "message.page.not.found";
	
	@SuppressWarnings("resource")
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		Locale.setDefault(Locale.ENGLISH);
		XmlWebApplicationContext ap = new XmlWebApplicationContext();
		ap.setServletContext(config.getServletContext());
		ap.refresh();
		client = (CommandFactory) ap.getBean(NAME_BEAN_COMMAND_FACTORY);	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String page = null;
		ActionCommand command = client.defineCommand(request);
		if (command != null){
			page = command.execute(request);
		}
		if (page != null) {
			/*RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			dispatcher.forward(request, response);*/
			RequestDispatcher dispatcher = request.getRequestDispatcher(page);
			dispatcher.forward(request, response);
			
		} else {
			page = ConfigurationManager.getProperty(PATH_ERROR_PAGE);
			request.getSession().setAttribute(PARAM_REQUEST_MESSAGE, PARAM_REQUEST_MESSAGE_TEXT);
			response.sendRedirect(request.getContextPath() + page);
		}
	}
}