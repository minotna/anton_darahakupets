package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.entity.Comment;

public class CommentValidator {
	
	private final static int MIN_SIZE = 5;
	private final static int MAX_SIZE = 100;
	
	public static boolean validate(Comment comment){
		if(comment.getCommentText().trim().length() < MIN_SIZE){
			return false;
		}else if(comment.getCommentText().trim().length() > MAX_SIZE){
			return false;
		}
		return true;
	}
}
