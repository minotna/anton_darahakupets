package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.ConfigurationManager;

public class ViewNewsCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(ViewNewsCommand.class);
	
	private static final String PATH_PAGE_SUCCES = "path.page.news";
	
	private static final String PARAM_NEWS = "newsVO";
	private static final String PARAM_NEWS_ID = "newsId";
	
	@Autowired
	@Qualifier("authorService")
	private AuthorService authorService;
	
	@Autowired
	@Qualifier("tagService")
	private TagService tagService;
	
	@Autowired
	@Qualifier("newsService")
	private NewsService newsService;
	
	@Override
	public String execute(HttpServletRequest request) {
		String pageReturn = null;
		Long newsId = null;
		String obj = request.getParameter(PARAM_NEWS_ID);
		NewsVO newsVO = null;
		try {
			newsId = Long.parseLong(obj);
			newsVO = newsService.getNewsVO(newsId);
			
			if (newsVO != null) {
				request.setAttribute(PARAM_NEWS, newsVO);
				pageReturn = ConfigurationManager.getProperty(PATH_PAGE_SUCCES);
				return pageReturn;
			} else {
				return pageReturn;
			}
		} catch (ServiceException | NumberFormatException e) {
			pageReturn = exceptionHelp(request, e, LOG);
			return pageReturn;
		}
	}
}
