package com.epam.newsmanagement.command.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.entity.SearchCriteria;

public class FilterCommand implements ActionCommand {
	
	private static final String ATTR_SESSION_AUTHOR = "authorId";
	private static final String ATTR_SESSION_TAGS = "tagIdList";
	private static final String ATTR_SESSION_SEARCH_CRITERIA = "searchCriteria";
	
	@Autowired
	@Qualifier("ShowNewsListCommand")
	private ActionCommand showNewsListCommand;
	
	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String pageReturn = null;
	
		String authorSearch = request.getParameter(ATTR_SESSION_AUTHOR);
		String[] tagsSearch = request.getParameterValues(ATTR_SESSION_TAGS);
		SearchCriteria sc = null;
		if (tagsSearch != null || authorSearch != null) {
			sc = createSearchCriteria(authorSearch, tagsSearch);
		}
		
		session.setAttribute(ATTR_SESSION_SEARCH_CRITERIA, sc);
		
		pageReturn = showNewsListCommand.execute(request);
		
		return pageReturn;
	}
	
	private SearchCriteria createSearchCriteria(String authorId, String[] tagIds) {
		SearchCriteria sc = new SearchCriteria();
		if (authorId != null && (!authorId.equals(""))) {
			sc.setAuthorId(Long.parseLong(authorId));
		}
		if (tagIds != null) {
			List<Long> tagIdsList = new ArrayList<>();
			for (String id : tagIds) {
				tagIdsList.add(Long.parseLong(id));
			}
			sc.setTagIdList(tagIdsList);;
		}
		return sc;
	}

}
