package com.epam.newsmanagement.command;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.utils.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Interface that implement each of Command in ServiceLayer
 * Functional interface
 */
public interface ActionCommand  {
	
	String PATH_PAGE_ERROR = "path.page.error";
	 
    /**
     * @param request HttpServletRequest
     * @return page to generate for user
     */
    String execute(HttpServletRequest request);

    /**
     * Helps to processing exceptions in Service Layer
     * @param request HttpServletRequest
     * @param e Exception
     * @param logger Logger Log4j
     * @return address of error page
     */
    default String exceptionHelp(HttpServletRequest request, Exception e, Logger logger){
        logger.error(e);
        request.setAttribute("errorMessage", e);
        return ConfigurationManager.getProperty("path.page.error");
    }
}