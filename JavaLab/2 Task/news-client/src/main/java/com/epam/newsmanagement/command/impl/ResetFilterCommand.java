package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.entity.SearchCriteria;

public class ResetFilterCommand implements ActionCommand {
	
	private static final String ATTR_SESSION_SEARCH_CRITERIA = "searchCriteria";

	@Autowired
	@Qualifier("ShowNewsListCommand")
	private ActionCommand showNewsListCommand;
	
	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String pageReturn = null;
		SearchCriteria searchCriteria = null;
		
		session.setAttribute(ATTR_SESSION_SEARCH_CRITERIA, searchCriteria);
		
		pageReturn = showNewsListCommand.execute(request);
		
		return pageReturn;
	}

}
