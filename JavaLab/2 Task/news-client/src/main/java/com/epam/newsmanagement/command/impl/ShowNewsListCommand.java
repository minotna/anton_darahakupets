package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.ConfigurationManager;

public class ShowNewsListCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(ShowNewsListCommand.class);
	
	private static final int DEFAULT_PAGE = 1;
	private static final int NUMBER_OF_NEWS_ON_PAGE = 3; 
	
	private static final String PATH_PAGE_SUCCESS = "path.page.newslist";
	
	private static final String PARAM_SEARCH_CRITERIA = "searchCriteria";
	private static final String PARAM_PAGE = "page";
	private static final String PARAM_AUTHORS = "authors";
	private static final String PARAM_NUMBER_OF_NEWS = "numberOfNews";
	private static final String PARAM_TAGS= "tags";
	private static final String PARAM_NEWS_LIST = "newsVOList";
	private static final String PARAM_NUMBER_OF_PAGES = "pages";
	
	
	@Autowired
	@Qualifier("authorService")
	private AuthorService authorService;
	
	@Autowired
	@Qualifier("tagService")
	private TagService tagService;
	
	@Autowired
	@Qualifier("newsService")
	private NewsService newsService;
	
	
	
	
	@Override
	public String execute(HttpServletRequest request) {
		String pageReturn = null;
		String obj =  request.getParameter(PARAM_PAGE);
		HttpSession session = request.getSession();
		int pages = 0;
		Integer page = null;
		
		if (obj != null) {
			try {
				page = Integer.parseInt(obj);
			} catch (NumberFormatException e) {
				return exceptionHelp(request, e, LOG);
			}
		} else {
			page = DEFAULT_PAGE;
		}
			
		SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(PARAM_SEARCH_CRITERIA);
		int startIndex = (page - 1) * NUMBER_OF_NEWS_ON_PAGE + 1;
		int lastIndex = startIndex + NUMBER_OF_NEWS_ON_PAGE - 1;
		
		try {
			int numberOfNews = newsService.getNewsNumber(searchCriteria);
			pages = Double.valueOf(Math.ceil((double)numberOfNews / NUMBER_OF_NEWS_ON_PAGE)).intValue();
			
			request.setAttribute(PARAM_AUTHORS, authorService.getAllAuthors());
			request.setAttribute(PARAM_NUMBER_OF_NEWS, numberOfNews);
			request.setAttribute(PARAM_TAGS, tagService.getAllTags());
			request.setAttribute(PARAM_NEWS_LIST, newsService.getListNewsVO(searchCriteria, startIndex, lastIndex));
			request.setAttribute(PARAM_NUMBER_OF_PAGES, pages);
			
			pageReturn = ConfigurationManager.getProperty(PATH_PAGE_SUCCESS);	
		} catch (ServiceException e) {
			pageReturn = exceptionHelp(request, e, LOG);
		}
		return pageReturn;
	}
}
