package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;



public class CommandFactory {
	private static Logger log = Logger.getLogger(CommandFactory.class);
	private final static String PAR_COMMAND = "command";

	@Autowired
	@Qualifier("ShowNewsListCommand")
	private ActionCommand showNewsListCommand;

	@Autowired
	@Qualifier("ChangeLanguageCommand")
	private ActionCommand changeLanguageCommand;
	
	@Autowired
	@Qualifier("FilterCommand")
	private ActionCommand filterCommand;
	
	@Autowired
	@Qualifier("ResetFilterCommand")
	private ActionCommand resetFilterCommand;
	
	@Autowired
	@Qualifier("ViewNewsCommand")
	private ActionCommand viewNewsCommand;
	
	@Autowired
	@Qualifier("AddCommentCommand")
	private ActionCommand addCommentCommand;
	
	@Autowired
	@Qualifier("NextNewsCommand")
	private ActionCommand nextNewsCommand;
	
	@Autowired
	@Qualifier("PreviousNewsCommand")
	private ActionCommand previousNewsCommand;

	/**
	 * Define which command must be execute and create it
	 * 
	 * @param request
	 *            the http servlet request object
	 * @return the ICommand object
	 */
	public ActionCommand defineCommand(HttpServletRequest request) {
		ActionCommand currentCommand = null;
		String action = request.getParameter(PAR_COMMAND);
		
		if (action == null || action.isEmpty()) {
			return currentCommand;
		}
		try {
			CommandName currentEnum = CommandName.valueOf(action.toUpperCase());
			switch (currentEnum) {
			case SHOW_LIST_NEWS:
				return showNewsListCommand;
			case CHANGE_LANGUAGE:
				currentCommand = changeLanguageCommand;
				break;
			case VIEW_NEWS:
				currentCommand = viewNewsCommand;
				break;
			case ADD_COMMENT:
				currentCommand = addCommentCommand;
				break;
			case FILTER:
				currentCommand = filterCommand;
				break;
			case RESET_FILTER:
				currentCommand = resetFilterCommand;
				break;
			case NEXT_NEWS:
				currentCommand = nextNewsCommand;
				break;
			case PREVIOUS_NEWS:
				currentCommand = previousNewsCommand;
				break;
			default:
				throw new EnumConstantNotPresentException(null, action);
			}
		} catch (IllegalArgumentException | EnumConstantNotPresentException e) {
			log.error("No such command: " + action, e);
		}
		
		return currentCommand;
	}
}

