package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.utils.ConfigurationManager;

public class ChangeLanguageCommand implements ActionCommand {

	private final static String PARAM_LOCALE = "locale";
	private static final String PATH_PAGE_SUCCESS = "path.page.index";


	@Override
	public String execute(HttpServletRequest request) {
		String language = request.getParameter(PARAM_LOCALE);
		request.getSession().setAttribute(PARAM_LOCALE, language);
		return ConfigurationManager.getProperty(PATH_PAGE_SUCCESS);
	}

}
