<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
<head>
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	
	<script type="text/javascript"
	src="<c:url value="/resources/js/checkbox-dropdown.js"/>"></script>
	
	

</head>
<body class="page">
	
	<%@include file="header/header.jspf"%>
	
	<jsp:include page="date-pattern.jsp"></jsp:include>
	
	<div class="body">
	
		<div class="filter">

		<div class="filterForm">

			<form:form  action="controller">
				<input type="hidden" name="command" value="filter"/>
				<select class="authorList" name="authorId">
					<option value="${null}">
						<fmt:message key="label.select.author" /></option>
					<c:forEach var="author" items="${authors}">
						<c:choose>
							<c:when
								test="${ searchCriteria != null && searchCriteria.authorId != null && searchCriteria.authorId == author.id}">
								<option value="${author.id}" selected="selected">${author.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${author.id}">${author.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>

				<div class="multiselect">
					<div class="select_box" onclick="showCheckboxes()">
						<select>
							<option selected="selected" value="false"><fmt:message key="label.select.taglist" /></option>
						</select>
						<div class="over_select" id="over_select"></div>
					</div>
					<div id="checkboxes">
						<c:forEach var="tag" items="${tags}">

							<c:set var="checked" value="false" />
							<c:if test="${ searchCriteria != null && searchCriteria.tagIdList != null}">
								<c:forEach var="selectedTag" items="${searchCriteria.tagIdList}">
									<c:if test="${selectedTag == tag.id }">
										<c:set var="checked" value="true" />
									</c:if>
								</c:forEach>
							</c:if>

							<c:choose>
								<c:when test="${checked==true }">
									<label for="${tag}">
									 	<input type="checkbox" name="tagIdList" value="${tag.id}" checked="checked" />
									 	<c:out	value="${tag.name}" />
									</label>
								</c:when>
								<c:otherwise>
									<label for="${tag}"> 
										<input type="checkbox" name="tagIdList" value="${tag.id}" />
										 <c:out	value="${tag.name}" />
									</label>
								</c:otherwise>
							</c:choose>


						</c:forEach>
					</div>
				</div>
				
				<div class="button">
					<input type="submit"
						value="<fmt:message key="label.button.filter" />" class="button" />
				</div>
			</form:form>
		</div>

		<div class="button">
			<form action="controller" method="post">
				<input type="hidden" name="command" value="reset_filter"/> 
				<input type="submit" value="<fmt:message key="label.button.reset" />"
					class="button" />
			</form>
		</div>
	</div>
		

		<ol class="newsList">
			<c:forEach var="newsVO" items="${newsVOList}">
				<li class="news">

					<div class="highPart">
						<div class="title">
							${newsVO.news.title}
						</div>
						<div class="author">(${newsVO.author.name})</div>
						<div class="modificationDate">
							<fmt:formatDate value="${newsVO.news.modificationDate}"
								pattern="${datePattern}" />
						</div>
					</div>

					<div class="middlePart">
						<div class="shortText">${newsVO.news.shortText}</div>
					</div>

					<div class="bottomPart">

						<div class="edit">
							<a href="controller?command=view_news&newsId=${newsVO.news.id}">
								<fmt:message key="label.button.read"/>
							</a>
						</div>
		
						<div class="commentsCount">
							<fmt:message key="label.message.comments" />
							(${newsVO.commentList.size()})
						</div>
						<div class="tagList">
							<c:forEach var="tag" items="${newsVO.tagList}">
								<c:out value="${tag.name}" />
							</c:forEach>
						</div>
					</div>
				<li>
			</c:forEach>
		</ol>
		
	</div>
	
	<div align="center">
	<table>
	<c:forEach var="i" begin="1" end="${pages}">
		<form action="controller"" method="GET">
				<input type="hidden" name="command" value="show_list_news"/>
				<input type="hidden" name="page" value="${i}"/>
			<tr>
				<input type="submit" value="${i}" />
			</tr>
		</form>
	</c:forEach>
	</table>
	</div>
	
	<%@include file="footer/footer.jspf"%>

</body>
</html>