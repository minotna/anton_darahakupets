<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<html>
<head>
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	
	<script type="text/javascript"
	src="<c:url value="/resources/js/checkbox-dropdown.js"/>"></script>
	
	

</head>
<body class="page">
	<%@include file="header/header.jspf"%>
	
	<jsp:include page="date-pattern.jsp"></jsp:include>
	
	<div class="body">
	
	<div align="left" class="topMenu">  
		<a href="controller?command=show_list_news">
				<button type="button" name="back">
				<fmt:message key="label.button.back" />
				</button>
		</a>
	</div>
		
	

	<div class="newsView">

		<div class="highPart">
			<div class="title">${newsVO.news.title}</div>
			<div class="author">(${newsVO.author.name})</div>
			<div class="modificationDate">
				<fmt:formatDate value="${newsVO.news.modificationDate}"
					pattern="${datePattern}" />
			</div>
		</div>
		<div class="fullTextWrapper">
			<div class="fullText">${newsVO.news.fullText}</div>
		</div>

		<div class="commentList">
			<div class="commentScroll">
				<c:forEach var="comment" items="${newsVO.commentList}">
					<div class="comment">
						<div class="commentDate">
							<fmt:formatDate value="${comment.creationDate}"
								pattern="${datePattern}" />
						</div>
						<div class="commentText">
							<div class="innerCommentText">${comment.commentText}</div>
						</div>
					</div>
				</c:forEach>
			</div>
			
			<form action="controller" method="post">
				<input type="hidden" name="command" value="add_comment">
				<div class="newComment">
					<textarea name="commentText" class="newComment"></textarea>
				</div>
				<input type="submit" value="<fmt:message key="label.button.post_comment" />"
					class="postComment">
					 <input type="hidden" name="newsId" value="${newsVO.news.id}">
			</form>
		</div>

	</div>
	
	<div>
		<c:set var="errorMessage" scope="request" value="${errorMessage}"/>
		<c:if test="${errorMessage == true}">
			<fmt:message key="lable.message.nothing_else"/>
		</c:if>
	</div>
	
	<div>
		<c:set var="invalidText" scope="request" value="${invalidText}"/>
		<c:if test="${invalidText == true}">
			<fmt:message key="Size.comment.text"/>
		</c:if>
	</div>
	</div>
	
	<table  width="900px">
	<tr>
		<td align="left">
		<form action="controller" id="previous" method="post">
		<input type="hidden" name="command" value="previous_news">
		<input type="hidden" name="newsId" value="${newsVO.news.id}">
		</form>
		<button class="button" type="submit"  form="previous"><fmt:message key="label.button.previous"/></button>
		</td>

		<td align="right">
		<form action="controller" id="next" method="post">
		<input type="hidden" name="command" value="next_news">
		<input type="hidden" name="newsId" value="${newsVO.news.id}">
		</form>
		<button class="button" type="submit" form="next"><fmt:message key="label.button.next"/></button>
		</td>
	</tr>
	</table>
	<%@include file="footer/footer.jspf"%>
</body>
</html>