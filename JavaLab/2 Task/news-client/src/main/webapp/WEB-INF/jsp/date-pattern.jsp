<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set value="${pageContext.response.locale}" var="currentLocale" />
<c:choose>
	<c:when test="${currentLocale eq 'RU' }">
		<c:set var="datePattern" value="dd/MM/yyyy" scope="application"></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="datePattern" value="MM/dd/yyyy" scope="application"></c:set>
	</c:otherwise>
</c:choose>
