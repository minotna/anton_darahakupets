<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="message.error.message"/> ${pageContext.errorData.statusCode}</title>
</head>
<body>

	<table>
	    <tr>
	        <td ><fmt:message key="message.error.message"/> ${pageContext.errorData.statusCode}</td>
	    </tr>
	    <tr>
	        <td ><fmt:message key="message.error.exception"/></td>
	    </tr>
	    <tr>
	        <td><fmt:message key="${message}"/></td>
	    </tr>
	</table>

<div align="center">
		<%@include file="../footer/footer.jspf"%>
	</div>
<%-- <a href="../../index.jsp">
    <input type="button" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a> --%>
</body>
</html>