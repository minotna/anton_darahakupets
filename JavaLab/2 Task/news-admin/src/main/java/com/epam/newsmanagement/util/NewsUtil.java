package com.epam.newsmanagement.util;


import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

public class NewsUtil {
	
	public static final int DEFAULT_PAGE = 1;
	public static final int NUMBER_OF_NEWS_ON_PAGE = 3;
	
	public static int findIndex(NewsService service, SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		return service.findIndex(searchCriteria, newsId);
	}
	
	public static void setPreparedParametres(NewsService newsService, AuthorService authorService, TagService tagService, HttpSession session, Model model, Integer page) throws ServiceException {
		SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
		page = page == null ? 1 : page;
		int startIndex = (page - 1) * NUMBER_OF_NEWS_ON_PAGE + 1;
		int lastIndex = startIndex + NUMBER_OF_NEWS_ON_PAGE - 1;
		int numberOfNews = newsService.getNewsNumber(searchCriteria);
		int pages = Double.valueOf(Math.ceil((double)numberOfNews / NUMBER_OF_NEWS_ON_PAGE)).intValue();
		model.addAttribute("numberOfNews", numberOfNews);
		model.addAttribute("authors", authorService.getAllAuthors());
		model.addAttribute("tags", tagService.getAllTags());
		model.addAttribute("newsVOList", newsService.getListNewsVO(searchCriteria, startIndex, lastIndex));
		model.addAttribute("pages", pages);
	}

}