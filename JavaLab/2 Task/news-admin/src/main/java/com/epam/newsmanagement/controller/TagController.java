package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

@Controller
@RequestMapping("/tags")
public class TagController {
	
	@Autowired
	private TagService tagService;
	
	@RequestMapping("/view")
	public String viewTags(Model model) throws ServiceException{
		model.addAttribute("tagList", tagService.getAllTags());
		return "tag";
	}
	
	@RequestMapping("/add")
	public String addTags(RedirectAttributes redirectAttributes, @Valid Tag tag,   BindingResult bindingResult) throws ServiceException{
		if (!bindingResult.hasErrors()) {
			tagService.create(tag);
		}else{
		    redirectAttributes.addFlashAttribute("tag", tag);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.tag", bindingResult);
		}
		return "redirect:/tags/view";
	}
	
	@RequestMapping("/save")
	public String saveTag(RedirectAttributes redirectAttributes, @Valid Tag tag,   BindingResult bindingResult) throws ServiceException{
		if (!bindingResult.hasErrors()) {
			if (tag.getId() != null) {
				tagService.update(tag);
			} else {
				tagService.create(tag);
			}
		}else{
		    redirectAttributes.addFlashAttribute("tag", tag);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.tag", bindingResult);
		}
		return "redirect:/tags/view";
	}
	
	@RequestMapping("/update")
	public String updateTag(RedirectAttributes redirectAttributes, @ModelAttribute @Valid Tag tag, BindingResult bindingResult) throws ServiceException{
		if (!bindingResult.hasErrors()) {
			tagService.update(tag);
		}else{
			redirectAttributes.addFlashAttribute("tag", tag);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.tag", bindingResult);
		}
		return "redirect:/tags/view";
	}
	
	@RequestMapping("/delete/{idTag}")
	public String deleteTag(@PathVariable("idTag") Long idTag) throws ServiceException{
		tagService.delete(idTag);
		return "redirect:/tags/view";
	}
	
	@RequestMapping(value = "/edit/{tagId}")
	public String enableEditingAuthor( @PathVariable("tagId") Long tagId, Model model) throws ServiceException {
		
		model.addAttribute("editingTagId", tagId);
		List<Tag> tagList = null;
		
		tagList = tagService.getAllTags();
		model.addAttribute("tagList", tagList);
		
		return "tag";
	}
}
