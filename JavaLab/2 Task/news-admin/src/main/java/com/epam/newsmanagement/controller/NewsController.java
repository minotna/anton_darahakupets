package com.epam.newsmanagement.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.NewsUtil;

@Controller
@RequestMapping("/news")
public class NewsController {

	@Autowired
	private NewsService newsService;
	
	@Autowired 
	private AuthorService authorService;
	
	@Autowired
	private TagService tagService;

	@RequestMapping("/watch")
	public String getAllNews(HttpSession session, Model model, @RequestParam(value = "page", required=false) Integer page)
			throws ServiceException {
		NewsUtil.setPreparedParametres(newsService, authorService, tagService, session, model, page);
		return "news";
	}

	@RequestMapping("/setSearchCriteria")
	public String setSearchCriteriaToSession(HttpSession session,@ModelAttribute SearchCriteria searchCriteria )			throws ServiceException {
		System.out.println(searchCriteria.toString());
		session.setAttribute("searchCriteria", searchCriteria);
		return "redirect:/news/watch";	
	}

	@RequestMapping("/reset")
	public String resetSearchCriteria(HttpSession session) {
		session.setAttribute("searchCriteria", null);
		return "redirect:/news/watch";
	}

	@RequestMapping("/delete/{newsId}")
	public String changePage(@PathVariable("newsId") Long newsId) throws ServiceException {
		newsService.deleteNews(newsId);
		return "redirect:/news/watch";
	}
	
	@RequestMapping("/page/{page}")
	public String changePage(HttpSession session, Model model, @PathVariable("page") Integer page) throws ServiceException {
		NewsUtil.setPreparedParametres(newsService, authorService, tagService, session, model, page);
		return "news";
	}

	
}