package com.epam.newsmanagement.controller;

import java.util.Calendar;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.NewsBody;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;


@Controller
@RequestMapping("/edit_news")
public class EditNewsController {

	@Autowired
	private NewsService newsService;
	
	@Autowired
	private TagService tagService;
	
	@Autowired
	private AuthorService authorService;

	@RequestMapping("/view")
	public String createNews(Model model) throws ServiceException {
		Date currentDate = Calendar.getInstance().getTime();
		model.addAttribute("currentDate", currentDate);
		model.addAttribute("authors", authorService.getAllAuthors());
		model.addAttribute("tags", tagService.getAllTags());
		return "edit_news";
	}

	@RequestMapping("/edit/{newsId}")
	public String editNews(RedirectAttributes redirectAttributes, @PathVariable("newsId") Long newsId, Model model)
			throws ServiceException {
		model.addAttribute("newsBody", newsService.readNewsBody(newsId));
		model.addAttribute("authors", authorService.getAllAuthors());
		model.addAttribute("tags", tagService.getAllTags());
		return "edit_news";
	}

	@RequestMapping("/save")
	public String saveNews(RedirectAttributes redirectAttributes, @ModelAttribute @Valid NewsBody newsBody,
			 BindingResult bindingResult) throws ServiceException {
		if (!bindingResult.hasErrors()) {

			if (newsBody.getNews().getId() == null) {
				newsService.addNewsBody(newsBody);
			} else {
				newsService.updateNewsBody(newsBody);
			}
			return "redirect:/news/watch";
		} else {
			redirectAttributes.addFlashAttribute("newsBody", newsBody);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.newsBody",
					bindingResult);
			return "redirect:/edit_news/view";
		}
	}
}