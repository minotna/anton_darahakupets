package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

@Controller
@RequestMapping("/authors")
public class AuthorController {
	@Autowired
	private AuthorService authorService;
	
	@RequestMapping("/view")
	public String viewAuthors(Model model) throws ServiceException{
		model.addAttribute("authorList", authorService.getAllAuthors());
		return "author";
	}
	
	@RequestMapping("/save")
	public String saveAuthor(RedirectAttributes redirectAttributes,@ModelAttribute @Valid Author author, BindingResult bindingResult) throws ServiceException{
		if (!bindingResult.hasErrors()) {
			if (author.getId() == null) {
				authorService.create(author);
			} else {
				authorService.update(author);
			}
		}else{
		    redirectAttributes.addFlashAttribute("author", author);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.author", bindingResult);
		}
		return "redirect:/authors/view";
	}
	
	@RequestMapping("/delete/{idAuthor}")
	public String deleteAuthor(@PathVariable("idAuthor") Long idAuthor) throws ServiceException{
		authorService.delete(idAuthor);
		return "redirect:/authors/view";
	}
	
	@RequestMapping(value = "/edit/{authorId}")
	public String enableEditingAuthor( @PathVariable("authorId") Long authorId, Model model) throws ServiceException {
		
		model.addAttribute("editingAuthorId", authorId);
		List<Author> authorList = null;
		
		authorList = authorService.getAllAuthors();
		model.addAttribute("authorList", authorList);
		
		return "author";
	}

}
