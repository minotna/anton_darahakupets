package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.util.NewsUtil;

@Controller
@RequestMapping("/current")
public class CurrentNewsController {

	@Autowired
	private NewsService newsService;
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private CommentService commentService;

	@RequestMapping("/news/{id}")
	public String getCurrentNews(HttpServletRequest request, HttpSession session, Model model, @PathVariable("id") Integer id)
			throws ServiceException {
		Long newsId = new Long(id);
		SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
		
		NewsVO expectedNews = null;
		List<NewsVO> newsVO = null;
		
		int index = newsService.findIndex(searchCriteria, newsId);
		if(request.getParameter("previous") != null) {
			int i = index - 1;
			newsVO = newsService.getListNewsVO(searchCriteria, i , i);
			if (newsVO.isEmpty()) {
				model.addAttribute("errorMessage", "Nothing else");
				model.addAttribute("newsVO", newsService.getNewsVO(newsId));
				return "current_news";
			} else {
				expectedNews = newsService.getListNewsVO(searchCriteria, i , i).get(0);
				model.addAttribute("newsVO", expectedNews);
				return "current_news";
			}
		} 
		

		if(request.getParameter("next") != null) {
			int i = index + 1;
			newsVO = newsService.getListNewsVO(searchCriteria, i , i);
			if (newsVO.isEmpty()) {
				model.addAttribute("errorMessage", "Nothing else");
				model.addAttribute("newsVO", newsService.getNewsVO(newsId));
				return "current_news";
			} else {
				expectedNews = newsService.getListNewsVO(searchCriteria, i , i).get(0);
				model.addAttribute("newsVO", expectedNews);
				return "current_news";
			}
		} 
		
		model.addAttribute("newsVO", newsService.getNewsVO(newsId));
		return "current_news";
	}

	@RequestMapping("/delete_comment/{commentId}")
	public String deleteComment(@PathVariable("commentId") Long commentId) throws ServiceException{
		Comment comment = commentService.read(commentId);
		commentService.delete(commentId);
		return "redirect:/current/news/" + comment.getNewsId();
	}
	
	@RequestMapping(value = "/add-comment")
	public String addComment(RedirectAttributes redirectAttributes, @ModelAttribute @Valid Comment comment, BindingResult bindingResult)
			throws ServiceException {
		if (!bindingResult.hasErrors()) {
			commentService.create(comment);
			redirectAttributes.addFlashAttribute("comment", new Comment());
		} else {
			redirectAttributes.addFlashAttribute("comment", comment);
		    redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.comment", bindingResult);
		}
		return "redirect:/current/news/" + comment.getNewsId();
	}
}