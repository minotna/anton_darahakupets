<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 <div align="left" class="topMenu">  
	<a href="/news-admin/news/watch">
			<button type="button" name="back">
			<spring:message code="label.button.back" />
			</button>
	</a>
</div>
 
 <div class="editTagList">
		<form:form commandName="tag" action="${pageContext.request.contextPath}/tags/save" name="tagForm">

			<table cellpadding="10px">
				<c:forEach items="${tagList}" var="tag">
					<tr>
						<td><b><spring:message code="label.text.tag" />:</b></td>
						<c:choose>
							<c:when test="${editingTagId == tag.id}">

								<td><input type="text" name="name" class="editTag"
									value="${tag.name}" /></td>

								<td><input type="hidden" name="id" value="${tag.id}">
									<input type="submit" class="linkLikeButton"
									value="<spring:message code="label.button.update" />"></td>

								<td><a
									href="${pageContext.request.contextPath}/tags/delete/${tag.id}"><spring:message
											code="label.button.delete" /></a></td>

								<td><a href="${pageContext.request.contextPath}/tags/view"><spring:message
											code="label.button.cancel" /></a></td>

							</c:when>
							<c:otherwise>
								<td><input type="text" class="name" value="${tag.name}"
									disabled="disabled" /></td>
								<td><a
									href="${pageContext.request.contextPath}/tags/edit/${tag.id}"><spring:message
											code="label.button.edit" /></a></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
				
				<form:errors cssStyle="color: red" path="name" />
				
				<c:if test="${editingTagId == null}">
					<tr height="100px">
						<td><b><spring:message code="label.text.add_tag" />:</b></td>
						<td><input type="text" name="name" class="editTag" /></td>
						<td><input type="submit" class="linkLikeButton" value="<spring:message code="label.button.save" />">
						</td>
					</tr>
				</c:if>
			</table>
		</form:form>
	</div>
 