<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

	<jsp:include page="date-pattern.jsp"></jsp:include>
	<form action="/news-admin/news/delete/${newsVO.news.id}"
		method="post"  name="deleteForm">
		<ol class="newsList">
			<c:forEach var="newsVO" items="${newsVOList}">
				<li class="news">

					<div class="highPart">
						<div class="title">
							<a
								href="/news-admin/current/news/${newsVO.news.id}">${newsVO.news.title}</a>
						</div>
						<div class="author">(${newsVO.author.name})</div>
						<div class="modificationDate">
							<fmt:formatDate value="${newsVO.news.modificationDate}"
								pattern="${datePattern}" />
						</div>
					</div>

					<div class="middlePart">
						<div class="shortText">${newsVO.news.shortText}</div>
					</div>

					<div class="bottomPart">

						<div class="deleteCheckbox">
							<h6><a href="/news-admin/news/delete/${newsVO.news.id}">x</a></h6>
						</div>

						<div class="edit">
							<a href="/news-admin/edit_news/edit/${newsVO.news.id}">
								<spring:message code="label.button.edit"></spring:message>
							</a>
						</div>
						<div class="commentsCount">
							<spring:message code="label.message.comments" />
							(${newsVO.commentList.size()})
						</div>
						<div class="tagList">
							<c:forEach var="tag" items="${newsVO.tagList}">
								<c:out value="${tag.name}" />
							</c:forEach>
						</div>
					</div>
				<li>
			</c:forEach>
		</ol>
	

	</form>
