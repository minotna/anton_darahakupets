<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	
	<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-1.11.2.custom.min.js"/>"></script>
	
	<script type="text/javascript"
	src="<c:url value="/resources/js/ui.dropdownchecklist-1.5-min.js"/>"></script>
	<script type="text/javascript"
	src="<c:url value="/resources/js/ui.dropdownchecklist-1.5-min.js"/>"></script>
<table width="1000px" align="center">

		<tr>
			<!-- Search -->
			<td ><tiles:insertAttribute
					name="searchCriteria" /></td>
		</tr>

		<tr>
			<td align="center"><tiles:insertAttribute name="list" /></td>
		</tr>

		<tr>
			<td align="center"><tiles:insertAttribute name="pagination" /></td>
		</tr>

	</table>