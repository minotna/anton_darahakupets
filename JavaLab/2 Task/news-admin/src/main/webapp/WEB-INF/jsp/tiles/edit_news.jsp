  <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<jsp:include page="date-pattern.jsp"></jsp:include>
	
	<div align="left" class="topMenu">  
	<a href="/news-admin/news/watch">
		<button type="button" name="back">
			<spring:message code="label.button.back" />
		</button>
	</a>
	</div>
	<form:form commandName="newsBody" action="${pageContext.request.contextPath}/edit_news/save"   role="form">
		
			<form:errors cssStyle="color: red" path="news.title" />

			<form:errors cssStyle="color: red" path="news.modificationDate" />
		<br>
			<form:errors cssStyle="color: red" path="news.shortText" />
		<br>
			<form:errors cssStyle="color: red" path="news.fullText" />
		<br>
			<form:errors cssStyle="color: red" path="authorId" />
		<br> 
			<form:errors cssStyle="color: red" path="tagIdList" />
		<br> 

		<input type="hidden" name="news.id" value="${newsBody.news.id}" />

		<div class="editFrame">
			<table cellpadding="10px" align="right">
				<tr>
					<td><b><spring:message code="lavel.text.title" />:</b></td>
					<td><input type="text" name="news.title" 
							value="${newsBody.news.title}" class="editTitle" /></td>
				</tr>
				<tr>
					<td><b><spring:message code="lavel.text.date" />:</b></td>
					<c:choose>
						<c:when test="${currentDate != null}">
											<fmt:formatDate value="${currentDate}" pattern="${datePattern}"
								var="formattedCurrentDate" />
							<td><input type="text" value="${formattedCurrentDate}" disabled="disabled"/></td>
						</c:when>
						<c:otherwise>
							<fmt:formatDate value="${newsBody.news.creationDate}" pattern="${datePattern}"
								var="formattedCurrentDate" />
							<td><input type="text" value="${formattedCurrentDate}" disabled="disabled"/></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td><b><spring:message code="lavel.text.brief" />:</b></td>
					<td><textarea rows="3" name="news.shortText" style="resize: none;" class="brief">${newsBody.news.shortText}</textarea></td>
				</tr>
				<tr>
					<td><b><spring:message code="lavel.text.content" />:</b></td>
					<td><textarea name="news.fullText" style="resize: none;" class="content">${newsBody.news.fullText}</textarea></td>
				</tr>
			</table>

			<div class="editDropdownListsWrap">
				<div class="editDropdownLists">
					<select class="authorList" name="authorId" id="authorId">
						<option value="${null}">
							<spring:message code="label.select.author" /></option>
						<c:forEach var="author" items="${authors}">
							<c:choose>
								<c:when
									test="${ newsBody.news != null && newsBody.authorId != null && newsBody.authorId == author.id}">
									<option value="${author.id}" selected="selected">${author.name}</option>
								</c:when>
								<c:otherwise>
									<c:if test="${author.expired == null}">
										<option value="${author.id}">${author.name}</option>
									</c:if>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>

					<div class="multiselect">
						<div class="select_box" onclick="showCheckboxes()">
							<select>
								<option selected="selected" value="false"><spring:message
										code="label.select.taglist" /></option>
							</select>
							<div class="over_select" id="over_select"></div>
						</div>
						<div id="checkboxes">
							<c:forEach var="tag" items="${tags}">

								<c:set var="checked" value="false" />
								<c:if test="${ newsBody.news != null && newsBody.tagIdList != null}">
									<c:forEach var="selectedTag" items="${newsBody.tagIdList}">
										<c:if test="${selectedTag == tag.id }">
											<c:set var="checked" value="true" />
										</c:if>
									</c:forEach>
								</c:if>

								<c:choose>
									<c:when test="${checked==true }">
										<label for="${tag}"> <input type="checkbox"
											name="tagIdList" value="${tag.id}" checked="checked" /> <c:out
												value="${tag.name}" />
										</label>
									</c:when>
									<c:otherwise>
										<label for="${tag}"> <input type="checkbox"
											name="tagIdList" value="${tag.id}" /> <c:out
												value="${tag.name}" />
										</label>
									</c:otherwise>
								</c:choose>


							</c:forEach>
						</div>
					</div>
				</div>
			</div>
			
			<div class="saveButton">
				<input class="saveButton" type="submit"
					value="<spring:message code="label.button.save" />">
			</div>

		</div>
	</form:form>

 