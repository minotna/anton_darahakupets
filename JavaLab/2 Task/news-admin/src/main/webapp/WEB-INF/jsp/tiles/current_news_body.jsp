<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


	<jsp:include page="date-pattern.jsp"></jsp:include>
	<div class="newsView">

		<div class="highPart">
			<div class="title">${newsVO.news.title}</div>
			<div class="author">(${newsVO.author.name})</div>
			<div class="modificationDate">
				<fmt:formatDate value="${newsVO.news.modificationDate}"
					pattern="${datePattern}" />
			</div>
		</div>
		<div class="fullTextWrapper">
			<div class="fullText">${newsVO.news.fullText}</div>
		</div>

		<div class="commentList">
			<div class="commentScroll">
				<c:forEach var="comment" items="${newsVO.commentList}">
					<div class="comment">
						<div class="commentDate">
							<fmt:formatDate value="${comment.creationDate}"
								pattern="${datePattern}" />
						</div>
						<div class="commentText">
							<div class="deleteCommentButton">
								<form
									action="/news-admin/current/delete_comment/${comment.id}"
									method="post">
									<input type="hidden" name="newsId"
										value="${newsVO.news.id}"> <input type="hidden"
										name="commentId" value="${comment.id}"> <input
										type="submit" value="X">
								</form>
							</div>
							<div class="innerCommentText">${comment.commentText}</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<form action="/news-admin/current/add-comment"
				method="post" name="commentForm">
				<div class="newComment">
					<textarea name="commentText" class="newComment"></textarea>
				</div>
				<input type="submit" value="<spring:message code="label.button.post_comment" />"
					class="postComment"> <input type="hidden" name="newsId"
					value="${newsVO.news.id}">
			</form>
		</div>

	</div>

