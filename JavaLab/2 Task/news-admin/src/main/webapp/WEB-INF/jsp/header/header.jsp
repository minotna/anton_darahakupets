<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<body>
	<div class="header">
		<div class="text">
			<spring:message code="label.head.name"></spring:message>
		</div>
		<div class="rightPart">
			<security:authorize access="hasRole('ROLE_ADMIN')">
				<div class="logout">
					<spring:message code="hello"></spring:message>,
					<security:authentication property="principal.username" />
					<a href="<c:url value='/news-admin/j_spring_security_logout'/>"><spring:message
							code="label.head.logout"></spring:message></a>
				</div>
			</security:authorize>
			<div class="locale">
				<a href="?lang=en"><spring:message code="label.button.en" /></a> <a
					href="?lang=ru"><spring:message code="label.button.ru" /></a>
			</div>
		</div>
	</div>
</body>
</html>