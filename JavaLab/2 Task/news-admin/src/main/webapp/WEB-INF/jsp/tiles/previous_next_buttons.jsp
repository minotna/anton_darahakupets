<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<table  width="900px">
	<tr>
		<td align="left">
		<form action="/news-admin/current/news/${newsVO.news.id}" id="previous" method="post">
		<input type="hidden" name="previous" value="previous">
		</form>
		<button class="button" type="submit"  form="previous"><spring:message code="label.button.previous"/></button>
		</td>

		<td align="right">
		<form action="/news-admin/current/news/${newsVO.news.id}" id="next" method="post">
		<input type="hidden" name="next" value="next">
		</form>
		<button class="button" type="submit" form="next"><spring:message code="label.button.next"/></button>
		</td>
	</tr>
</table>