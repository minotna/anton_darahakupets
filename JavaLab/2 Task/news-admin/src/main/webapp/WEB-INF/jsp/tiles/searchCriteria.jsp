<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

 	<div class="filter">

		<div class="filterForm">

			<form:form  commandName="searchCriteria" action="${pageContext.request.contextPath}/news/setSearchCriteria">

				<select class="authorList" name="authorId">
					<option value="${null}">
						<spring:message code="label.select.author" /></option>
					<c:forEach var="author" items="${authors}">
						<c:choose>
							<c:when
								test="${ searchCriteria != null && searchCriteria.authorId != null && searchCriteria.authorId == author.id}">
								<option value="${author.id}" selected="selected">${author.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${author.id}">${author.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>

				<div class="multiselect">
					<div class="select_box" onclick="showCheckboxes()">
						<select>
							<option selected="selected" value="false"><spring:message
									code="label.select.taglist" /></option>
						</select>
						<div class="over_select" id="over_select"></div>
					</div>
					<div id="checkboxes">
						<c:forEach var="tag" items="${tags}">

							<c:set var="checked" value="false" />
							<c:if test="${ searchCriteria != null && searchCriteria.tagIdList != null}">
								<c:forEach var="selectedTag" items="${searchCriteria.tagIdList}">
									<c:if test="${selectedTag == tag.id }">
										<c:set var="checked" value="true" />
									</c:if>
								</c:forEach>
							</c:if>

							<c:choose>
								<c:when test="${checked==true }">
									<label for="${tag}">
									 	<input type="checkbox" name="tagIdList" value="${tag.id}" checked="checked" />
									 	<c:out	value="${tag.name}" />
									</label>
								</c:when>
								<c:otherwise>
									<label for="${tag}"> 
										<input type="checkbox" name="tagIdList" value="${tag.id}" />
										 <c:out	value="${tag.name}" />
									</label>
								</c:otherwise>
							</c:choose>


						</c:forEach>
					</div>
				</div>
				
				<div class="button">
					<input type="submit"
						value="<spring:message code="label.button.filter" />" class="button" />
				</div>
			</form:form>
		</div>

		<div class="button">
			<form action="${pageContext.request.contextPath}/news/reset"
				method="post">
				<input type="submit" value="<spring:message code="label.button.reset" />"
					class="button" />
			</form>
		</div>
	</div>