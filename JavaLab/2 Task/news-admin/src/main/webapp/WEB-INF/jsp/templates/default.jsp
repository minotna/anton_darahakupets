<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<html>
<head>
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	
	<script type="text/javascript"
	src="<c:url value="/resources/js/checkbox-dropdown.js"/>"></script>
	

	<script type="text/javascript"
	src="<c:url value="/resources/js/style.js"/>"></script>


</head>
<body class="page">
	<tiles:insertAttribute name="header" />
	<table class="body">
		<tr>
			<td class="menu"><tiles:insertAttribute name="menu" ignore="true" /></td>
			<td valign="top"><tiles:insertAttribute name="body" /></td>
		</tr>
	</table>
	<tiles:insertAttribute name="footer" />

</body>
</html>