<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<div class="menuList">
		<div class="menuItem">
			<a href="/news-admin/news/watch"><spring:message code="label.menu.newslist"/></a>
		</div>
		<div class="menuItem">
			<a href="/news-admin/edit_news/view"><spring:message code="label.menu.addnews"/></a>
		</div>
		<div class="menuItem">
			<a href="/news-admin/authors/view"><spring:message code="label.menu.editauthors"/></a>
		</div>
		<div class="menuItem">
			<a href="/news-admin/tags/view"><spring:message code="label.menu.edittags"/></a>
		</div>
	</div>


