 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 	
 	<div align="left" class="topMenu">  
		<a href="/news-admin/news/watch">
			<button type="button" name="back">
				<spring:message code="label.button.back" />
			</button>
		</a>
	</div>
 	<div class="editAuthorList">
		<form:form commandName="author" action="${pageContext.request.contextPath}/authors/save" name="authorForm">
			
			<form:errors cssStyle="color: red" path="name" />
			
			<table cellpadding="10px">
				
				<c:forEach items="${authorList}" var="author">
					<tr>
						<c:if test="${author.expired == null}">
							<td><b><spring:message code="label.text.author" />:</b></td>
							<c:choose>
								<c:when test="${editingAuthorId == author.id}">

									<td><input type="text" name="name"
										class="editAuthor" value="${author.name}" /></td>

									<td><input type="hidden" name="id"
										value="${author.id}"> 
										<input type="submit" class="linkLikeButton"
										value="<spring:message
												code="label.button.update" />"></td>

									<td><a
										href="${pageContext.request.contextPath}/authors/delete/${author.id}"><spring:message
												code="label.message.expired" /></a></td>

									<td><a href="${pageContext.request.contextPath}/authors/view"><spring:message
												code="label.button.cancel" /></a></td>

								</c:when>
								<c:otherwise>
									<td><input type="text" class="editAuthor"
										value="${author.name}" disabled="disabled" /></td>
									<td><a
										href="${pageContext.request.contextPath}/authors/edit/${author.id}"><spring:message
												code="label.button.edit" /></a></td>
								</c:otherwise>
							</c:choose>
						</c:if>
					</tr>
				</c:forEach>

				<c:if test="${editingAuthorId == null}">
					<tr height="100px">
						<td><b><spring:message code="label.text.add_author" />:</b></td>
						<td><input type="text" name="name" class="editAuthor" /></td>
						<td><input type="submit" class="linkLikeButton" value="<spring:message code="label.button.save" />"/></td>
					</tr>
				</c:if>

			</table>
		</form:form>
	</div>