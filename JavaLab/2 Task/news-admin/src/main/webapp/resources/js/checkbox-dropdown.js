var expanded = false;
function showCheckboxes() {
	var checkboxes = document.getElementById("checkboxes");

	if (!expanded) {
		checkboxes.style.display = "block";
		expanded = true;
	} else {
		checkboxes.style.display = "none";
		expanded = false;
	}
}
window.addEventListener('mouseup', function(event) {
	var checkboxes = document.getElementById('checkboxes');
	var over_select = document.getElementById('over_select');
	if (event.target != over_select && event.target != checkboxes
			&& event.target.parentNode != checkboxes
			&& event.target.parentNode.parentNode != checkboxes) {
		checkboxes.style.display = 'none';
		expanded = false;
	}
});
