package com.epam.newsmanagement.implService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

@ContextConfiguration(locations = {"/testSpringContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceImplTest {

	@Autowired
	private TagDAO mockAopTagDAO;
	
	@InjectMocks
	@Autowired
	private TagService tagService;
	

	@Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void testCreate() throws DAOException, ServiceException {
		Tag expectedTag = new Tag();
		Long expectedTagId = 1L;
		expectedTag.setId(expectedTagId);
		
		
		when(mockAopTagDAO.create(expectedTag)).thenReturn(expectedTag.getId());
		
		Long actualAuthorId = tagService.create(expectedTag);
		
		verify(mockAopTagDAO, times(1)).create(expectedTag);
		assertEquals(expectedTagId, actualAuthorId);
	}
	
	@Test
	public void testRead() throws ServiceException, DAOException {
		Tag expectedTag = new Tag();
		Long tagId = 1L;
		String name = "TestTag";
		expectedTag.setId(tagId);
		expectedTag.setName(name);
		
		when(tagService.read(tagId)).thenReturn(expectedTag);
		
		Tag actualTag = tagService.read(expectedTag.getId());
		
		verify(mockAopTagDAO, times(1)).read(tagId);
		assertEquals(expectedTag, actualTag);
	}
	
	@Test
	public void testUpdate() throws ServiceException, DAOException {
		Tag tag = new Tag();
		tagService.update(tag);
		verify(mockAopTagDAO,times(1)).update(tag);
	}
	
	@Test
	public void testDelete() throws ServiceException, DAOException {
		Tag tag = new Tag();
		tagService.delete(tag.getId());
		verify(mockAopTagDAO,times(1)).delete(tag.getId());
	}
	
	@Test
	public void testGetAllTags() throws ServiceException, DAOException {
		tagService.getAllTagsFromNews(anyLong());
		
		verify(mockAopTagDAO, times(1)).getAllTagsFromNews(anyLong());
	}
	
	@Test 
	public void testAddListTagsForNews() throws ServiceException, DAOException {
		tagService.addListTagsForNews(anyLong(), anyListOf(Long.class));
		
		verify(mockAopTagDAO, times(1)).addListTagsForNews(anyLong(), anyListOf(Long.class));
	}
	
	@Test
	public void testAddTagForNews() throws ServiceException, DAOException {
		tagService.addTagForNews(anyLong(), anyLong());
		
		verify(mockAopTagDAO, times(1)).addTagForNews(anyLong(), anyLong());
	}
	
	@Test
	public void testDeleteAllTags() throws ServiceException, DAOException {
		tagService.deleteAllTags(anyLong());
		
		verify(mockAopTagDAO, times(1)).deleteAllTags(anyLong());
	}
}