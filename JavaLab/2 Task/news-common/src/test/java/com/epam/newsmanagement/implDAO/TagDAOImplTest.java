package com.epam.newsmanagement.implDAO;


import java.util.ArrayList;
import java.util.List;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/testSpringContext.xml" })
public class TagDAOImplTest extends DBTestCase {
	
	@Autowired
	private TagDAO tagDAO;

	@Autowired
	private IDatabaseTester tester;

	
	@Before
	public void setUp() throws Exception {
		IDataSet dataSet = getDataSet();
		tester.setDataSet(dataSet);
		tester.onSetup();
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@After
	public void tearDown() throws Exception {
		tester.setTearDownOperation(getTearDownOperation());
		tester.onTearDown();
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream("/tagDataSet.xml"));
	}

	@Test
	public void testCreate() throws Exception {
		Tag expectedTag = new Tag();
		String tagName = "Test tag";
		expectedTag.setName(tagName);
		
		Long tagId = tagDAO.create(expectedTag);
		Tag actualTag = tagDAO.read(tagId);
		
		assertEquals(expectedTag.getName(), actualTag.getName());
	}	
	
	@Test
	public void testRead() throws DAOException {
		Tag expectedTag = new Tag();
		Long tagId = 4L;
		String tagName = "TV";
		
		expectedTag.setId(tagId);
		expectedTag.setName(tagName);
		
		Tag actualTag = tagDAO.read(tagId);
		
		assertEquals(expectedTag.getId(), actualTag.getId());
		assertEquals(expectedTag.getName(), actualTag.getName());
	}
	
	@Test 
	public void testUpdate() throws DAOException {
		Tag expectedTag = new Tag();
		Long tagId = 4L;
		String newsTagName = "New tag";
		
		expectedTag.setId(tagId);
		expectedTag.setName(newsTagName);
		
		tagDAO.update(expectedTag);
		Tag actualTag = tagDAO.read(tagId);
		
		assertEquals(expectedTag.getId(), actualTag.getId());
		assertEquals(expectedTag.getName(), actualTag.getName());
	}
	
	@Test
	public void testDelete() throws DAOException {
		Long tagId = 1L;
		
		tagDAO.delete(tagId);
		Tag actualTag = tagDAO.read(tagId);
		
		assertNull(actualTag);
	}
	
	@Test 
	public void testGetAllTagsByNewsId() throws DAOException {
		Long newsId = 1L;
		int expectedSize = 2;
		List<Tag> actualTagsList = null;
		
		actualTagsList = tagDAO.getAllTagsFromNews(newsId);
		
		assertEquals(expectedSize, actualTagsList.size());
	}
	
	@Test
	public void testDeleteAllTagsByNewsId() throws DAOException {
		Long newsId = 1L;
		int expectedSize = 0;
		List<Tag> actualTagsList = null;
		
		tagDAO.deleteAllTags(newsId);
		actualTagsList = tagDAO.getAllTagsFromNews(newsId);
		
		assertEquals(expectedSize, actualTagsList.size());
	}
	
	@Test
	public void testAddTagForNews() throws DAOException {
		int expectedSize = 3;
		List<Tag> actualTagsList = null;
 		Long tagId = 3L;
		Long newsId = 1L;
		
		tagDAO.addTagForNews(newsId, tagId);
		actualTagsList = tagDAO.getAllTagsFromNews(newsId);
		
		assertEquals(expectedSize, actualTagsList.size());
	}
	
	@Test
	public void testAddListTagsForNews() throws DAOException {
		int expectedSize = 3;
		List<Tag> actualTagsList = null;
		Long newsId = 3L;
		
		List<Long> idTagsList = new ArrayList<>();
		Long tagId1 = 1L;
		Long tagId2 = 2L;
		idTagsList.add(tagId1);
		idTagsList.add(tagId2);
		
		tagDAO.addListTagsForNews(newsId, idTagsList);
		actualTagsList = tagDAO.getAllTagsFromNews(newsId);
		
		assertEquals(expectedSize, actualTagsList.size());
	}
}