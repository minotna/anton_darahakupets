package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class CommentServiceImpl implements CommentService {
	
	private CommentDAO commentDAO;
	
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}	

	@Override
	public Long create(Comment entity) throws ServiceException {
		try {
			return commentDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Comment read(Long id) throws ServiceException {
		try {
			return commentDAO.read(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(Comment entity) throws ServiceException {
		try {
			commentDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			commentDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Comment> getAllCommentsFromNews(Long newsId) throws ServiceException {
		try {
			return commentDAO.getAllCommentsFromNews(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteAllCommentsFromNews(Long newsId) throws ServiceException {
		try {
			commentDAO.deleteAllCommentsFromNews(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addListCommentsForNews(List<Comment> commentsList) throws ServiceException {
		try {
			commentDAO.addListCommentsToNews(commentsList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}