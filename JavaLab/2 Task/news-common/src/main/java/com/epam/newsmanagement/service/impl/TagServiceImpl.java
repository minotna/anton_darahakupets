package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class TagServiceImpl implements TagService {
	
	private TagDAO tagDAO;
	
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	@Override
	public Long create(Tag entity) throws ServiceException {
		try {
			return tagDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Tag read(Long id) throws ServiceException {
		try {
			return tagDAO.read(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(Tag entity) throws ServiceException {
		try {
			tagDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			tagDAO.detachTagFromAllNews(id);
			tagDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> getAllTagsFromNews(Long newsId) throws ServiceException {
		try {
			return tagDAO.getAllTagsFromNews(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteAllTags(Long newsId) throws ServiceException {
		try {
			tagDAO.deleteAllTags(newsId);;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addListTagsForNews(Long newsId, List<Long> idTagsList) throws ServiceException {
		try {
			tagDAO.addListTagsForNews(newsId, idTagsList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addTagForNews(Long newsId, Long tagId) throws ServiceException {
		try {
			tagDAO.addTagForNews(newsId, tagId);;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<Tag> getAllTags() throws ServiceException {
		try {
			return tagDAO.getAllTags();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}