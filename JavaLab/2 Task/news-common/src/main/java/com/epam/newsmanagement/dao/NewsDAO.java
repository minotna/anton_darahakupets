package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;

/**HashMap
 * @author Anton_Darahakupets
 */
public interface NewsDAO extends NewsManagementDAO<News, Long> {

	/**
	 * Return news with satisfy search criteria 
	 * @param searchCriteria search criteria
	 * @param startIndex Start index of news
	 * @param lastIndex Last index of news
	 * @return Return list of news that satisfy search criteria 
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	List<News> getNewsWithSearchCriteria(SearchCriteria searchCriteria, int startIndex, int lastIndex) throws DAOException;
	
	/**
	 * Return the number of news that satisfy search criteria
	 * @param searchCriteria Search criteria
	 * @return int the number of news
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	int getNewsNumber(SearchCriteria searchCriteria) throws DAOException;
	
	/**
	 * 
	 * @param searchCriteria
	 * @param newsId
	 * @return
	 * @throws DAOException
	 */
	int findIndex(SearchCriteria searchCriteria, Long newsId) throws DAOException;
}