package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public interface TagDAO extends NewsManagementDAO<Tag, Long> {
	
	/**
	 * Returns a list of the news tags by newsId
	 * @param newsId News id
	 * @return List of tags by newsId
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	List<Tag> getAllTagsFromNews(Long newsId) throws DAOException;
	
	/**
	 * Delete all tags from news by newsId
	 * @param newsId News id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void deleteAllTags(Long newsId) throws DAOException;
	
	/**
	 * Add list of tags for the news
	 * @param newsId News id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void addListTagsForNews(Long newsId, List<Long> idTagsList) throws DAOException;
	
	/**
	 * Add one tag for news with newsId
	 * @param newsId News id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void addTagForNews(Long newsId, Long tagId) throws DAOException;
	
	/**
	 * Return all tags from database
	 * @return List<Tag>
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	List<Tag> getAllTags() throws DAOException;
	
	/**
	 * Delete tag from all news which include this tag
	 * @param tagid Tag id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void detachTagFromAllNews(Long tagId) throws DAOException; 
}	