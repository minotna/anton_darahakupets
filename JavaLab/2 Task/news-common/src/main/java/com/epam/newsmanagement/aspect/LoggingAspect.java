package com.epam.newsmanagement.aspect;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.mockito.cglib.core.ProcessArrayCallback;

/**
 * Aspect with loggingService advice for logging Service layer
 * @author Anton_Darahakupets
 *
 */
public class LoggingAspect {
	private final static Logger LOGGER = Logger.getLogger(LoggingAspect.class);
 
    public Object loggingService(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        LOGGER.info("Before invoking  method" + " " + "\"" + proceedingJoinPoint.getSignature().toString() + "\"");
        
        Signature sig = proceedingJoinPoint.getSignature();
        Object[] args = proceedingJoinPoint.getArgs();
        String location = sig.getDeclaringTypeName() + '.' + sig.getName() + ", args=" + Arrays.toString(args);
        LOGGER.info(location);
        
        Object value = null;
        try {
        	value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
        	LOGGER.error("Error during execution method" + " " + "\"" + proceedingJoinPoint.getSignature().getName().toString() + "\"");
            throw e;
        }
        LOGGER.info("After invoking  method" + " " + "\"" + proceedingJoinPoint.getSignature().getName().toString() + "\"\n");
        return value;
    }
}