package com.epam.newsmanagement.service;

import com.epam.newsmanagement.exception.ServiceException;

/**
 * Main interface to be implemented by objects is used for implementation C.R.U.D operations 
 * @author Anton_Darahakupets
 * @param <Entity> The type of the object that will be used for C.R.U.D. operations
 * @param <TypeId> The type of the id that will be used for C.R.U.D operations
 */
public interface NewsManagementService<Entity, TypeId> {
	
	/**
	 * Create new entity in database
	 * @param entity Representation entity by the Entity object
	 * @return The id of the entity that is created in database
	 * @throws ServiceException if there is some trouble with Persistence layer
	 * 
	 */
	TypeId create(Entity entity) throws ServiceException;
	
	/**
	 * Read entity from database
	 * @param id The id of the entity in database
	 * @return Entity object that represents an entity
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	Entity read(TypeId id) throws ServiceException;
	
	/**
	 * Update entity in database
	 * @param entity Entity object that represents an entity 
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void update(Entity entity) throws ServiceException;

	/**
	 * Delete entity from database
	 * @param id The id of the entity in database
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void delete(TypeId id) throws ServiceException;
}