package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class CommentDAOImpl implements CommentDAO {
	
	private static final String SQL_CREATE_COMMENT = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (comments_comment_id_seq.nextval,?,?,SYSDATE)";
	private static final String SQL_READ_COMMENT_BY_ID = "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE comment_id= ? ";
	private static final String SQL_UPDATE_COMMENT_BY_ID = "UPDATE comments SET news_id = ?, comment_text = ? WHERE comment_id = ? ";
	private static final String SQL_DELETE_COMMENT_BY_ID = "DELETE FROM comments WHERE comment_id = ? ";
	private static final String SQL_READ_ALL_COMMENTS_NEWS = "SELECT comment_id, comment_text, creation_date FROM comments WHERE news_id = ? ";
	private static final String SQL_DELETE_ALL_COMMENTS_NEWS = "DELETE FROM comments WHERE news_id = ? ";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long create(Comment entity) throws DAOException {
		Long id = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_CREATE_COMMENT, new String[] {"COMMENT_ID"});
			Long newsId = entity.getNewsId();
			String text = entity.getCommentText();
			statement.setLong(1, newsId);
			statement.setString(2, text);
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
			} else {
				throw new DAOException("Creating user failed, no ID obtained.");
			}
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return id;
	}

	@Override
	public Comment read(Long id) throws DAOException {
		Comment comment = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_COMMENT_BY_ID);
			statement.setLong(1,id);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				comment = new Comment();
				Long commentId = resultSet.getLong(1);
				Long newsId = resultSet.getLong(2);
				String commentText = resultSet.getString(3);
				Timestamp creationDate = resultSet.getTimestamp(4);
				comment.setId(commentId);
				comment.setNewsId(newsId);
				comment.setCommentText(commentText);
				comment.setCreationDate(creationDate);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}	
		return comment;
	}

	@Override
	public void update(Comment entity) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_COMMENT_BY_ID);
			Long commentId = entity.getId();
			Long newsId = entity.getNewsId();
			String commentText = entity.getCommentText();
			statement.setLong(1, newsId);
			statement.setString(2, commentText);
			statement.setLong(3, commentId);
			statement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_COMMENT_BY_ID);
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
		
	}
	
	@Override
	public List<Comment> getAllCommentsFromNews(Long newsId) throws DAOException {
		List<Comment> commentsList = null;
		Comment comment = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_ALL_COMMENTS_NEWS);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			commentsList = new ArrayList<>();
			while (resultSet.next()) {
				Long commentId = resultSet.getLong(1);
				String commentText = resultSet.getString(2);
				Timestamp creationDate = resultSet.getTimestamp(3);
				comment = new Comment();
				comment.setId(commentId);
				comment.setNewsId(newsId);
				comment.setCommentText(commentText);
				comment.setCreationDate(creationDate);
				commentsList.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return commentsList;
	}
	
	@Override
	public void deleteAllCommentsFromNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_ALL_COMMENTS_NEWS);
			statement.setLong(1, newsId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}	
	}
	
	@Override
	public void addListCommentsToNews(List<Comment> commentsList) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_CREATE_COMMENT);
			for (Comment comment : commentsList) {
				statement.setLong(1, comment.getNewsId());
				statement.setString(2, comment.getCommentText());
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}	
	}	
}