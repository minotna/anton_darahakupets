CREATE TABLE authors
  (
    author_id NUMBER (20) NOT NULL ,
    author_name NVARCHAR2 (30) NOT NULL ,
    expired TIMESTAMP
  ) ;
ALTER TABLE authors ADD CONSTRAINT authors_PK PRIMARY KEY ( author_id ) ;

CREATE TABLE comments
  (
    comment_id NUMBER (20) NOT NULL ,
    news_id    NUMBER (20) NOT NULL ,
    comment_text NVARCHAR2 (100) NOT NULL ,
    creation_date TIMESTAMP NOT NULL
  ) ;
ALTER TABLE comments ADD CONSTRAINT comments_PK PRIMARY KEY (comment_id) ;

CREATE TABLE news
  (
    news_id NUMBER (20) NOT NULL ,
    title NVARCHAR2 (30) NOT NULL ,
    short_text NVARCHAR2 (100) NOT NULL ,
    full_text NVARCHAR2 (2000) NOT NULL ,
    creation_date     TIMESTAMP NOT NULL ,
    modification_date DATE NOT NULL
  ) ;
ALTER TABLE news ADD CONSTRAINT news_PK PRIMARY KEY (news_id) ;

CREATE TABLE news_authors
  (
    news_id   NUMBER (20) NOT NULL ,
    author_id NUMBER (20) NOT NULL
  ) ;

CREATE TABLE news_tags
  (
    news_id NUMBER (20) NOT NULL ,
    tag_id  NUMBER (20) NOT NULL
  ) ;

CREATE TABLE roles
  (
    user_id   NUMBER (20) NOT NULL ,
    role_name VARCHAR2 (50) NOT NULL
  ) ;

CREATE TABLE tags
  (
    tag_id NUMBER (20) NOT NULL ,
    tag_name NVARCHAR2 (30) NOT NULL
  ) ;
ALTER TABLE tags ADD CONSTRAINT tag_PK PRIMARY KEY (tag_id) ;

CREATE TABLE users
  (
    user_id NUMBER (20) NOT NULL ,
    user_name NVARCHAR2 (50) NOT NULL ,
    login    VARCHAR2 (30) NOT NULL ,
    password VARCHAR2 (32) NOT NULL
  ) ;

ALTER TABLE users ADD CONSTRAINT user_PK PRIMARY KEY (user_id) ;

ALTER TABLE news_tags ADD CONSTRAINT news_tag_Fi_news FOREIGN KEY (news_id) REFERENCES news (news_id);

ALTER TABLE comments ADD CONSTRAINT comments_Fi_news FOREIGN KEY (news_id) REFERENCES news (news_id) ;

ALTER TABLE news_authors ADD CONSTRAINT news_authors_Fi_authors FOREIGN KEY (author_id) REFERENCES authors (author_id) ;

ALTER TABLE news_authors ADD CONSTRAINT news_authors_Fi_news FOREIGN KEY (news_id) REFERENCES news (news_id);

ALTER TABLE news_tags ADD CONSTRAINT news_tag_Fi_tags FOREIGN KEY (tag_id) REFERENCES tags (tag_id) ;

ALTER TABLE roles ADD CONSTRAINT roles_Fi_user FOREIGN KEY (user_id) REFERENCES users (user_id) ;

CREATE SEQUENCE news_news_id_seq START WITH 10 NOCACHE ORDER ;

CREATE SEQUENCE users_user_id_seq START WITH 10 NOCACHE ORDER ;

CREATE SEQUENCE authors_author_id_seq START WITH 10 NOCACHE ORDER ;

CREATE SEQUENCE comments_comment_id_seq START WITH 10 NOCACHE ORDER ;

CREATE SEQUENCE tags_tag_id_seq START WITH 10 NOCACHE ORDER ;
