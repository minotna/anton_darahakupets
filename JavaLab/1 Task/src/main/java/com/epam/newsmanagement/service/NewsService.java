package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface NewsService is used for implementation methods that work with News object in Service layer
 * @author Anton_Darahakupets
 *
 */
public interface NewsService extends NewsManagementService<News, Long> {
	
	/**
	 * Get all news satisfy search criteria
	 * @param searchCriteria
	 * @return list of news
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	List<News> getNewsWithSearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Editing news that include updating tags, author and news message in one transaction
	 * @param news The News object that represent news message in database
	 * @param idTagsList The id list of tags for adding to the news 
	 * @param authorId The id of the author for adding to the news
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void editNews(News news, List<Long> idTagsList, Long authorId) throws ServiceException;
	
	/**
	 * Delete news in full in one transaction : detaching all tags, author of the news
	 *  and deleting all comments 
	 * @param newsId The id of the news that is represented in database
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Create news with news message, tags and author in one transaction
	 * @param authorId The value of the author id. 
	 * @param news The News object that represent news message in database
	 * @param idTagsList The id list of tags for adding to news 
	 * @return newsId Return the id of the news in database
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	Long addNews(Long authorId, News news, List<Long> idTagsList) throws ServiceException;
	
}