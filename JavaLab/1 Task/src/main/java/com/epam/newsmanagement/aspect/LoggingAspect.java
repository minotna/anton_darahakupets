package com.epam.newsmanagement.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Aspect with loggingService advice for logging Service layer
 * @author Anton_Darahakupets
 *
 */
public class LoggingAspect {
	private final static Logger LOGGER = Logger.getLogger(LoggingAspect.class);
 
    public Object loggingService(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        LOGGER.info("Before invoking  method" + " " + "\"" + proceedingJoinPoint.getSignature().getName().toString() + "\"");
        Object value = null;
        try {
        	value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
        	LOGGER.error("Error during execution method" + " " + "\"" + proceedingJoinPoint.getSignature().getName().toString() + "\"");
            throw e;
        }
        LOGGER.info("After invoking  method" + " " + "\"" + proceedingJoinPoint.getSignature().getName().toString() + "\"");
        return value;
    }
}