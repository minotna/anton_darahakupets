package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public interface CommentDAO extends NewsManagementDAO<Comment, Long> {
	
	/**
	 * Returns a list of the news comments by newsId
	 * @param newsId News id
	 * @return List of comments by newsId
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	List<Comment> getAllCommentsFromNews(Long newsId) throws DAOException;
	
	/**
	 * Delete all comments from news by newsId
	 * @param newsId News id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void deleteAllCommentsFromNews(Long newsId) throws DAOException;
	
	/**
	 * Add list of comments for the news
	 * @param commentsList List of comments
	 * @throws DAOException
	 */
	void addListCommentsToNews(List<Comment> commentsList) throws DAOException;
	
}