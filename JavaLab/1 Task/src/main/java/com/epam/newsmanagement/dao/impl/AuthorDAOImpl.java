package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class AuthorDAOImpl implements AuthorDAO {
	
	private DataSource dataSource;
	
	private static final String SQL_CREATE_NEW_AUTHOR = "INSERT INTO authors  (author_id, author_name) VALUES (authors_author_id_seq.nextval, ? ) ";
	private static final String SQL_READ_AUTHOR_BY_ID = "SELECT author_id, author_name, expired FROM authors WHERE author_id = ? ";
	private static final String SQL_UPDATE_AUTHOR_BY_ID = "UPDATE authors SET author_name = ? WHERE author_id = ? " ;
	private static final String SQL_DELETE_AUTHOR_BY_ID = "UPDATE authors SET expired = SYSDATE WHERE author_id = ? ";
	private static final String SQL_INSERT_NEWS_AUTHOR = "INSERT INTO news_authors (news_id, author_id) VALUES ( ?, ? ) ";
	private static final String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM news_authors WHERE news_id = ? ";
	private static final String SQL_READ_AUTHOR_WITH_NEWS_ID = "SELECT authors.author_id, authors.author_name, authors.expired FROM authors INNER JOIN news_authors  ON authors.author_id = news_authors.author_id  WHERE news_id = ?";	
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long create(Author entity) throws DAOException {
		Long id = null;
		Connection connection= null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_CREATE_NEW_AUTHOR, new String[] {"AUTHOR_ID"});
			String authorName = entity.getName();
			statement.setString(1, authorName);
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
			} else {
				throw new DAOException("Creating user failed, no ID obtained.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return id;
	}

	@Override
	public Author read(Long id) throws DAOException {
		Author author = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_AUTHOR_BY_ID);
			statement.setLong(1, id);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				author = new Author();
				Long authorId = resultSet.getLong(1);
				String authorName = resultSet.getString(2);
				Timestamp expired = resultSet.getTimestamp(3);
				author.setId(authorId);
				author.setName(authorName);
				author.setExpired(expired);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}	
		return author;
	}
	
	
	@Override
	public void update(Author entity) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_AUTHOR_BY_ID);
			Long authorId = entity.getId();
			String authorName = entity.getName();
			statement.setString(1, authorName);
			statement.setLong(2, authorId);
			statement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection  = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_AUTHOR_BY_ID);
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}

	@Override
	public void addAuthorForNews(Long newsId, Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_INSERT_NEWS_AUTHOR);
			statement.setLong(1, newsId);
			statement.setLong(2, authorId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closeConnection(dataSource, connection, statement);
		}	
	}

	@Override
	public void deleteAuthorFromNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR);
			statement.setLong(1, newsId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}

	@Override
	public Author getNewsAuthor(Long newsId) throws DAOException {
		Author author = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_AUTHOR_WITH_NEWS_ID);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				author = new Author();
				Long authorId = resultSet.getLong(1);
				String name = resultSet.getString(2);
				Timestamp expired = resultSet.getTimestamp(3);
				author.setId(authorId);
				author.setName(name);
				author.setExpired(expired);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return author;
	}
}