package com.epam.newsmanagement.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class AuthorServiceImpl implements AuthorService {
	
	private AuthorDAO authorDAO;
	
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	
	@Override
	public Long create(Author entity) throws ServiceException {
		try {
			return authorDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Author read(Long id) throws ServiceException {
		try {
			return authorDAO.read(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(Author entity) throws ServiceException {
		try {
			authorDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			authorDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addAuthorForNews(Long newsId, Long authorId) throws ServiceException {
		try {
			authorDAO.addAuthorForNews(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteAuthorFromNews(Long newsId) throws ServiceException {
		try {
			authorDAO.deleteAuthorFromNews(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Author getNewsAuthor(Long newsId) throws ServiceException {
		try {
			return authorDAO.getNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}