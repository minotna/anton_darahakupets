package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class NewsServiceImpl implements NewsService {
	
	private NewsDAO newsDAO;
	private AuthorDAO authorDAO;
	private CommentDAO commentDAO;
	private TagDAO tagDAO;
	
	
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
	
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	@Override
	public Long create(News entity) throws ServiceException {
		try {
			return newsDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public News read(Long id) throws ServiceException {
		try {
			return newsDAO.read(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(News entity) throws ServiceException {
		try {
			newsDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			newsDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> getNewsWithSearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.getNewsWithSearchCriteria(searchCriteria);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void editNews(News news, List<Long> idTagsList, Long authorId) throws ServiceException  {
		try {
			authorDAO.deleteAuthorFromNews(news.getId());
			tagDAO.deleteAllTags(news.getId());
			newsDAO.update(news);
			authorDAO.addAuthorForNews(news.getId(), authorId);
			tagDAO.addListTagsForNews(news.getId(), idTagsList);	
		} catch (DAOException e) {
			throw new ServiceException(e);
		}	
	}
	
	@Override
	public Long addNews(Long authorId, News news, List<Long> idTagsList) throws ServiceException {
		Long newsId= null;
		try {
			newsId = newsDAO.create(news);
			authorDAO.addAuthorForNews(newsId, authorId);
			tagDAO.addListTagsForNews(newsId, idTagsList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsId;
	}
		
	@Override
	public void deleteNews(Long newsId) throws ServiceException {
		try {
			commentDAO.deleteAllCommentsFromNews(newsId);
			tagDAO.deleteAllTags(newsId);
			authorDAO.deleteAuthorFromNews(newsId);
			newsDAO.delete(newsId);	
		} catch (DAOException e) {
			throw new ServiceException(e);
		}	
	}	
}