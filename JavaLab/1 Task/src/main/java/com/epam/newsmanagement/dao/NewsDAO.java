package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;

/**HashMap
 * @author Anton_Darahakupets
 */
public interface NewsDAO extends NewsManagementDAO<News, Long> {
	/**
	 * Get news with satisfy search criteria (If parameter serachCriteria is null method will return all news)
	 * @param searchCriteria search criteria
	 * @return Return list of news that satisfy search criteria 
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	List<News> getNewsWithSearchCriteria(SearchCriteria searchCriteria) throws DAOException;	
}