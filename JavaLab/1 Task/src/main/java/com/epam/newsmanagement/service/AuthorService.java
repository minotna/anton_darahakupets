package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface AuthorService is used for implementation methods that work with Author object in Service layer
 * @author Anton_Darahakupets
 *
 */
public interface AuthorService extends NewsManagementService<Author, Long> {
	/**
	 * Add author with authorId for news with newsId
	 * @param newsId News id
	 * @param AuthorId Author id
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void addAuthorForNews(Long newsId, Long authorId) throws ServiceException;
	
	/**
	 * Delete author with aouthorId from news with newsId
	 * @param newsId News id
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void deleteAuthorFromNews(Long newsId) throws ServiceException;
	
	/**
	 * Return author of news with newsId
	 * @param newsId News is
	 * @return Author 
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	Author getNewsAuthor(Long newsId) throws ServiceException;
}