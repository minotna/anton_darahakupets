package com.epam.newsmanagement.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.exception.DAOException;

/**
 * Main interface to be implemented by objects is used for implementation C.R.U.D operations 
 * @author Anton_Darahakupets
 * @param <Entity> The type of the object that will be used for C.R.U.D. operations
 * @param <TypeId> The type of the id that will be used for C.R.U.D operations
 */
public interface NewsManagementDAO<Entity, TypeId> {
	
	/**
	 * Create new entity in database
	 * @param entity Representation entity by the Entity object
	 * @return The id of the entity that is created in database
	 * @throws DAOException if there is some trouble with Persistence layer
	 * 
	 */
	TypeId create(Entity entity) throws DAOException;
	
	/**
	 * Read entity from database
	 * @param id The id of the entity in database
	 * @return Entity object that represents an entity
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	Entity read(TypeId id) throws DAOException;
	
	/**
	 * Update entity in database
	 * @param entity Entity object that represents an entity 
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void update(Entity entity) throws DAOException;

	/**
	 * Delete entity from database
	 * @param id The id of the entity in database
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void delete(TypeId id) throws DAOException;
	
	/**
	 * 
	 * @param dataSource
	 * @param connection
	 */
	default void closeConnection(DataSource dataSource, Connection connection) {
		DataSourceUtils.releaseConnection(connection, dataSource);
	}
	
	/**
	 * 
	 * @param dataSource
	 * @param connection
	 * @param statement
	 * @throws DAOException
	 */
	default void closeConnection(DataSource dataSource, Connection connection, Statement statement) throws DAOException {
		try {
			if(statement != null) {
				statement.close();
			}
		} catch (SQLException e){
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}
	
	/**
	 * 
	 * @param dataSource
	 * @param connection
	 * @param statement
	 * @param resultSet
	 * @throws DAOException
	 */
	default void closeConnection(DataSource dataSource, Connection connection, Statement statement, ResultSet resultSet) throws DAOException {
		try {
			if(resultSet != null) {
				resultSet.close();
			} 
		} catch (SQLException e){
			throw new DAOException(e);
		} finally {
			this.closeConnection(dataSource, connection, statement);
		}
	}
	
	/**
	 * 
	 * @param dataSource
	 * @return
	 */
	default Connection getConnection(DataSource dataSource) {
		Connection connection = null;
		connection = DataSourceUtils.getConnection(dataSource);
		return connection;
	}
	
}