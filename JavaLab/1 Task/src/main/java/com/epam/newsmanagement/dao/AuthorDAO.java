package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public interface AuthorDAO extends NewsManagementDAO<Author, Long>{
	
	/**
	 * Add author with authorId for news with newsId
	 * @param newsId News id
	 * @param AuthorId Author id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void addAuthorForNews(Long newsId, Long authorId) throws DAOException;
	
	/**
	 * Delete author with aouthorId from news with newsId
	 * @param newsId News id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void deleteAuthorFromNews(Long newsId) throws DAOException;
	
	/**
	 * Return author of news with newsId
	 * @param newsId News is
	 * @return Author 
	 * @throws DAOException
	 */
	Author getNewsAuthor(Long newsId) throws DAOException;
	
}