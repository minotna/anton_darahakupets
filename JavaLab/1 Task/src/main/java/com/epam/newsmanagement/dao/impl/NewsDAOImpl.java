package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class NewsDAOImpl implements NewsDAO {
	
	private static final String SQL_CREATE_NEW_NEWS = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (news_news_id_seq.nextval, ? , ? , ? , SYSDATE, SYSDATE)";
	private static final String SQL_READ_NEWS_BY_ID = "SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news WHERE news_id = ? ";
	private static final String SQL_UPDATE_NEWS_BY_ID = "UPDATE news SET title = ?, short_text = ?, full_text = ?, modification_date = SYSDATE WHERE news_id = ? ";
	private static final String SQL_DELETE_NEWS_BY_ID = "DELETE FROM news WHERE news_id = ? ";
	private static final String SQL_READ_ALL_NEWS_SORTED_BY_COMMENTS = "SELECT DISTINCT NID,  TT, ST, FT, CD, MD, number_commet, Aut_id FROM (SELECT NID, number_commet, A_NAME, Aut_id, TAG_NAME, TAG_ID, ROW_NUMBER() OVER (PARTITION BY NID ORDER BY TAG_ID) number_row, TT, ST, FT, CD, MD " +
			  "FROM  (SELECT DISTINCT n.NEWS_ID NID, count(c.COMMENT_ID) OVER (PARTITION BY n.NEWS_ID, TAG_ID) number_commet, aut.A_NAME A_NAME, aut.Aut_id, tags.TAG_NAME TAG_NAME, tags.TAG_ID TAG_ID, n.TITLE TT, n.SHORT_TEXT ST, n.FULL_TEXT FT, n.CREATION_DATE CD, n.MODIFICATION_DATE MD " +
			  "FROM NEWS n "
			  + "LEFT JOIN COMMENTS c on n.NEWS_ID = c.NEWS_ID "
			  + "LEFT JOIN (select na.NEWS_ID NEWS_ID, a.AUTHOR_NAME A_NAME, a.AUTHOR_ID Aut_id from NEWS_AUTHORS na INNER JOIN AUTHORS a on na.AUTHOR_ID = a.AUTHOR_ID)Aut on n.NEWS_ID = Aut.NEWS_ID "
			  + "LEFT JOIN (SELECT nt.NEWS_ID NEWS_ID, t.TAG_NAME TAG_NAME, t.TAG_ID TAG_ID from NEWS_TAGS nt INNER JOIN TAGS t on nt.TAG_ID = t.TAG_ID)tags on n.NEWS_ID = tags.NEWS_ID)) ";
	private static final String SQL_SEARCH_BY_TAGS = "WHERE number_row>=? AND TAG_ID in";
	private static final String SQL_SEARCH_BY_AUTHOR = "WHERE Aut_id = ? ";
	private static final String SQL_AND_SERCH_BY_TAGS = "AND number_row>=? AND TAG_ID in";
	private static final String SQL_OREDER_BY_MOST_COMMENTED = "ORDER BY number_commet DESC";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public Long create(News entity) throws DAOException {
		Long newsId = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_CREATE_NEW_NEWS, new String[] {"NEWS_ID"});
			String title = entity.getTitle();
			String short_text = entity.getShortText();
			String full_text = entity.getFullText();
			statement.setString(1, title);
			statement.setString(2, short_text);
			statement.setString(3, full_text);
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				newsId = resultSet.getLong(1);
			} else {
				throw new DAOException("Creating user failed, no ID obtained.");
			}
		} catch(SQLException e) {
			throw new DAOException(System.lineSeparator() + " Problem during creating news ", e);
		} finally { 
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return newsId;
	}

	@Override
	public News read(Long id) throws DAOException {
		News news = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_NEWS_BY_ID);
			statement.setLong(1, id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				news = new News();
				Long newsId = resultSet.getLong(1);
				String title  = resultSet.getString(2);
				String shortText = resultSet.getString(3);
				String fullText = resultSet.getString(4);
				Timestamp creationDate = resultSet.getTimestamp(5);
				Date modificationDate = resultSet.getDate(6);
				news.setId(newsId);
				news.setTitle(title);
				news.setShortText(shortText);
				news.setFullText(fullText);
				news.setCreationDate(creationDate);
				news.setModificationDate(modificationDate);
			}
		} catch(SQLException e) {
			throw new DAOException(System.lineSeparator() + " Problem during reading news ", e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return news;
	}

	@Override
	public void update(News entity) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_NEWS_BY_ID);
			Long newsId = entity.getId();
			String title = entity.getTitle();
			String shortText = entity.getShortText();
			String fullText = entity.getFullText();
			statement.setString(1, title);
			statement.setString(2, shortText);
			statement.setString(3, fullText);
			statement.setLong(4, newsId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(System.lineSeparator() + " Problem during updating news ", e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try { 
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_NEWS_BY_ID);
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(System.lineSeparator() + " Problem during deleting news ", e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}

	private String createQuery(SearchCriteria searchCriteria) {
		StringBuilder sbAllNews = new StringBuilder(SQL_READ_ALL_NEWS_SORTED_BY_COMMENTS);
		
		if (searchCriteria != null) {
			sbAllNews = createQueryWithSearchCriteria(searchCriteria);
		}
		
		sbAllNews.append(SQL_OREDER_BY_MOST_COMMENTED);
		
		return sbAllNews.toString();
	}

	private StringBuilder createQueryWithSearchCriteria(SearchCriteria searchCriteria) {
		StringBuilder sbSearchCriteria = new StringBuilder(SQL_READ_ALL_NEWS_SORTED_BY_COMMENTS);
		if (searchCriteria.getAuthorId() != null) { 
			sbSearchCriteria.append(SQL_SEARCH_BY_AUTHOR);
		}
		if (!searchCriteria.getTagIdList().isEmpty()) {
			if (searchCriteria.getAuthorId() == null) {
				sbSearchCriteria.append(SQL_SEARCH_BY_TAGS);
			} else {
				sbSearchCriteria.append(SQL_AND_SERCH_BY_TAGS);
			}
			sbSearchCriteria.append(makeParametres(searchCriteria));
		}

		return sbSearchCriteria;
	}
	
	private String makeParametres(SearchCriteria searchCriteria) {
		StringBuilder sb = new StringBuilder("(");
		for (int j = 0; j < searchCriteria.getTagIdList().size(); j++) {
			sb.append("?,");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		return sb.toString();
	}
	
	private PreparedStatement insertParametres(SearchCriteria sc, PreparedStatement ps) throws DAOException {
		try {
			int i = 1;
			if (sc != null) {
				if (sc.getAuthorId() != null) {
					ps.setLong(i, sc.getAuthorId());
					++i;
				}
				for (Long id : sc.getTagIdList()) {
					ps.setLong(i, id);
					++i;
				}
			}
		} catch (SQLException e) {
			throw new DAOException(System.lineSeparator() + " Problem during inserting parametrs ", e);
		}
		return ps;
	}

	@Override
	public List<News> getNewsWithSearchCriteria(SearchCriteria searchCriteria) throws DAOException {
		List<News> newsList = null;
		News news = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(createQuery(searchCriteria));
			resultSet = insertParametres(searchCriteria, statement).executeQuery();
			newsList = new ArrayList<>();
			while (resultSet.next()) {
				news = new News();
				Long newsId = resultSet.getLong(1);
				String title = resultSet.getString(2);
				String shortText = resultSet.getString(3);
				String fullText = resultSet.getString(4);
				Timestamp creationDate = resultSet.getTimestamp(5);
				Date modificationDate = resultSet.getDate(6);
				news.setId(newsId);
				news.setTitle(title);
				news.setShortText(shortText);
				news.setFullText(fullText);
				news.setCreationDate(creationDate);
				news.setModificationDate(modificationDate);
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(System.lineSeparator() + " Problem during reading news ", e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return newsList;
	}
}