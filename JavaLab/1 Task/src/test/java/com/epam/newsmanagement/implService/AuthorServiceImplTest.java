package com.epam.newsmanagement.implService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

@ContextConfiguration(locations = {"/testSpringContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceImplTest {

	@Autowired
	private AuthorDAO mockAopAuthorDAO;
	
	@InjectMocks
	@Autowired
	private AuthorService authorService;
	

	@Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        
    }
	
	@Test
	public void testCreate() throws DAOException, ServiceException {
		Author expectedAuthor = new Author();
		Long expectedAuthorId = 1L;
		expectedAuthor.setId(expectedAuthorId);
		
		when(mockAopAuthorDAO.create(expectedAuthor)).thenReturn(expectedAuthor.getId());
		
		Long actualAuthorId = authorService.create(expectedAuthor);
		
		verify(mockAopAuthorDAO, times(1)).create(expectedAuthor);
		assertEquals(expectedAuthorId, actualAuthorId);
	}
	
	@Test
	public void testRead() throws ServiceException, DAOException {
		Author expectedAuthor = new Author();
		Long authorId = 1L;
		String name = "TestAuthor";
		expectedAuthor.setId(authorId);
		expectedAuthor.setName(name);
		
		when(authorService.read(authorId)).thenReturn(expectedAuthor);
		
		Author actualAuthor = authorService.read(authorId);
		
		verify(mockAopAuthorDAO, times(1)).read(authorId);
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testUpdate() throws ServiceException, DAOException {
		Author author = new Author();
		authorService.update(author);
		
		verify(mockAopAuthorDAO,times(1)).update(author);
	}
	
	@Test
	public void testDelete() throws ServiceException, DAOException {
		Author author = new Author();
		authorService.delete(author.getId());
		
		verify(mockAopAuthorDAO,times(1)).delete(author.getId());
	}
	
	@Test
	public void testAddAuthorForNews() throws  DAOException, ServiceException {
		authorService.addAuthorForNews(anyLong(), anyLong());
		
		verify(mockAopAuthorDAO, times(1)).addAuthorForNews(anyLong(), anyLong());
	}
	
	@Test
	public void testDeleteAuthorFromNews() throws DAOException, ServiceException {
		authorService.deleteAuthorFromNews(anyLong());
		
		verify(mockAopAuthorDAO, times(1)).deleteAuthorFromNews(anyLong());
	}
	
	@Test
	public void testGetNewsAuthor() throws ServiceException, DAOException {
		authorService.getNewsAuthor(anyLong());
		
		verify(mockAopAuthorDAO, times(1)).getNewsAuthor(anyLong());
	}
}