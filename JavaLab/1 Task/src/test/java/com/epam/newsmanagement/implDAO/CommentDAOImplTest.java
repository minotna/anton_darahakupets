package com.epam.newsmanagement.implDAO;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/testSpringContext.xml" })
public class CommentDAOImplTest extends DBTestCase {
	
	@Autowired
	private CommentDAO commentDAO;

	@Autowired
	private IDatabaseTester tester;

	
	@Before
	public void setUp() throws Exception {
		IDataSet dataSet = getDataSet();
		tester.setDataSet(dataSet);
		tester.onSetup();
	}
	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@After
	public void tearDown() throws Exception {
		tester.setTearDownOperation(getTearDownOperation());
		tester.onTearDown();
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream("/commentDataSet.xml"));
	}
	
	@Test
	public void testCreate() throws Exception {
		Comment expectedComment = new Comment();
		Long newsId = 2L;
		String commentText = "Some comment";
		expectedComment.setNewsId(newsId);
		expectedComment.setCommentText(commentText);
		
		Long commentId = commentDAO.create(expectedComment);
		Comment actualComment = commentDAO.read(commentId);
		
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		assertNotNull(actualComment.getCreationDate());
	}	
	
	@Test
	public void testRead() throws DAOException {
		Comment expectedComment = new Comment();
		Long commentId = 5L;
		Long newsId = 1L;
		String commentText = "Some comment text1";
		String date = "2014-05-14 11:10:09";
		Timestamp creationDate = Timestamp.valueOf(date);
		expectedComment.setId(commentId);
		expectedComment.setNewsId(newsId);
		expectedComment.setCommentText(commentText);
		expectedComment.setCreationDate(creationDate);
		
		Comment actualComment = commentDAO.read(commentId);
		
		assertEquals(expectedComment.getId(), actualComment.getId());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		assertEquals(expectedComment.getCreationDate(), actualComment.getCreationDate());
	}
	
	@Test 
	public void testUpdate() throws DAOException {
		Comment expectedComment = new Comment();
		Long commentId = 5L;
		Long newsId = 1L;
		String newCommentText = "Another comment text1";
		expectedComment.setId(commentId);
		expectedComment.setNewsId(newsId);
		expectedComment.setCommentText(newCommentText);
		
		commentDAO.update(expectedComment);
		Comment actualComment = commentDAO.read(commentId);
		
		assertEquals(expectedComment.getId(), actualComment.getId());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
	}
	
	@Test
	public void testDelete() throws DAOException {
		Long commentId = 5L;
		
		commentDAO.delete(commentId);
		Comment comment = commentDAO.read(commentId);
		
		assertNull(comment);
	}
	
	
	@Test 
	public void testGetAllCommentsByNewsId() throws DAOException {
		Long newsId = 1L;
		int expectedSize = 2;
		List<Comment> actualCommentList = null;
		
		actualCommentList = commentDAO.getAllCommentsFromNews(newsId);
		
		assertEquals(expectedSize, actualCommentList.size());
	}
	
	@Test 
	public void testDeleteAllCommetsByNewsId() throws DAOException {
		Long newsId = 1L;
		int expectedSize = 0;
		List<Comment> actualCommentList = null;
		
		commentDAO.deleteAllCommentsFromNews(newsId);
		actualCommentList = commentDAO.getAllCommentsFromNews(newsId);
		
		assertEquals(expectedSize, actualCommentList.size());
	}
	
	@Test
	public void testAddListCommentForNews() throws DAOException {
		List<Comment> commentsList = new ArrayList<>();
		List<Comment> actualCommentsList = new ArrayList<>();
		Long newsId = 1L;
		int expectedSize = 4;
		
		Comment comment1 = new Comment();
		comment1.setNewsId(newsId);
		comment1.setCommentText("Comment text1");
		
		Comment comment2 = new Comment();
		comment2.setNewsId(newsId);
		comment2.setCommentText("Comment text2");
		
		commentsList.add(comment1);
		commentsList.add(comment2);
		
		commentDAO.addListCommentsToNews(commentsList);
		actualCommentsList = commentDAO.getAllCommentsFromNews(newsId);
		
		assertEquals(expectedSize, actualCommentsList.size());
	}
}