package com.epam.newsmanagement.implService;

import java.util.ArrayList;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;


@ContextConfiguration(locations = {"/testSpringContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
//@TransactionConfiguration(defaultRollback = true, transactionManager = "transactionManager")
public class TransactionMethodsTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	@Autowired
	private NewsService newsService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private PlatformTransactionManager transactionManager;
	
	@Test
	public void test() throws ServiceException {
				 Long authorId = 5L;
				 ArrayList<Long> idTagsList = new ArrayList<Long>();
				 idTagsList.add(3L);
				 idTagsList.add(2L);
				 News news = new News();
				 news.setTitle("Title");
				 news.setShortText("ShortText");
				 news.setFullText("FullText");
				
				 TransactionStatus transaction = transactionManager.getTransaction(new DefaultTransactionDefinition(
				 TransactionDefinition.PROPAGATION_REQUIRES_NEW));
				 
				 
		 		 Long newsId = newsService.addNews(authorId, news, idTagsList);
		 		 
		 		transactionManager.commit(transaction);
		 		
		 		 //News result = newsService.read(newsId);
		 		 
		 		 //Assert.assertEquals("Title", result.getTitle());
		 		 transactionManager.rollback(transaction);
	}
}
