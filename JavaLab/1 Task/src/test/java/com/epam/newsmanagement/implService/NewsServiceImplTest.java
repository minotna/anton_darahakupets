package com.epam.newsmanagement.implService;

import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

@ContextConfiguration(locations = {"/testSpringContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceImplTest {

	@Autowired
	private NewsDAO mockAopNewsDAO;
	
	@InjectMocks
	@Autowired
	private NewsService newsService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreate() throws ServiceException, DAOException {
		News news = new News();
		Long expectedNewsId = 1L;
		news.setId(expectedNewsId);
		
		when(mockAopNewsDAO.create(news)).thenReturn(1L);
		Long actualNewsId = newsService.create(news);
		
	    verify(mockAopNewsDAO,times(1)).create(news);
		Assert.assertEquals(news.getId(), actualNewsId);
	}
	
	@Test
	public void testRead() throws ServiceException, DAOException {
		News expectedNews = new News();
		
		when(mockAopNewsDAO.read(anyLong())).thenReturn(expectedNews);
		News actualNews = newsService.read(anyLong());
		
		verify(mockAopNewsDAO,times(1)).read(anyLong());
		Assert.assertEquals(expectedNews, actualNews);
	}

	@Test
	public void testUpdate() throws ServiceException, DAOException {
		News news = new News();
		newsService.update(news);
		verify(mockAopNewsDAO,times(1)).update(news);
	}
	

	@Test
	public void testDelete() throws ServiceException, DAOException {
		News news = new News();
		newsService.delete(news.getId());
		verify(mockAopNewsDAO,times(1)).delete(news.getId());
	}
}