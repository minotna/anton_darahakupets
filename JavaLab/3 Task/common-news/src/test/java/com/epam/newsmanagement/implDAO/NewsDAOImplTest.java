package com.epam.newsmanagement.implDAO;


import java.sql.Date;
import java.sql.Timestamp;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/testSpringContext.xml" })
public class NewsDAOImplTest extends DBTestCase {
	
	@Autowired
	private NewsDAO newsDAO;

	@Autowired
	private IDatabaseTester tester;

	
	@Before
	public void setUp() throws Exception {
		IDataSet dataSet = getDataSet();
		tester.setDataSet(dataSet);
		tester.onSetup();
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@After
	public void tearDown() throws Exception {
		tester.setTearDownOperation(getTearDownOperation());
		tester.onTearDown();
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream("/newsDataSet.xml"));
	}
	
	@Test
	public void testCreate() throws Exception {
		News expectedNews = new News();
		String title = "Test title";
		String shortText = "Test short text";
		String fullText = "Test full text";

		expectedNews.setTitle(title);
		expectedNews.setShortText(shortText);
		expectedNews.setFullText(fullText);
		
		Long newsId = newsDAO.create(expectedNews);
		News actualNews = newsDAO.read(newsId);
		
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	}	
	
	@Test
	public void testRead() throws DAOException {
		News expectedNews = new News();
		Long newsId = 1L;
		String title = "Some title0";
		String shortText = "Some short text0";
		String fullText = "Some full text0";
		String dateTime = "2013-12-31 09:17:17";
		String date = "2013-12-31";
		Timestamp creationDate = Timestamp.valueOf(dateTime);
		Date modificationDate = Date.valueOf(date);
		
		expectedNews.setId(newsId);
		expectedNews.setTitle(title);
		expectedNews.setShortText(shortText);
		expectedNews.setFullText(fullText);
		expectedNews.setCreationDate(creationDate);
		expectedNews.setModificationDate(modificationDate);
		
		News actualNews = newsDAO.read(newsId);
		
		assertEquals(expectedNews.getId(), actualNews.getId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
		assertEquals(expectedNews.getCreationDate(), actualNews.getCreationDate());
		assertEquals(expectedNews.getModificationDate(), actualNews.getModificationDate());
	}
	
	@Test 
	public void testUpdate() throws DAOException {
		News expectedNews = new News();
		Long newsId = 1L;
		String newTitle = "Another title0";
		String newShortText = "Another short text0";
		String newFullText = "Another full text0";
		
		expectedNews.setId(newsId);
		expectedNews.setTitle(newTitle);
		expectedNews.setShortText(newShortText);
		expectedNews.setFullText(newFullText);
		
		newsDAO.update(expectedNews);
		News actualNews = newsDAO.read(newsId);
		
		assertEquals(expectedNews.getId(), actualNews.getId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
		assertNotNull(actualNews.getCreationDate());
		assertNotNull(actualNews.getModificationDate());	
	}
	
	@Test
	public void testDelete() throws DAOException {
		Long newsId = 1L;
	
		newsDAO.delete(newsId);
		News actualNews = newsDAO.read(newsId);
		
		assertNull(actualNews);
	}
	
	@Test
	public void testGetNewsEmptySearchCriteria() throws Exception {
		int size = 3;
		assertEquals(size, newsDAO.getNewsWithSearchCriteria(new SearchCriteria(), Integer.MIN_VALUE, Integer.MAX_VALUE).size());
	}
}