package com.epam.newsmanagement.implDAO;


import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/testSpringContext.xml" })
public class AuthorDAOImplTest extends DBTestCase {
	
	@Autowired
	private AuthorDAO authorDAO;

	@Autowired
	private IDatabaseTester tester;

	
	@Before
	public void setUp() throws Exception {
		IDataSet dataSet = getDataSet();
		tester.setDataSet(dataSet);
		tester.onSetup();
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@After
	public void tearDown() throws Exception {
		tester.setTearDownOperation(getTearDownOperation());
		tester.onTearDown();
	}
		
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream("/authorDataSet.xml"));
	}

	@Test
	public void testCreate() throws Exception {
		Author expectedAuthor = new Author();
		String authorName = "testAuthor";
		expectedAuthor.setName(authorName);
		
		Long idAuthor = authorDAO.create(expectedAuthor);
		Author actualAuthor = authorDAO.read(idAuthor);
		
		assertEquals(expectedAuthor.getName(), actualAuthor.getName());
	}	
	
	@Test
	public void testRead() throws DAOException {
		Author expectedAuthor = new Author();
		Long authorId = 4L;
		String authorName = "Alex";
		expectedAuthor.setId(authorId);
		expectedAuthor.setName(authorName);
		
		Author actualAuthor = authorDAO.read(authorId);
		
		assertEquals(expectedAuthor.getId(), actualAuthor.getId());
		assertEquals(expectedAuthor.getName(), actualAuthor.getName());
	}
	
	@Test 
	public void testUpdate() throws DAOException {
		Author expectedAuthor = new Author();
		Long authorId = 4L;
		String authorName = "Anton";
		expectedAuthor.setId(authorId);
		expectedAuthor.setName(authorName);
		
		authorDAO.update(expectedAuthor);
		Author actualAuthor = authorDAO.read(authorId);
		
		assertEquals(expectedAuthor.getId(), actualAuthor.getId());
		assertEquals(expectedAuthor.getName(), actualAuthor.getName());
	}
	
	@Test
	public void testDelete() throws DAOException {
		Long authorId = 1L;
		Author expectedAuthor = authorDAO.read(authorId);
		
		authorDAO.delete(authorId);
		
		Author actualAuthor = authorDAO.read(authorId);
		
		assertNull(expectedAuthor.getExpired());
		assertNotNull(actualAuthor.getExpired());
	}
	
	@Test 
	public void testReadAuthorNewsById() throws DAOException {
		Long newsId = 3L;
		Author expectedAuthor = new Author();
		Long authorId = 2L;
		String authorName = "Vasja";
		expectedAuthor.setId(authorId);
		expectedAuthor.setName(authorName);
		
		Author actualAuthor = authorDAO.getNewsAuthor(newsId);
		
		assertEquals(expectedAuthor.getId(), actualAuthor.getId());
		assertEquals(expectedAuthor.getName(), actualAuthor.getName());
	}
	@Test
	public void testAddAuthorForNews() throws DAOException {
		
		Author expectedAuthor = new Author();
		Long newsId = 1L;
		Long authorId = 4L;
		String name = "Alex";
		expectedAuthor.setId(authorId);
		expectedAuthor.setName(name);
		
		authorDAO.addAuthorForNews(newsId, authorId);
		Author actualAuthor = authorDAO.getNewsAuthor(newsId);
		
		assertEquals(expectedAuthor.getId(), actualAuthor.getId());
		assertEquals(expectedAuthor.getName(), actualAuthor.getName());
	}
	
	@Test
	public void testDeleteAuthorFromNews() throws DAOException {
		Long newsId = 3L;
		
		authorDAO.deleteAuthorFromNews(newsId);
		Author actualAuthor = authorDAO.getNewsAuthor(newsId);
		
		assertEquals(null, actualAuthor);
	}
}