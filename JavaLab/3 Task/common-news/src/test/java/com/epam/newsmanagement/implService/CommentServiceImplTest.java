package com.epam.newsmanagement.implService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;

import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

@ContextConfiguration(locations = {"/testSpringContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceImplTest  {

	@Autowired
	private CommentDAO mockAopCommentDAO;
	
	@InjectMocks
	@Autowired
	private CommentService commentService;

	@Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void testCreate() throws DAOException, ServiceException {
		Comment expectedComment = new Comment();
		Long expectedCommentId = 1L;
		expectedComment.setId(expectedCommentId);
		
		when(mockAopCommentDAO.create(expectedComment)).thenReturn(expectedComment.getId());
		
		Long actualCommentId = commentService.create(expectedComment);
		
		verify(mockAopCommentDAO, times(1)).create(expectedComment);
		assertEquals(expectedCommentId, actualCommentId);
	}
	
	@Test
	public void testRead() throws ServiceException, DAOException {
		Comment expectedComment = new Comment();	
		
		when(commentService.read(anyLong())).thenReturn(expectedComment);
		Comment actualComment = commentService.read(anyLong());
		
		verify(mockAopCommentDAO, times(1)).read(anyLong());
		assertEquals(expectedComment, actualComment);
	}
	
	@Test
	public void testUpdate() throws ServiceException, DAOException {
		Comment comment = new Comment();
		commentService.update(comment);
		verify(mockAopCommentDAO,times(1)).update(comment);
	}
	
	@Test
	public void testDelete() throws ServiceException, DAOException {
		Comment comment = new Comment();
		commentService.delete(comment.getId());
		verify(mockAopCommentDAO,times(1)).delete(comment.getId());
	}
	
	@Test
	public void testGetAllCommentsFromNews() throws ServiceException, DAOException {
		commentService.getAllCommentsFromNews(anyLong());
		
		verify(mockAopCommentDAO, times(1)).getAllCommentsFromNews(anyLong());
	}
	
	@Test
	public void testDeleteAllCommentsFromNews() throws ServiceException, DAOException {
		commentService.deleteAllCommentsFromNews(anyLong());
		
		verify(mockAopCommentDAO, times(1)).deleteAllCommentsFromNews(anyLong());
	}
	
	@Test
	public void testAddListComments() throws ServiceException, DAOException {
		commentService.addListCommentsForNews(anyListOf(Comment.class));
		verify(mockAopCommentDAO, times(1)).addListCommentsToNews(anyListOf(Comment.class));
	}
}