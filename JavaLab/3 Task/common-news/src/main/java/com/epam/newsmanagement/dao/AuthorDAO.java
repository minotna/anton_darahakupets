package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public interface AuthorDAO extends NewsManagementDAO<Author, Long>{
	
	/**
	 * Add author with authorId for news with newsId
	 * @param newsId News id
	 * @param AuthorId Author id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void addAuthorForNews(Long newsId, Long authorId) throws DAOException;
	
	/**
	 * Delete author with aouthorId from news with newsId
	 * @param newsId News id
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	void deleteAuthorFromNews(Long newsId) throws DAOException;
	
	/**
	 * Return author of news with newsId
	 * @param newsId News is
	 * @return Author 
	 * @throws DAOException if there is some trouble with Persistence layer
	 */
	Author getNewsAuthor(Long newsId) throws DAOException;
	
	/**
	 * Return all authors from database
	 * @return List<Author>
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	List<Author> getAllAuthors() throws DAOException;
	
}