package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class TagDAOImpl implements TagDAO {
	
	private static final String SQL_CREATE_NEW_TAG = "INSERT INTO tags (tag_id, tag_name) VALUES (tags_tag_id_seq.nextval, ? )";
	private static final String SQL_READ_TAG_BY_ID = "SELECT tag_id, tag_name FROM tags WHERE tag_id = ? ";
	private static final String SQL_UPDATE_TAG_BY_ID = "UPDATE tags  SET tag_name = ? WHERE tag_id = ? ";
	private static final String SQL_DELETE_TAG_BY_ID = "DELETE FROM tags WHERE tag_id = ? ";
	private static final String SQL_READ_ALL_TAGS_NEWS = "SELECT tags.tag_id, tags.tag_name FROM tags INNER JOIN news_tags ON tags.tag_id = news_tags.tag_id WHERE news_id = ? ";
	private static final String SQL_DELETE_ALL_TAGS_NEWS = "DELETE FROM news_tags WHERE news_id = ? ";
	private static final String SQL_ADD_TAG_FOR_NEWS = "INSERT INTO news_tags (news_id, tag_id) VALUES ( ? , ? )";
	private static final String SQL_READ_ALL_TAGS = "SELECT t.tag_id, t.tag_name FROM tags t";
	private static final String SQL_DELETE_TAG_FROM_ALL_NEWS = "DELETE FROM news_tags WHERE tag_id = ?";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long create(Tag entity) throws DAOException {
		Long id = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_CREATE_NEW_TAG, new String[] {"TAG_ID"} );
			String tagName = entity.getName();
			statement.setString(1, tagName);
			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
			} else {
				throw new DAOException("Creating user failed, no ID obtained.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return id;
	}

	@Override
	public Tag read(Long id) throws DAOException {
		Tag tag = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_TAG_BY_ID);
			statement.setLong(1, id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				tag = new Tag();
				String tagName = resultSet.getString(2);
				tag.setId(id);
				tag.setName(tagName);
			}	
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return tag;
	}

	@Override
	public void update(Tag entity) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_TAG_BY_ID);
			String tagName = entity.getName();
			Long tagId = entity.getId();
			statement.setString(1, tagName);
			statement.setLong(2, tagId);
			statement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_TAG_BY_ID);
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}
	
	
	
	@Override
	public List<Tag> getAllTagsFromNews(Long newsId) throws DAOException {
		List<Tag> tagsList = null;
		Tag tag = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_ALL_TAGS_NEWS);
			statement.setLong(1, newsId);
			resultSet = statement.executeQuery();
			tagsList = new ArrayList<Tag>();
			while(resultSet.next()) {
				tag = new Tag();
				Long tagId = resultSet.getLong(1);
				String tagName = resultSet.getString(2);
				tag.setId(tagId);
				tag.setName(tagName);
				tagsList.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return tagsList;
	}

	@Override
	public void deleteAllTags(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_ALL_TAGS_NEWS);
			statement.setLong(1, newsId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}

	@Override
	public void addListTagsForNews(Long newsId, List<Long> idTagsList) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_ADD_TAG_FOR_NEWS);
			for (Long tagId: idTagsList) {
				statement.setLong(1, newsId);
				statement.setLong(2, tagId);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}	
	}

	@Override
	public void addTagForNews(Long newsId, Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_ADD_TAG_FOR_NEWS);
			statement.setLong(1, newsId);
			statement.setLong(2, tagId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
	}
	
	public List<Tag> getAllTags() throws DAOException {
		List<Tag> tagsList = null;
		Tag tag = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_ALL_TAGS);
			resultSet = statement.executeQuery();
			tagsList = new ArrayList<>();
			while(resultSet.next()) {
				tag = new Tag();
				Long tagId = resultSet.getLong(1);
				String tagName = resultSet.getString(2);
				tag.setId(tagId);
				tag.setName(tagName);
				tagsList.add(tag);
			}	
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement, resultSet);
		}
		return tagsList;
		
	}

	@Override
	public void detachTagFromAllNews(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_TAG_FROM_ALL_NEWS);
			statement.setLong(1, tagId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeConnection(dataSource, connection, statement);
		}
		
	}
}