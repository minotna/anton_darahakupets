package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsBody;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * 
 * @author Anton_Darahakupets
 *
 */
public class NewsServiceImpl implements NewsService {
	
	private NewsDAO newsDAO;
	private AuthorDAO authorDAO;
	private CommentDAO commentDAO;
	private TagDAO tagDAO;
	
	
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
	
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	@Override
	public Long create(News entity) throws ServiceException {
		try {
			return newsDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public News read(Long id) throws ServiceException {
		try {
			return newsDAO.read(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(News entity) throws ServiceException {
		try {
			newsDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			newsDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> getNewsWithSearchCriteria(SearchCriteria searchCriteria, int startIndex, int lastIndex) throws ServiceException {
		try {
			return newsDAO.getNewsWithSearchCriteria(searchCriteria, startIndex, lastIndex);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void editNews(News news, List<Long> idTagsList, Long authorId) throws ServiceException  {
		try {
			authorDAO.deleteAuthorFromNews(news.getId());
			tagDAO.deleteAllTags(news.getId());
			newsDAO.update(news);
			authorDAO.addAuthorForNews(news.getId(), authorId);
			tagDAO.addListTagsForNews(news.getId(), idTagsList);	
		} catch (DAOException e) {
			throw new ServiceException(e);
		}	
	}
	
	@Override
	public Long addNews(Long authorId, News news, List<Long> idTagsList) throws ServiceException {
		Long newsId= null;
		try {
			newsId = newsDAO.create(news);
			authorDAO.addAuthorForNews(newsId, authorId);
			tagDAO.addListTagsForNews(newsId, idTagsList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsId;
	}
		
	@Override
	public void deleteNews(Long newsId) throws ServiceException {
		try {
			commentDAO.deleteAllCommentsFromNews(newsId);
			tagDAO.deleteAllTags(newsId);
			authorDAO.deleteAuthorFromNews(newsId);
			newsDAO.delete(newsId);	
		} catch (DAOException e) {
			throw new ServiceException(e);
		}	
	}

	@Override
	public int getNewsNumber(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.getNewsNumber(searchCriteria);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}	
	}

	@Override
	public NewsVO getNewsVO(Long id) throws ServiceException {
		NewsVO newsVO = null;
		try {
			News news = newsDAO.read(id);
			if (news == null) {
				return newsVO;
			}
			newsVO = new NewsVO();
			newsVO.setNews(newsDAO.read(id));
			newsVO.setAuthor(authorDAO.getNewsAuthor(id));
			newsVO.setTagList(tagDAO.getAllTagsFromNews(id));
			newsVO.setCommentList(commentDAO.getAllCommentsFromNews(id));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsVO;
	}

	@Override
	public List<NewsVO> getListNewsVO(SearchCriteria searchCriteria, int startIndex, int lastIndex) throws ServiceException {
		List<NewsVO> newsVOList = null;
		List<News> listNews = null;
		NewsVO newsVO = null;
		try {
			newsVOList = new ArrayList<>();
			listNews = newsDAO.getNewsWithSearchCriteria(searchCriteria, startIndex, lastIndex);
			for (News news : listNews) {
				newsVO = new NewsVO();
				Long newsId = news.getId();
				newsVO.setNews(news);
				newsVO.setAuthor(authorDAO.getNewsAuthor(newsId));
				newsVO.setTagList(tagDAO.getAllTagsFromNews(newsId));
				newsVO.setCommentList(commentDAO.getAllCommentsFromNews(newsId));
				newsVOList.add(newsVO);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsVOList;
	}

	@Override
	public NewsBody updateNewsBody(NewsBody newsBody) throws ServiceException {
		NewsBody nb = null;
		Long newsId = newsBody.getNews().getId();
		try {
			authorDAO.deleteAuthorFromNews(newsId);
			tagDAO.deleteAllTags(newsId);
			newsDAO.update(newsBody.getNews());
			authorDAO.addAuthorForNews(newsId, newsBody.getAuthorId());
			tagDAO.addListTagsForNews(newsId, newsBody.getTagIdList());
			nb = readNewsBody(newsId);
			} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return nb;
	}

	@Override
	public Long addNewsBody(NewsBody newsBody) throws ServiceException {
		Long newsId = null;
		try {
			newsId = newsDAO.create(newsBody.getNews());
			tagDAO.addListTagsForNews(newsId, newsBody.getTagIdList());
			authorDAO.addAuthorForNews(newsId, newsBody.getAuthorId());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsId;
	}
	
	@Override
	public NewsBody readNewsBody(Long newsId) throws ServiceException {
		NewsBody newsBody = new NewsBody();
		List<Long> idTagList = new ArrayList<>();
		try {
			for (Tag tag : tagDAO.getAllTagsFromNews(newsId)) {
				idTagList.add(tag.getId());
			}
			newsBody.setNews(newsDAO.read(newsId));
			newsBody.setTagIdList(idTagList);
			newsBody.setAuthorId(authorDAO.getNewsAuthor(newsId).getId());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsBody;
	}

	@Override
	public int findIndex(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		try {
			return newsDAO.findIndex(searchCriteria, newsId);
		} catch (DAOException e) {
			throw new ServiceException(" Exception during getting index of News ", e);
		}
	}
}
