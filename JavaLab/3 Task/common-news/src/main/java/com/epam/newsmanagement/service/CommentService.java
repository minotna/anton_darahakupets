package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface CommentService is used for implementation methods that work with Comment object in Service layer
 * @author Anton_Darahakupets
 *
 */
public interface CommentService extends NewsManagementService<Comment, Long> {
	/**
	 * Returns a list of the news comments by newsId
	 * @param newsId News id
	 * @return List of comments by newsId
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	List<Comment> getAllCommentsFromNews(Long newsId) throws ServiceException;
	
	/**
	 * Delete all comments from news by newsId in one transaction
	 * @param newsId News id
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void deleteAllCommentsFromNews(Long newsId) throws ServiceException;
	
	/**
	 * Add list of comments for the news in one transaction
	 * @param commentsList List of comments
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void addListCommentsForNews(List<Comment> commentsList) throws ServiceException;
}