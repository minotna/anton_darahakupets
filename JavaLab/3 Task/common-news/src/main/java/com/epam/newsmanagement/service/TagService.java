package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface TagService is used for implementation methods that work with Tag object in Service layer
 * @author Anton_Darahakupets
 *
 */
public interface TagService extends NewsManagementService<Tag, Long> {
	/**
	 * Returns a list of the news tags by newsId
	 * @param newsId News id
	 * @return List of tags by newsId
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	List<Tag> getAllTagsFromNews(Long newsId) throws ServiceException;
	
	/**
	 * Delete all tags from news by newsId
	 * @param newsId News id
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void deleteAllTags(Long newsId) throws ServiceException;
	
	/**
	 * Add list of tags for the news
	 * @param newsId News id
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void addListTagsForNews(Long newsId, List<Long> idTagsList) throws ServiceException;
	
	/**
	 * Add one tag for news with newsId
	 * @param newsId News id
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	void addTagForNews(Long newsId, Long tagId) throws ServiceException;
	
	/**
	 * Return all tags from database
	 * @return List<Tag>
	 * @throws ServiceException if there is some trouble with Persistence layer
	 */
	List<Tag> getAllTags() throws ServiceException;
}