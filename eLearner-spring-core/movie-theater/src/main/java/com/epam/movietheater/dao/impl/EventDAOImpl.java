package com.epam.movietheater.dao.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


import com.epam.movietheater.dao.EventDAO;
import com.epam.movietheater.domain.Event;

public class EventDAOImpl implements EventDAO {
	
	static private Set<Event> events;
	
	static {
		events = new HashSet<>();
	}
	
	@Override
	public Event save(Event object) {
		if (events.add(object)) {
			return object;
		}
		return null;
	}
	
	@Override
	public Event update(Event object) {
		
		for (Event event: events) {
			if (event.getId().equals(object.getId())) {
				events.remove(object);
				break;
			}
		}
		
		events.add(object);
		return object;
	}
	
	@Override
	public void remove(Event object) {
		events.remove(object);
	}

	@Override
	public Event getById(Long id) {
		Event returnedEvent = null;
		for (Event event: events) {
			if (event.getId().equals(id)) {
				return event;
			}
		}
		return returnedEvent;
	}

	@Override
	public Collection<Event> getAll() {
		return events;
	}

	@Override
	public Event getByName(String name) {
		Event returnedEvent = null;
		for (Event event: events) {
			if (event.getName().equals(name)) {
				return event;
			}
		}	
		return returnedEvent;
	}

	@Override
	public Set<Event> getForDateRange(LocalDate from, LocalDate to) {
		Set<Event> returnedEvents = new HashSet<>();
		for (Event event: events) {
			if (event.airsOnDates(from, to)) {
				for (LocalDateTime date: event.getAirDates()) {
					if ((date.toLocalDate().compareTo(from) >= 0) && (date.toLocalDate().compareTo(to) <= 0)) {
						returnedEvents.add(event);
					}
				}
			}
		}
		return returnedEvents;
	}

	@Override
	public Set<Event> getNextEvents(LocalDateTime to) {
		LocalDateTime now = LocalDateTime.now();
		Set<Event> returnedEvents = new HashSet<>();
		for (Event event: events) {
			if (event.airsOnDates(now.toLocalDate(), to.toLocalDate())) {
				for (LocalDateTime date: event.getAirDates()) {
					if ((date.compareTo(now) >= 0) && (date.compareTo(to) <= 0 )) {
						returnedEvents.add(event);
					}
				}
			}
		}
		return returnedEvents;
	}
}
