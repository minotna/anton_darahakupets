package com.epam.movietheater.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.epam.movietheater.dao.UserDAO;
import com.epam.movietheater.domain.User;

public class UserDAOImpl implements UserDAO {
	
	final static private String SQL_CREATE_USER = "INSERT INTO users (user_id,first_name,email,birthday,last_name) values (users_user_id_seq.nextval,?,?,?,?)";
	final static private String SQL_READ_ALL_USERS = "SELECT user_id,first_name,email,birthday,last_name FROM users";
	final static private String SQL_DELETE_USER_BY_ID = "DELETE FROM users WHERE user_id = ?";
	final static private String SQL_READ_USER_BY_ID = "SELECT * FROM users WHERE user_id = ?";
	final static private String SQL_READ_USER_BY_EMAIL = "SELECT * FROM users WHERE email = ?";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public User save(User object) {
		Date birthdate = Date.valueOf(object.getBirthday());
		jdbcTemplate.update(SQL_CREATE_USER,
				object.getFirstName(),
				object.getEmail(),
				birthdate,
				object.getLastName());
		return object;
	}

	@Override
	public void remove(User object) {
		jdbcTemplate.update(SQL_DELETE_USER_BY_ID, object.getId());
	}

	@Override
	public User getById(Long id) {
		User user = null;
		try {
			user =  jdbcTemplate.queryForObject(SQL_READ_USER_BY_ID,
					new Object[]{id},
					new RowMapper<User>() {
						public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			                User user = new User();
			                user.setId(rs.getLong("user_id"));
			                user.setFirstName(rs.getString("first_name"));
			                user.setBirthday(rs.getDate("birthday").toLocalDate());
			                user.setLastName(rs.getString("last_name"));
			                user.setEmail(rs.getString("email"));
			                return user;
						}});
		} catch (IncorrectResultSizeDataAccessException e) {
			return user;
		}
		return user;
	}

	@Override
	public Collection<User> getAll() {
		Collection<User> users = jdbcTemplate.query(SQL_READ_ALL_USERS, 
				new RowMapper<User>() {
					public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		                User user = new User();
		                user.setId(rs.getLong("user_id"));
		                user.setFirstName(rs.getString("first_name"));
		                user.setBirthday(rs.getDate("birthday").toLocalDate());
		                user.setLastName(rs.getString("last_name"));
		                user.setEmail(rs.getString("email"));
		                return user;
        }});
		return users;
	}

	@Override
	public User getUserByEmail(String email) {
		User user =  jdbcTemplate.queryForObject(SQL_READ_USER_BY_EMAIL,
				new Object[]{email},
				new RowMapper<User>() {
					public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		                User user = new User();
		                user.setId(rs.getLong("user_id"));
		                user.setFirstName(rs.getString("first_name"));
		                user.setBirthday(rs.getDate("birthday").toLocalDate());
		                user.setLastName(rs.getString("last_name"));
		                user.setEmail(rs.getString("email"));
		                return user;
					}});
		return user;
	}
}
