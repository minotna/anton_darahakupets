package com.epam.movietheater.dao.impl;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.TreeSet;

import com.epam.movietheater.dao.BookingDAO;
import com.epam.movietheater.domain.Event;
import com.epam.movietheater.domain.Ticket;
import com.epam.movietheater.domain.User;

public class BookingDAOImpl implements BookingDAO {
	
	private Set<Ticket> purchasedTickets = new TreeSet<>();
	
	@Override
	public void bookTickets(Set<Ticket> tickets) {
		User user = null;
		if (tickets.iterator().hasNext()) {
			user = tickets.iterator().next().getUser();
		}	
		if (user == null) {
			purchasedTickets.addAll(tickets);
		} else {
			purchasedTickets.addAll(tickets);
			user.addTickets(tickets);
		}
	}

	@Override
	public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime) {
		Set<Ticket> tickets = new TreeSet<Ticket>();
		for (Ticket ticket: purchasedTickets) {
			if (event.equals(ticket.getEvent()) && dateTime.equals(ticket.getDateTime())) {
				tickets.add(ticket);
			}
		}
		return tickets;
	}
}
